# 进销存管理

#### 介绍
基础数据,入库管理，出库管理，库存管理，统计报表，往来帐款，系统设置。该软件还有问题，一直在修改中，每隔几天都会发布新的版本。觉得有用的麻烦大家点个star。

#### 软件架构
使用CS架构客户端使用winform开发并使用UI框架Devexpress。服务端使用.net6.0 webapi开发。客户端和服务端通过http接口交互。使用Mysql数据库，ORM框架sqlsugar使用ioc容器对对象管理使用.net内置过滤器对异常和权限进行统一处理。
 **个人主页：http://121.4.95.243:9527/#/
需要服务端代码得可以加我微信15029367414 
微信交流群：加我微信拉大家进群** 
#### 安装教程
直接下载运行即可
#### 功能说明
![登陆界面](JXC/Images/login.png)
![主页](JXC/Images/index.png)
![商品信息](JXC/Images/product.png)
![供货商信息](JXC/Images/ghs.png)
![客户信息](JXC/Images/KH.png)
![采购入库](JXC/Images/CGRK.png)
![采购退货](JXC/Images/CGTH.png)
![入库统计](JXC/Images/RKTJ.png)
![退货统计](JXC/Images/THTJ.png)
![商品销售](JXC/Images/SPXS.png)
![顾客退货](JXC/Images/GKTH.png)
![出库统计](JXC/Images/CKTJ.png)
![销售退货统计](JXC/Images/XSTHTJ.png)
![库存盘点](JXC/Images/KCPD.png)
![库存查询](JXC/Images/KCCX.png)
![库存报警](JXC/Images/SPKCLYJ.png)
![应收登记](JXC/Images/YSDJ.png)
![应付登记](JXC/Images/YFDJ.png)
![收款登记](JXC/Images/SKDJ.png)
![付款登记](JXC/Images/FKDJ.png)
