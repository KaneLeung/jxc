# 进销存管理

#### Description
基础数据,入库管理，出库管理，库存管理，统计报表，往来帐款，系统设置。使用CS架构客户端使用winform开发并使用UI框架Devexpress。服务端使用.net6.0 webapi开发。客户端和服务端通过http接口交互。使用Mysql数据库，ORM框架sqlsugar使用ioc容器对对象管理使用.net内置过滤器对异常和权限进行统一处理。该软件还有问题，一直在修改中，每隔几天都会发布新的版本

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
