﻿using Newtonsoft.Json;
using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using DevExpress.XtraEditors;
using JXC.Models;
using JXC.UI;
using NetTaste;
using DevExpress.Office.Utils;
using JXC.WebApi.Entity;
using JXC.WebApi.EntityExtend;

namespace JXC
{
    public class APIClient
    {
        private static readonly object LockObj = new object();
        private static HttpClient client = null;
        //private static readonly string BASE_ADDRESS = "http://localhost:5177/";
        private static readonly string BASE_ADDRESS = ConfigurationManager.AppSettings["ServerUrl"];
        private static readonly string ADDRESS_LOGIN = "system/login";//登录接口
        public APIClient()
        {
            GetInstance();
        }


        #region URL

        #region 基础数据

        private static readonly string ADDRESS_GetLastSPCode = "api/JCSJ/GetLastSPCode";//获取商品最后一个编码
        private static readonly string ADDRESS_AddSP = "api/JCSJ/AddSP";//新增商品 AddSP
        private static readonly string ADDRESS_GetSPList = "api/JCSJ/GetSPList";//获取所有商品
        private static readonly string ADDRESS_GetSPByID = "api/JCSJ/GetSPByID";//获取商品根据ID
        private static readonly string ADDRESS_UpdateSP = "api/JCSJ/UpdateSP";//修改商品
        private static readonly string ADDRESS_DeleteSP = "api/JCSJ/DeleteSP";//删除商品
        private static readonly string ADDRESS_AddGHS = "api/JCSJ/AddGHS";//新增供货商 AddSP
        private static readonly string ADDRESS_CheckGHSName = "api/JCSJ/CheckGHSName";//校验供应商名称是否重复

        private static readonly string ADDRESS_UpdateGHS = "api/JCSJ/UpdateGHS";//修改供货商
        private static readonly string ADDRESS_DeleteGHS = "api/JCSJ/DeleteGHS";//删除供货商
        private static readonly string ADDRESS_GetGHSList = "api/JCSJ/GetGHSList";//获取所有供货商
        private static readonly string ADDRESS_GetGHSByID = "api/JCSJ/GetGHSByID";//获取供货商根据ID
        private static readonly string ADDRESS_AddKH = "api/JCSJ/AddKH";//新增客户
        private static readonly string ADDRESS_UpdateKH = "api/JCSJ/UpdateKH";//修改客户
        private static readonly string ADDRESS_DeleteKH = "api/JCSJ/DeleteKH";//删除客户
        private static readonly string ADDRESS_CheckKHName = "api/JCSJ/CheckKHName";//校验供应商名称是否重复
        private static readonly string ADDRESS_GetKHList = "api/JCSJ/GetKHList";//获取所有客户
        private static readonly string ADDRESS_GetKHByID = "api/JCSJ/GetKHByID";//获取客户根据ID

        #endregion

        #region 系统设置

        private static readonly string ADDRESS_DeleteDicType = "system/DeleteDicType";//删除字典类型
        private static readonly string ADDRESS_AddDicType = "system/AddDicType";//增加字典类型
        private static readonly string ADDRESS_UpdateDicType = "system/UpdateDicType";//修改字典类型
        private static readonly string ADDRESS_GetDicTypeList = "system/GetDicTypeList";//获取所有的字典类型
        private static readonly string ADDRESS_GetDicTypeByID = "system/GetDicTypeByID";//获取字典类型根据ID
        private static readonly string ADDRESS_DeleteDicData = "system/DeleteDicData";//删除字典数据
        private static readonly string ADDRESS_AddDicData = "system/AddDicData";//增加字典数据
        private static readonly string ADDRESS_UpdateDicData = "system/UpdateDicData";//修改字典数据
        private static readonly string ADDRESS_GetDicDataList = "system/GetDicDataList";//获取所有的字典数据
        private static readonly string ADDRESS_GetDicDataByID = "system/GetDicDataByID";//获取字典数据根据ID

        private static readonly string ADDRESS_AddCZY = "system/AddCZY";//增加操作员
        private static readonly string ADDRESS_UpdateCZY = "system/UpdateCZY";//修改操作员
        private static readonly string ADDRESS_DeleteCZY = "system/DeleteCZY";//删除操作员
        private static readonly string ADDRESS_GetCZYList = "system/GetCZYList";//获取所有的操作员

        private static readonly string ADDRESS_AddJSGL = "system/AddJSGL";//增加角色
        private static readonly string ADDRESS_UpdateJSGL = "system/UpdateJSGL";//修改角色
        private static readonly string ADDRESS_DeleteJSGL = "system/DeleteJSGL";//删除角色
        private static readonly string ADDRESS_GetJSGLList = "system/GetJSGLList";//获取角色
        private static readonly string ADDRESS_GetJSGNList = "system/GetJSGNList";//获取角色功能

        #endregion

        #region 入库管理

        #region 采购入库

        private static readonly string ADDRESS_GetCGRKDCode = "api/RKGL/GetCGRKDCode";//生成采购入库单号
        private static readonly string ADDRESS_AddCGRKM = "api/RKGL/AddCGRKM";//新增采购入库主表
        private static readonly string ADDRESS_AddCGRKD = "api/RKGL/AddCGRKD";//新增采购入库明细表
        private static readonly string ADDRESS_UpdateCGRKM = "api/RKGL/UpdateCGRKM";//修改采购入库主表
        private static readonly string ADDRESS_UpdateCGRKD = "api/RKGL/UpdateCGRKD";//修改采购入库明细表
        private static readonly string ADDRESS_GetCGRKMListN = "api/RKGL/GetCGRKMListN";// 获取采购入库单主表 未审核
        private static readonly string ADDRESS_GetCGRKMList = "api/RKGL/GetCGRKMList";//获取采购入库单主表 已审核
        private static readonly string ADDRESS_GetCGRKDList = "api/RKGL/GetCGRKDList";//获取采购入库单明细表
        private static readonly string ADDRESS_DelCGRKDList = "api/RKGL/DelCGRKDList";// 删除采购入库单明细表
        private static readonly string ADDRESS_DelCGRKMList = "api/RKGL/DelCGRKMList";//删除采购入库单主表 
        private static readonly string ADDRESS_UpdateCGRKDList = "api/RKGL/UpdateCGRKDList";//批量修改采购入库明细表审核状态
        private static readonly string ADDRESS_AddCGRK = "api/RKGL/AddCGRK";//新增采购入库单 带事务
        private static readonly string ADDRESS_UpdateCGRK = "api/RKGL/UpdateCGRK";//新增采购入库单 带事务
        private static readonly string ADDRESS_UpdateCGRKMSH = "api/RKGL/UpdateCGRKMSH";//修改采购入库单据审核状态


        #endregion

        #region 采购退货

        private static readonly string ADDRESS_GetCGTHDCode = "api/RKGL/GetCGTHDCode";//生成采购退货单号
        private static readonly string ADDRESS_AddCGTHM = "api/RKGL/AddCGTHM";//新增采购退货主表
        private static readonly string ADDRESS_AddCGTHD = "api/RKGL/AddCGTHD";//新增采购退货明细表
        private static readonly string ADDRESS_UpdateCGTHM = "api/RKGL/UpdateCGTHM";//修改采购退货主表
        private static readonly string ADDRESS_UpdateCGTHD = "api/RKGL/UpdateCGTHD";//修改采购退货明细表
        private static readonly string ADDRESS_GetCGTHMListN = "api/RKGL/GetCGTHMListN";// 获取采购退货单主表 未审核
        private static readonly string ADDRESS_GetCGTHMList = "api/RKGL/GetCGTHMList";//获取采购退货单主表 已审核
        private static readonly string ADDRESS_GetCGTHDList = "api/RKGL/GetCGTHDList";//获取采购退货单明细表
        private static readonly string ADDRESS_DelCGTHDList = "api/RKGL/DelCGTHDList";// 删除采购退货单明细表
        private static readonly string ADDRESS_DelCGTHMList = "api/RKGL/DelCGTHMList";//删除采购退货单主表 
        private static readonly string ADDRESS_UpdateCGTHDList = "api/RKGL/UpdateCGTHDList";//批量修改采购退货明细表审核状态
        private static readonly string ADDRESS_AddCGTH = "api/RKGL/AddCGTH";//新增采购退货单 带事务
        private static readonly string ADDRESS_UpdateCGTH = "api/RKGL/UpdateCGTH";//新增采购退货单 带事务
        private static readonly string ADDRESS_UpdateCGTHMSH = "api/RKGL/UpdateCGTHMSH";//修改采购退货单据审核状态
        #endregion

        #region 入库统计

        private static readonly string ADDRESS_GetCGRKTJBList = "api/RKGL/GetCGRKTJBList";//入库统计报表


        #endregion

        #region 退库统计

        private static readonly string ADDRESS_GetCGTHTJBList = "api/RKGL/GetCGTHTJBList";//退货统计报表


        #endregion

        #endregion

        #region 出库管理

        #region 商品销售

        private static readonly string ADDRESS_GetSPXSCode = "api/CKGL/GetSPXSCode";//生成销售订单号
        private static readonly string ADDRESS_AddSPXS = "api/CKGL/AddSPXS";//新增 商品销售
        private static readonly string ADDRESS_UpdateSPXS = "api/CKGL/UpdateSPXS";//修改商品销售
        private static readonly string ADDRESS_AddSPXSM = "api/CKGL/AddSPXSM";//新增商品销售主表
        private static readonly string ADDRESS_AddSPXSD = "api/CKGL/AddSPXSD";//新增商品销售明细表
        private static readonly string ADDRESS_UpdateSPXSM = "api/CKGL/UpdateSPXSM";// 修改商品销售主表
        private static readonly string ADDRESS_UpdateSPXSMSH = "api/CKGL/UpdateSPXSMSH";//修改商品销售单据审核状态
        private static readonly string ADDRESS_UpdateSPXSD = "api/CKGL/UpdateSPXSD";//修改商品销售明细表
        private static readonly string ADDRESS_UpdateSPXSDList = "api/CKGL/UpdateSPXSDList";// 批量修改商品销售明细表审核状态
        private static readonly string ADDRESS_GetSPXSMListN = "api/CKGL/GetSPXSMListN";//获取商品销售单主表 未审核
        private static readonly string ADDRESS_GetSPXSMList = "api/CKGL/GetSPXSMList";//获取商品销售单主表 已审核
        private static readonly string ADDRESS_GetSPXSDList = "api/CKGL/GetSPXSDList";// 获取商品销售单明细表
        private static readonly string ADDRESS_DelSPXSDList = "api/CKGL/DelSPXSDList";//删除商品销售单明细表
        private static readonly string ADDRESS_DelSPXSMList = "api/CKGL/DelSPXSMList";//删除商品销售单主表
        #endregion
        #region 销售退货

        private static readonly string ADDRESS_GetXSTHCode = "api/CKGL/GetXSTHCode";//生成销售退货单号
        private static readonly string ADDRESS_AddXSTH = "api/CKGL/AddXSTH";//新增销售退货
        private static readonly string ADDRESS_UpdateXSTH = "api/CKGL/UpdateXSTH";//修改销售退货
        private static readonly string ADDRESS_AddXSTHM = "api/CKGL/AddXSTHM";//新增销售退货主表
        private static readonly string ADDRESS_AddXSTHD = "api/CKGL/AddXSTHD";//新增销售退货明细表
        private static readonly string ADDRESS_UpdateXSTHM = "api/CKGL/UpdateXSTHM";// 修改销售退货主表
        private static readonly string ADDRESS_UpdateXSTHMSH = "api/CKGL/UpdateXSTHMSH";//修改销售退货单据审核状态
        private static readonly string ADDRESS_UpdateXSTHD = "api/CKGL/UpdateXSTHD";//修改销售退货明细表
        private static readonly string ADDRESS_UpdateXSTHDList = "api/CKGL/UpdateXSTHDList";// 批量修改销售退货明细表审核状态
        private static readonly string ADDRESS_GetXSTHMListN = "api/CKGL/GetXSTHMListN";//获取销售退货单主表 未审核
        private static readonly string ADDRESS_GetXSTHMList = "api/CKGL/GetXSTHMList";//获取销售退货单主表 已审核
        private static readonly string ADDRESS_GetXSTHDList = "api/CKGL/GetXSTHDList";// 获取商品销售单明细表
        private static readonly string ADDRESS_DelXSTHDList = "api/CKGL/DelXSTHDList";//删除销售退货单明细表
        private static readonly string ADDRESS_DelXSTHMList = "api/CKGL/DelXSTHMList";//删除销售退货单主表
        #endregion
        #region 出库统计

        private static readonly string ADDRESS_GetXSCKTJBList = "api/CKGL/GetXSCKTJBList";//出库统计报表


        #endregion

        #region 退库统计

        private static readonly string ADDRESS_GetXSTHTJBList = "api/CKGL/GetXSTHTJBList";//退货统计报表
        #endregion

        #endregion

        #region 库存管理

        #region 库存盘点

        private static readonly string ADDRESS_GetKCPDCode = "api/KCGL/GetKCPDCode";//生产库存盘点单号
        private static readonly string ADDRESS_AddKCPD = "api/KCGL/AddKCPD";//新增库存盘点表
        private static readonly string ADDRESS_UpdateKCPD = "api/KCGL/UpdateKCPD";//修改库存盘点表
        private static readonly string ADDRESS_AddKCPDM = "api/KCGL/AddKCPDM";//新增库存盘点主表
        private static readonly string ADDRESS_AddKCPDD = "api/KCGL/AddKCPDD";//新增库存盘点明细表
        private static readonly string ADDRESS_UpdateKCPDM = "api/KCGL/UpdateKCPDM";// 修改库存盘点主表
        private static readonly string ADDRESS_UpdateKCPDMSH = "api/KCGL/UpdateKCPDMSH";//修改库存盘点单据审核状态
        private static readonly string ADDRESS_UpdateKCPDD = "api/KCGL/UpdateKCPDD";//修改库存盘点明细表
        private static readonly string ADDRESS_UpdateKCPDDList = "api/KCGL/UpdateKCPDDList";// 批量修改库存盘点明细表审核状态
        private static readonly string ADDRESS_GetKCPDMListN = "api/KCGL/GetKCPDMListN";//获取库存盘点单主表 未审核
        private static readonly string ADDRESS_GetKCPDMList = "api/KCGL/GetKCPDMList";//获取库存盘点单主表 已审核
        private static readonly string ADDRESS_GetKCPDDList = "api/KCGL/GetKCPDDList";// 获取库存盘点单明细表
        private static readonly string ADDRESS_DelKCPDDList = "api/KCGL/DelKCPDDList";//删除库存盘点单明细表
        private static readonly string ADDRESS_DelKCPDMList = "api/KCGL/DelKCPDMList";//删除库存盘点单主表

        #endregion

        #region 商品库存

        private static readonly string ADDRESS_GetSPKCCXList = "api/KCGL/GetSPKCCXList";//商品库存

        private static readonly string ADDRESS_GetSPKCDList = "api/KCGL/GetSPKCDList";//商品库存明细
        #endregion
        #endregion

        #region 往来帐款

        #region 应收登记和应付登记


        private static readonly string ADDRESS_GetYFKDJ = "api/WLZK/GetYFKDJ";//获取应付款登记

        private static readonly string ADDRESS_GetYSKDJ = "api/WLZK/GetYSKDJ";//获取应收款登记

        #endregion
        #region 收款登记和付款登记

        private static readonly string ADDRESS_GetFKDJ = "api/WLZK/GetFKDJ";//付款登记
        private static readonly string ADDRESS_GetFKDJCode = "api/WLZK/GetFKDJCode";//生成付款登记单号
        private static readonly string ADDRESS_AddFKDJ = "api/WLZK/AddFKDJ";//新增付款登记
        private static readonly string ADDRESS_DeleteFKDJ = "api/WLZK/DeleteFKDJ";//删除付款登记
        private static readonly string ADDRESS_UpdateFKDJ = "api/WLZK/UpdateFKDJ";//修改付款登记
        private static readonly string ADDRESS_UpdateFKDJSH = "api/WLZK/UpdateFKDJSH";//修改付款登记审核状态

        private static readonly string ADDRESS_GetSKDJ = "api/WLZK/GetSKDJ";//收款登记
        private static readonly string ADDRESS_GetSKDJCode = "api/WLZK/GetSKDJCode";//生成收款登记单号
        private static readonly string ADDRESS_AddSKDJ = "api/WLZK/AddSKDJ";//新增收款登记
        private static readonly string ADDRESS_DeleteSKDJ = "api/WLZK/DeleteSKDJ";//删除收款登记
        private static readonly string ADDRESS_UpdateSKDJ = "api/WLZK/UpdateSKDJ";//修改收款登记
        private static readonly string ADDRESS_UpdateSKDJSH = "api/WLZK/UpdateSKDJSH";//修改收款登记审核状态

        #endregion
        #endregion
        #endregion

        #region 方法

        #region 公共方法

        /// <summary>
        /// 制造单例
        /// </summary>
        /// <returns></returns>
        public static HttpClient GetInstance()
        {
            if (client == null)
            {
                lock (LockObj)
                {
                    if (client == null)
                    {
                        client = new HttpClient()
                        {
                            BaseAddress = new Uri(BASE_ADDRESS)
                        };
                    }
                }
            }
            return client;
        }
        /// <summary>
        /// 异步Post请求
        /// </summary>
        /// <param name="url">请求地址</param>
        /// <param name="strJson">传入的数据</param>
        /// <returns></returns>
        public async Task<string> PostAsync(string url, string strJson)
        {
            try
            {
                HttpContent content = new StringContent(strJson);
                content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                HttpResponseMessage res = await client.PostAsync(url, content);
                if (res.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    string resMsgStr = await res.Content.ReadAsStringAsync();
                    return resMsgStr;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
        /// <summary>
        /// 同步Post
        /// </summary>
        /// <param name="url"></param>
        /// <param name="strJson"></param>
        /// <returns></returns>
        public string? Post(string url, object strJson)
        {
            try
            {

                HttpContent content = new StringContent(JsonConvert.SerializeObject(strJson));
                content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                //client.DefaultRequestHeaders.Connection.Add("keep-alive");
                //由HttpClient发出Post请求
                if (!client.DefaultRequestHeaders.Contains("Role"))
                {
                    client.DefaultRequestHeaders.Add("Role", AppInfo.user?.CZYCode ?? "login");
                }

                if (AppInfo.user!=null&& AppInfo.user.CZYCode != null && !client.DefaultRequestHeaders.Contains("Delete"))
                {
                    client.DefaultRequestHeaders.Add("Delete", AppInfo.user.CZYCode);

                }
                HttpResponseMessage res = client.PostAsync(url, content).Result;
                if (res.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    string resMsgStr = res.Content.ReadAsStringAsync().Result;
                    return resMsgStr;
                }
                else if(res.StatusCode == HttpStatusCode.Unauthorized)
                {
                    throw new Exception("没有权限");
                }
                else if (res.StatusCode == HttpStatusCode.InternalServerError)
                {
                    throw new Exception(res.Content.ReadAsStringAsync().Result);
                }
                return null;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        /// <summary>
        /// 异步Post请求
        /// </summary>
        /// <typeparam name="TResult">返回参数的数据类型</typeparam>
        /// <param name="url">请求地址</param>
        /// <param name="data">传入的数据</param>
        /// <returns></returns>
        public async Task<TResult> PostAsync<TResult>(string url, object data)
        {
            try
            {
                var jsonData = JsonConvert.SerializeObject(data);
                HttpContent content = new StringContent(jsonData);
                content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                HttpResponseMessage res = await client.PostAsync(url, content);
                if (res.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    string resMsgStr = await res.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<ResultDto<TResult>>(resMsgStr);
                    return result != null ? result.Data : default;
                }
                else
                {
                    XtraMessageBox.Show(res.StatusCode.ToString());
                    return default;
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
                return default;
            }
        }
        /// <summary>
        /// 同步Get请求
        /// </summary>
        /// <param name="url">请求地址</param>
        /// <returns></returns>
        public string? Get(string url)
        {
            try
            {
                if (!client.DefaultRequestHeaders.Contains("Role"))
                {
                    client.DefaultRequestHeaders.Add("Role", AppInfo.user.CZYCode);
                }
                if (AppInfo.user != null && AppInfo.user.CZYCode != null && !client.DefaultRequestHeaders.Contains("Delete"))
                {
                    client.DefaultRequestHeaders.Add("Delete", AppInfo.user.CZYCode);
                }
                var responseString = client.GetStringAsync(url);
                return responseString.Result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// 异步Get请求
        /// </summary>
        /// <param name="url">请求地址</param>
        /// <returns></returns>
        public async Task<string> GetAsync(string url)
        {
            try
            {
                var responseString = await client.GetStringAsync(url);
                return responseString;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// 异步Get请求
        /// </summary>
        /// <typeparam name="TResult">返回参数的数据</typeparam>
        /// <param name="url">请求地址</param>
        /// <returns></returns>
        public async Task<TResult> GetAsync<TResult>(string url)
        {
            try
            {
                var resMsgStr = await client.GetStringAsync(url);
                var result = JsonConvert.DeserializeObject<ResultDto<TResult>>(resMsgStr);
                return result != null ? result.Data : default;
            }
            catch (Exception ex)
            {
                return default(TResult);
            }
        }


        #endregion

        #region 系统设置

        #region 登录

        /// <summary>
        /// 登录接口
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public bool Login(string username, string password)
        {
            czysz u = new czysz();
            u.CZYCode = username;
            u.PWD = password;
            string strJson = Post(ADDRESS_LOGIN, u);
            if (strJson == null)
            {
                throw new Exception("访问服务器异常");
            }
            ResultDto<czysz> result = JsonConvert.DeserializeObject<ResultDto<czysz>>(strJson); ;
            if (result == null)
            {
                return false;
            }

            if (!result.Success)
            {
                XtraMessageBox.Show(result.Msg);
            }

            AppInfo.user = result.Data;
            return result.Success;
        }

        #endregion

        #region 字典类型

        /// <summary>
        /// 增加字典类型
        /// </summary>
        /// <param name="m"></param>
        public void AddDicType(dicType m)
        {
            var strResult = Post(ADDRESS_AddDicType, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(strResult);
            if (result.Success)
            {
                XtraMessageBox.Show("新增成功");
            }
            else
            {
                XtraMessageBox.Show(result.Msg);
            }
        }

        /// <summary>
        /// 修改字典类型
        /// </summary>
        /// <param name="m"></param>
        public void UpdateDicType(dicType m)
        {
            var strResult = Post(ADDRESS_UpdateDicType, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(strResult);
            if (result.Success)
            {
                XtraMessageBox.Show("修改成功");
            }
            else
            {
                XtraMessageBox.Show(result.Msg);
            }
        }
        /// <summary>
        /// 删除字典类型
        /// </summary>
        /// <param name="m"></param>
        public void DeleteDicType(dicType m)
        {
            var strResult = Post(ADDRESS_DeleteDicType, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(strResult);
        }
        /// <summary>
        /// 获取所有的字典类型
        /// </summary>
        /// <param name="m"></param>
        public List<dicType> GetDicTypeList()
        {
            var strResult = Get(ADDRESS_GetDicTypeList);
            ResultDto<List<dicType>> result = JsonConvert.DeserializeObject<ResultDto<List<dicType>>>(strResult);
            if (result.Success)
            {
                return result.Data;
            }
            else
            {
                XtraMessageBox.Show(result.Msg);
            }

            return new List<dicType>();
        }

        /// <summary>
        /// 获取字典类型根据ID
        /// </summary>
        /// <param name="m"></param>
        public dicType GetDicTypeByID()
        {
            var strResult = Get(ADDRESS_GetDicTypeByID);
            ResultDto<dicType> result = JsonConvert.DeserializeObject<ResultDto<dicType>>(strResult);
            if (result.Success)
            {
                return result.Data;
            }
            else
            {
                XtraMessageBox.Show(result.Msg);
            }

            return new dicType();
        }
        #endregion

        #region 字典数据

        /// <summary>
        /// 增加字典数据
        /// </summary>
        /// <param name="m"></param>
        public void AddDicData(dicData m)
        {
            var strResult = Post(ADDRESS_AddDicData, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(strResult);
            if (result.Success)
            {
                XtraMessageBox.Show("新增成功");
            }
            else
            {
                XtraMessageBox.Show(result.Msg);
            }
        }
        /// <summary>
        /// 删除字典类型
        /// </summary>
        /// <param name="m"></param>
        public void DeleteDicData(dicData m)
        {
            var strResult = Post(ADDRESS_DeleteDicData, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(strResult);
        }
        /// <summary>
        /// 修改字典数据
        /// </summary>
        /// <param name="m"></param>
        public void UpdateDicData(dicData m)
        {
            var strResult = Post(ADDRESS_UpdateDicData, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(strResult);
            if (result.Success)
            {
                XtraMessageBox.Show("修改成功");
            }
            else
            {
                XtraMessageBox.Show(result.Msg);
            }
        }

        /// <summary>
        /// 获取所有的字典数据
        /// </summary>
        /// <param name="m"></param>
        public List<dicData> GetDicDataList()
        {
            var strResult = Get(ADDRESS_GetDicDataList);
            ResultDto<List<dicData>> result = JsonConvert.DeserializeObject<ResultDto<List<dicData>>>(strResult);
            if (result.Success)
            {
                return result.Data;
            }
            else
            {
                XtraMessageBox.Show(result.Msg);
            }

            return new List<dicData>();
        }

        /// <summary>
        /// 获取字典数据根据ID
        /// </summary>
        /// <param name="m"></param>
        public dicData GetDicDataByID()
        {
            var strResult = Get(ADDRESS_GetDicDataByID);
            ResultDto<dicData> result = JsonConvert.DeserializeObject<ResultDto<dicData>>(strResult);
            if (result.Success)
            {
                return result.Data;
            }
            else
            {
                XtraMessageBox.Show(result.Msg);
            }

            return new dicData();
        }
        #endregion

        #region 操作员

        /// <summary>
        /// 新增操作员
        /// </summary>
        /// <param name="m"></param>
        public bool AddCZY(czysz m)
        {
            string data = Post(ADDRESS_AddCZY, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data ;
        }

        /// <summary>
        /// 获取所有操作员
        /// </summary>
        /// <returns></returns>
        public List<czysz> GetCZYList()
        {
            string data = Get(ADDRESS_GetCZYList);
            ResultDto<List<czysz>> result = JsonConvert.DeserializeObject<ResultDto<List<czysz>>>(data);
            return result.Data;

        }
        /// <summary>
        /// 修改操作员
        /// </summary>
        /// <returns></returns>
        public bool UpdateCZY(czysz m)
        {
            string data = Post(ADDRESS_UpdateCZY, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;

        }
        /// <summary>
        /// 删除操作员
        /// </summary>
        /// <returns></returns>
        public bool DeleteCZY(czysz m)
        {
            string data = Post(ADDRESS_DeleteCZY, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;

        }


        #endregion
        #region 角色

        /// <summary>
        /// 新增角色
        /// </summary>
        /// <param name="m"></param>
        public bool AddJSGL(JSGLM m)
        {
            string data = Post(ADDRESS_AddJSGL, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;
        }

        /// <summary>
        /// 获取所有角色
        /// </summary>
        /// <returns></returns>
        public List<jsgl> GetJSGLList()
        {
            string data = Get(ADDRESS_GetJSGLList);
            ResultDto<List<jsgl>> result = JsonConvert.DeserializeObject<ResultDto<List<jsgl>>>(data);
            return result.Data;

        }  /// <summary>
        /// 获取角色GONGN
        /// </summary>
        /// <returns></returns>
        public List<jsgn> GetJSGNList(int id)
        {
            string data = Get(ADDRESS_GetJSGNList+$"?id={id}");
            ResultDto<List<jsgn>> result = JsonConvert.DeserializeObject<ResultDto<List<jsgn>>>(data);
            return result.Data;

        }
        /// <summary>
        /// 修改角色
        /// </summary>
        /// <returns></returns>
        public bool UpdateJSGL(JSGLM m)
        {
            string data = Post(ADDRESS_UpdateJSGL, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;

        }
        /// <summary>
        /// 删除角色
        /// </summary>
        /// <returns></returns>
        public bool DeleteJSGL(JSGLM m)
        {
            string data = Post(ADDRESS_DeleteJSGL, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;

        }

        #endregion
        #endregion
        #region 基础数据

        #region 商品管理

        /// <summary>
        /// 获取最后一个商品编码
        /// </summary>
        /// <returns></returns>
        public string GetLastSPCode()
        {
            var data = Get(ADDRESS_GetLastSPCode);
            if (data == null)
            {
                throw new Exception("访问服务器异常");
            }
            ResultDto<string> result = JsonConvert.DeserializeObject<ResultDto<string>>(data);
            if (result == null)
            {
                return null;
            }

            if (!result.Success)
            {
                XtraMessageBox.Show(result.Msg);
                return null;
            }
            return result.Data;
        }

        /// <summary>
        /// 新增商品
        /// </summary>
        /// <param name="m"></param>
        public bool AddSP(spxx m)
        {
            string data = Post(ADDRESS_AddSP, m);
            ResultDto<int> result = JsonConvert.DeserializeObject<ResultDto<int>>(data);
            return result.Data > 0;
        }
        /// <summary>
        /// 获取所有商品
        /// </summary>
        /// <returns></returns>
        public List<spxx> GetSPList()
        {
            string data = Get(ADDRESS_GetSPList);
            ResultDto<List<spxx>> result = JsonConvert.DeserializeObject<ResultDto<List<spxx>>>(data);
            return result.Data;

        }
        /// <summary>
        /// 修改商品
        /// </summary>
        /// <returns></returns>
        public bool UpdateSP(List<spxx> m)
        {
            string data = Post(ADDRESS_UpdateSP, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;

        }
        /// <summary>
        /// 删除商品
        /// </summary>
        /// <returns></returns>
        public bool DeleteSP(List<spxx> m)
        {
            string data = Post(ADDRESS_DeleteSP, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;

        }
        /// <summary>
        /// 获取商品根据ID
        /// </summary>
        /// <returns></returns>
        public spxx GetSPByID()
        {
            string data = Get(ADDRESS_GetSPByID);
            ResultDto<spxx> result = JsonConvert.DeserializeObject<ResultDto<spxx>>(data);
            return result.Data;

        }

        #endregion
        #region 供货商管理

        /// <summary>
        /// 新增供货商
        /// </summary>
        /// <param name="m"></param>
        public bool AddGHS(ghsxx m)
        {
            string data = Post(ADDRESS_AddGHS, m);
            ResultDto<int> result = JsonConvert.DeserializeObject<ResultDto<int>>(data);
            return result.Data > 0;
        }

        /// <summary>
        /// 获取所有供货商
        /// </summary>
        /// <returns></returns>
        public List<ghsxx> GetGHSList()
        {
            string data = Get(ADDRESS_GetGHSList);
            ResultDto<List<ghsxx>> result = JsonConvert.DeserializeObject<ResultDto<List<ghsxx>>>(data);
            return result.Data;

        }

        /// <summary>
        /// 获取供货商根据ID
        /// </summary>
        /// <returns></returns>
        public ghsxx GetGHSByID()
        {
            string data = Get(ADDRESS_GetGHSByID);
            ResultDto<ghsxx> result = JsonConvert.DeserializeObject<ResultDto<ghsxx>>(data);
            return result.Data;

        }
        /// <summary>
        /// 修改供货商
        /// </summary>
        /// <returns></returns>
        public bool UpdateGHS(List<ghsxx> m)
        {
            string data = Post(ADDRESS_UpdateGHS, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;

        }
        /// <summary>
        /// 校验供货商名称是否重复
        /// </summary>
        /// <param name="name"></param>
        public bool CheckGHSName(string name)
        {
            string data = Get(ADDRESS_CheckGHSName + $"?name={name}");
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;
        }
        /// <summary>
        /// 删除供货商
        /// </summary>
        /// <returns></returns>
        public bool DeleteGHS(List<ghsxx> m)
        {
            string data = Post(ADDRESS_DeleteGHS, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;

        }
        #endregion
        #region 客户管理

        /// <summary>
        /// 新增客户
        /// </summary>
        /// <param name="m"></param>
        public bool AddKH(khxx m)
        {
            string data = Post(ADDRESS_AddKH, m);
            ResultDto<int> result = JsonConvert.DeserializeObject<ResultDto<int>>(data);
            return result.Data > 0;
        }
        /// <summary>
        /// 校验客户名称是否重复
        /// </summary>
        /// <param name="name"></param>
        public bool CheckKHName(string name)
        {
            string data = Get(ADDRESS_CheckKHName + $"?name={name}");
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;
        }
        /// <summary>
        /// 获取所有客户
        /// </summary>
        /// <returns></returns>
        public List<khxx> GetKHList()
        {
            string data = Get(ADDRESS_GetKHList);
            ResultDto<List<khxx>> result = JsonConvert.DeserializeObject<ResultDto<List<khxx>>>(data);
            return result.Data;

        }

        /// <summary>
        /// 获取客户根据ID
        /// </summary>
        /// <returns></returns>
        public khxx GetKHByID()
        {
            string data = Get(ADDRESS_GetKHByID);
            ResultDto<khxx> result = JsonConvert.DeserializeObject<ResultDto<khxx>>(data);
            return result.Data;

        }
        /// <summary>
        /// 修改客户
        /// </summary>
        /// <returns></returns>
        public bool UpdateKH(List<khxx> m)
        {
            string data = Post(ADDRESS_UpdateKH, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;

        }
        /// <summary>
        /// 删除客户
        /// </summary>
        /// <returns></returns>
        public bool DeleteKH(List<khxx> m)
        {
            string data = Post(ADDRESS_DeleteKH, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;

        }
        #endregion
        #endregion
        #region 入库管理

        #region 采购入库

        /// <summary>
        /// 生成采购入库单号
        /// </summary>
        /// <returns></returns>
        public string GetCGRKDCode()
        {
            string data = Get(ADDRESS_GetCGRKDCode);
            ResultDto<string> result = JsonConvert.DeserializeObject<ResultDto<string>>(data);
            return result.Data;
        }
        /// <summary>
        /// 新增采购入库主表
        /// </summary>
        /// <returns></returns>
        public int AddCGRKM(cgrkm m)
        {
            string data = Post(ADDRESS_AddCGRKM, m);
            ResultDto<int> result = JsonConvert.DeserializeObject<ResultDto<int>>(data);
            return result.Data;
        }
        /// <summary>
        /// 新增采购入库
        /// </summary>
        /// <returns></returns>
        public bool AddCGRK(CGRK m)
        {
            string data = Post(ADDRESS_AddCGRK, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;
        }
        /// <summary>
        /// 新增采购入库明细表
        /// </summary>
        /// <returns></returns>
        public int AddCGRKD(List<cgrkd> m)
        {
            string data = Post(ADDRESS_AddCGRKD, m);
            ResultDto<int> result = JsonConvert.DeserializeObject<ResultDto<int>>(data);
            return result.Data;
        }
        /// <summary>
        /// 修改采购入库主表
        /// </summary>
        /// <returns></returns>
        public bool UpdateCGRKM(cgrkm m)
        {
            string data = Post(ADDRESS_UpdateCGRKM, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;
        }
        /// <summary>
        /// 批量修改采购入库明细表审核状态
        /// </summary>
        /// <returns></returns>
        public bool UpdateCGRKMSH(List<cgrkm> m)
        {
            string data = Post(ADDRESS_UpdateCGRKMSH, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;
        }
        /// <summary>UpdateCGRKMSH
        /// 修改采购入库明细表
        /// </summary>
        /// <returns></returns>
        public bool UpdateCGRKD(cgrkd m)
        {
            string data = Post(ADDRESS_UpdateCGRKD, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;
        }
        /// <summary>
        /// 修改采购入库
        /// </summary>
        /// <returns></returns>
        public bool UpdateCGRK(CGRK m)
        {
            string data = Post(ADDRESS_UpdateCGRK, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;
        }
        /// <summary>
        /// 获取采购入库单主表 未审核
        /// </summary>
        /// <returns></returns>
        public List<cgrkm> GetCGRKMListN()
        {
            string data = Get(ADDRESS_GetCGRKMListN);
            ResultDto<List<cgrkm>> result = JsonConvert.DeserializeObject<ResultDto<List<cgrkm>>>(data);
            return result.Data;
        }
        /// <summary>
        /// 获取采购入库单主表 已审核
        /// </summary>
        /// <returns></returns>
        public List<cgrkm> GetCGRKMList()
        {
            string data = Get(ADDRESS_GetCGRKMList);
            ResultDto<List<cgrkm>> result = JsonConvert.DeserializeObject<ResultDto<List<cgrkm>>>(data);
            return result.Data;
        }

        /// <summary>
        /// 获取采购入库单明细表
        /// </summary>
        /// <returns></returns>
        public List<cgrkd> GetCGRKDList(string code)
        {
            string data = Get(ADDRESS_GetCGRKDList + "?code=" + code);
            ResultDto<List<cgrkd>> result = JsonConvert.DeserializeObject<ResultDto<List<cgrkd>>>(data);
            return result.Data;
        }

        /// <summary>
        /// 删除采购入库单明细表
        /// </summary>
        /// <returns></returns>
        public bool DelCGRKDList(List<cgrkd> m)
        {
            string data = Post(ADDRESS_DelCGRKDList, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;
        }
        /// <summary>
        /// 删除采购入库单主表
        /// </summary>
        /// <returns></returns>
        public bool DelCGRKMList(List<cgrkm> m)
        {
            string data = Post(ADDRESS_DelCGRKMList, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;
        }
        /// <summary>
        /// 批量修改采购入库明细表审核状态
        /// </summary>
        /// <returns></returns>
        public bool UpdateCGRKDList(List<cgrkd> m)
        {
            string data = Post(ADDRESS_UpdateCGRKDList, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;
        }
        #endregion

        #region 采购退货

        /// <summary>
        /// 生成采购入库单号
        /// </summary>
        /// <returns></returns>
        public string GetCGTHDCode()
        {
            string data = Get(ADDRESS_GetCGTHDCode);
            ResultDto<string> result = JsonConvert.DeserializeObject<ResultDto<string>>(data);
            return result.Data;
        }
        /// <summary>
        /// 新增采购入库主表
        /// </summary>
        /// <returns></returns>
        public int AddCGTHM(cgthm m)
        {
            string data = Post(ADDRESS_AddCGTHM, m);
            ResultDto<int> result = JsonConvert.DeserializeObject<ResultDto<int>>(data);
            return result.Data;
        }
        /// <summary>
        /// 新增采购入库
        /// </summary>
        /// <returns></returns>
        public bool AddCGTH(CGTH m)
        {
            string data = Post(ADDRESS_AddCGTH, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;
        }
        /// <summary>
        /// 新增采购入库明细表
        /// </summary>
        /// <returns></returns>
        public int AddCGTHD(List<cgthd> m)
        {
            string data = Post(ADDRESS_AddCGTHD, m);
            ResultDto<int> result = JsonConvert.DeserializeObject<ResultDto<int>>(data);
            return result.Data;
        }
        /// <summary>
        /// 修改采购入库主表
        /// </summary>
        /// <returns></returns>
        public bool UpdateCGTHM(cgthm m)
        {
            string data = Post(ADDRESS_UpdateCGTHM, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;
        }
        /// <summary>
        /// 批量修改采购入库明细表审核状态
        /// </summary>
        /// <returns></returns>
        public bool UpdateCGTHMSH(List<cgthm> m)
        {
            string data = Post(ADDRESS_UpdateCGTHMSH, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;
        }
        /// <summary>UpdateCGTHMSH
        /// 修改采购入库明细表
        /// </summary>
        /// <returns></returns>
        public bool UpdateCGTHD(cgthd m)
        {
            string data = Post(ADDRESS_UpdateCGTHD, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;
        }
        /// <summary>
        /// 修改采购入库
        /// </summary>
        /// <returns></returns>
        public bool UpdateCGTH(CGTH m)
        {
            string data = Post(ADDRESS_UpdateCGTH, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;
        }
        /// <summary>
        /// 获取采购入库单主表 未审核
        /// </summary>
        /// <returns></returns>
        public List<cgthm> GetCGTHMListN()
        {
            string data = Get(ADDRESS_GetCGTHMListN);
            ResultDto<List<cgthm>> result = JsonConvert.DeserializeObject<ResultDto<List<cgthm>>>(data);
            return result.Data;
        }
        /// <summary>
        /// 获取采购入库单主表 已审核
        /// </summary>
        /// <returns></returns>
        public List<cgthm> GetCGTHMList()
        {
            string data = Get(ADDRESS_GetCGTHMList);
            ResultDto<List<cgthm>> result = JsonConvert.DeserializeObject<ResultDto<List<cgthm>>>(data);
            return result.Data;
        }

        /// <summary>
        /// 获取采购入库单明细表
        /// </summary>
        /// <returns></returns>
        public List<cgthd> GetCGTHDList(string code)
        {
            string data = Get(ADDRESS_GetCGTHDList + "?code=" + code);
            ResultDto<List<cgthd>> result = JsonConvert.DeserializeObject<ResultDto<List<cgthd>>>(data);
            return result.Data;
        }

        /// <summary>
        /// 删除采购入库单明细表
        /// </summary>
        /// <returns></returns>
        public bool DelCGTHDList(List<cgthd> m)
        {
            string data = Post(ADDRESS_DelCGTHDList, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;
        }
        /// <summary>
        /// 删除采购入库单主表
        /// </summary>
        /// <returns></returns>
        public bool DelCGTHMList(List<cgthm> m)
        {
            string data = Post(ADDRESS_DelCGTHMList, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;
        }
        /// <summary>
        /// 批量修改采购入库明细表审核状态
        /// </summary>
        /// <returns></returns>
        public bool UpdateCGTHDList(List<cgthd> m)
        {
            string data = Post(ADDRESS_UpdateCGTHDList, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;
        }
        #endregion

        #region 入库统计

        /// <summary>
        /// 入库统计报表
        /// </summary>
        /// <returns></returns>
        public List<CGTJB> GetCGRKTJBList(string code, string spcode)
        {
            string data = Post(ADDRESS_GetCGRKTJBList, new cgrkd() { MCode = code, SPCode = spcode });
            ResultDto<List<CGTJB>> result = JsonConvert.DeserializeObject<ResultDto<List<CGTJB>>>(data);
            return result.Data;
        }

        #endregion
        #region 退库统计


        /// <summary>
        /// 退货统计报表
        /// </summary>
        /// <returns></returns>
        public List<CGTJB> GetCGTHTJBList(string code, string spcode)
        {
            string data = Post(ADDRESS_GetCGTHTJBList, new cgrkd() { MCode = code, SPCode = spcode });
            ResultDto<List<CGTJB>> result = JsonConvert.DeserializeObject<ResultDto<List<CGTJB>>>(data);
            return result.Data;
        }
        #endregion
        #endregion
        #region 出库管理

        #region 商品销售

        /// <summary>
        /// 生成商品销售单号
        /// </summary>
        /// <returns></returns>
        public string GetSPXSDCode()
        {
            string data = Get(ADDRESS_GetSPXSCode);
            ResultDto<string> result = JsonConvert.DeserializeObject<ResultDto<string>>(data);
            return result.Data;
        }
        /// <summary>
        /// 新增商品销售主表
        /// </summary>
        /// <returns></returns>
        public int AddSPXSM(spxsm m)
        {
            string data = Post(ADDRESS_AddSPXSM, m);
            ResultDto<int> result = JsonConvert.DeserializeObject<ResultDto<int>>(data);
            return result.Data;
        }
        /// <summary>
        /// 新增商品销售
        /// </summary>
        /// <returns></returns>
        public bool AddSPXS(SPXS m)
        {
            string data = Post(ADDRESS_AddSPXS, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;
        }
        /// <summary>
        /// 新增商品销售明细表
        /// </summary>
        /// <returns></returns>
        public int AddSPXSD(List<spxsd> m)
        {
            string data = Post(ADDRESS_AddSPXSD, m);
            ResultDto<int> result = JsonConvert.DeserializeObject<ResultDto<int>>(data);
            return result.Data;
        }
        /// <summary>
        /// 修改商品销售主表
        /// </summary>
        /// <returns></returns>
        public bool UpdateSPXSM(spxsm m)
        {
            string data = Post(ADDRESS_UpdateSPXSM, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;
        }
        /// <summary>
        /// 批量修改商品销售明细表审核状态
        /// </summary>
        /// <returns></returns>
        public bool UpdateSPXSMSH(List<spxsm> m)
        {
            string data = Post(ADDRESS_UpdateSPXSMSH, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;
        }
        /// <summary>UpdateCGRKMSH
        /// 修改商品销售明细表
        /// </summary>
        /// <returns></returns>
        public bool UpdateSPXSD(spxsd m)
        {
            string data = Post(ADDRESS_UpdateSPXSD, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;
        }
        /// <summary>
        /// 修改商品销售
        /// </summary>
        /// <returns></returns>
        public bool UpdateSPXS(SPXS m)
        {
            string data = Post(ADDRESS_UpdateSPXS, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;
        }
        /// <summary>
        /// 获取商品销售单主表 未审核
        /// </summary>
        /// <returns></returns>
        public List<spxsm> GetSPXSMListN()
        {
            string data = Get(ADDRESS_GetSPXSMListN);
            ResultDto<List<spxsm>> result = JsonConvert.DeserializeObject<ResultDto<List<spxsm>>>(data);
            return result.Data;
        }
        /// <summary>
        /// 获取商品销售单主表 已审核
        /// </summary>
        /// <returns></returns>
        public List<spxsm> GetSPXSMList()
        {
            string data = Get(ADDRESS_GetSPXSMList);
            ResultDto<List<spxsm>> result = JsonConvert.DeserializeObject<ResultDto<List<spxsm>>>(data);
            return result.Data;
        }

        /// <summary>
        /// 获取商品销售单明细表
        /// </summary>
        /// <returns></returns>
        public List<spxsd> GetSPXSDList(string code)
        {
            string data = Get(ADDRESS_GetSPXSDList + "?code=" + code);
            ResultDto<List<spxsd>> result = JsonConvert.DeserializeObject<ResultDto<List<spxsd>>>(data);
            return result.Data;
        }

        /// <summary>
        /// 删除商品销售单明细表
        /// </summary>
        /// <returns></returns>
        public bool DelSPXSDList(List<spxsd> m)
        {
            string data = Post(ADDRESS_DelCGRKDList, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;
        }
        /// <summary>
        /// 删除商品销售单主表
        /// </summary>
        /// <returns></returns>
        public bool DelSPXSMList(List<spxsm> m)
        {
            string data = Post(ADDRESS_DelSPXSMList, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;
        }
        /// <summary>
        /// 批量修改商品销售明细表审核状态
        /// </summary>
        /// <returns></returns>
        public bool UpdateSPXSDList(List<spxsd> m)
        {
            string data = Post(ADDRESS_UpdateSPXSDList, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;
        }

        #endregion

        #region 商品库存

        

        #endregion
        #region 销售退货

        /// <summary>
        /// 生成销售退货单号
        /// </summary>
        /// <returns></returns>
        public string GetXSTHDCode()
        {
            string data = Get(ADDRESS_GetXSTHCode);
            ResultDto<string> result = JsonConvert.DeserializeObject<ResultDto<string>>(data);
            return result.Data;
        }
        /// <summary>
        /// 新增销售退货主表
        /// </summary>
        /// <returns></returns>
        public int AddXSTHM(xsthm m)
        {
            string data = Post(ADDRESS_AddXSTHM, m);
            ResultDto<int> result = JsonConvert.DeserializeObject<ResultDto<int>>(data);
            return result.Data;
        }
        /// <summary>
        /// 新增销售退货
        /// </summary>
        /// <returns></returns>
        public bool AddXSTH(XSTH m)
        {
            string data = Post(ADDRESS_AddXSTH, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;
        }
        /// <summary>
        /// 新增销售退货明细表
        /// </summary>
        /// <returns></returns>
        public int AddXSTHD(List<xsthd> m)
        {
            string data = Post(ADDRESS_AddXSTHD, m);
            ResultDto<int> result = JsonConvert.DeserializeObject<ResultDto<int>>(data);
            return result.Data;
        }
        /// <summary>
        /// 修改销售退货主表
        /// </summary>
        /// <returns></returns>
        public bool UpdateXSTHM(xsthm m)
        {
            string data = Post(ADDRESS_UpdateXSTHM, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;
        }
        /// <summary>
        /// 批量修改销售退货明细表审核状态
        /// </summary>
        /// <returns></returns>
        public bool UpdateXSTHMSH(List<xsthm> m)
        {
            string data = Post(ADDRESS_UpdateXSTHMSH, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;
        }
        /// <summary>UpdateCGRKMSH
        /// 修改销售退货明细表
        /// </summary>
        /// <returns></returns>
        public bool UpdateXSTHD(xsthd m)
        {
            string data = Post(ADDRESS_UpdateXSTHD, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;
        }
        /// <summary>
        /// 修改销售退货
        /// </summary>
        /// <returns></returns>
        public bool UpdateXSTH(XSTH m)
        {
            string data = Post(ADDRESS_UpdateXSTH, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;
        }
        /// <summary>
        /// 获取销售退货单主表 未审核
        /// </summary>
        /// <returns></returns>
        public List<xsthm> GetXSTHMListN()
        {
            string data = Get(ADDRESS_GetXSTHMListN);
            ResultDto<List<xsthm>> result = JsonConvert.DeserializeObject<ResultDto<List<xsthm>>>(data);
            return result.Data;
        }
        /// <summary>
        /// 获取销售退货单主表 已审核
        /// </summary>
        /// <returns></returns>
        public List<xsthm> GetXSTHMList()
        {
            string data = Get(ADDRESS_GetXSTHMList);
            ResultDto<List<xsthm>> result = JsonConvert.DeserializeObject<ResultDto<List<xsthm>>>(data);
            return result.Data;
        }

        /// <summary>
        /// 获取销售退货单明细表
        /// </summary>
        /// <returns></returns>
        public List<xsthd> GetXSTHDList(string code)
        {
            string data = Get(ADDRESS_GetXSTHDList + "?code=" + code);
            ResultDto<List<xsthd>> result = JsonConvert.DeserializeObject<ResultDto<List<xsthd>>>(data);
            return result.Data;
        }

        /// <summary>
        /// 删除销售退货单明细表
        /// </summary>
        /// <returns></returns>
        public bool DelXSTHDList(List<xsthd> m)
        {
            string data = Post(ADDRESS_DelCGRKDList, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;
        }
        /// <summary>
        /// 删除销售退货单主表
        /// </summary>
        /// <returns></returns>
        public bool DelXSTHMList(List<xsthm> m)
        {
            string data = Post(ADDRESS_DelXSTHMList, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;
        }
        /// <summary>
        /// 批量修改销售退货明细表审核状态
        /// </summary>
        /// <returns></returns>
        public bool UpdateXSTHDList(List<xsthd> m)
        {
            string data = Post(ADDRESS_UpdateXSTHDList, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;
        }

        #endregion

        #region 入库统计

        /// <summary>
        /// 出库统计报表
        /// </summary>
        /// <returns></returns>
        public List<XSTJB> GetXSCKTJBList(string code, string spcode)
        {
            string data = Post(ADDRESS_GetXSCKTJBList, new spxsd
                ()
            { MCode = code, SPCode = spcode });
            ResultDto<List<XSTJB>> result = JsonConvert.DeserializeObject<ResultDto<List<XSTJB>>>(data);
            return result.Data;
        }

        #endregion
        #region 退库统计


        /// <summary>
        /// 退货统计报表
        /// </summary>
        /// <returns></returns>
        public List<XSTJB> GetXSTHTJBList(string code, string spcode)
        {
            string data = Post(ADDRESS_GetXSTHTJBList, new xsthd() { MCode = code, SPCode = spcode });
            ResultDto<List<XSTJB>> result = JsonConvert.DeserializeObject<ResultDto<List<XSTJB>>>(data);
            return result.Data;
        }
        #endregion
        #endregion
        #region 库存管理

        #region 库存盘点

        /// <summary>
        /// 生成库存盘点单号
        /// </summary>
        /// <returns></returns>
        public string GetKCPDDCode()
        {
            string data = Get(ADDRESS_GetKCPDCode);
            ResultDto<string> result = JsonConvert.DeserializeObject<ResultDto<string>>(data);
            return result.Data;
        }
        /// <summary>
        /// 新增库存盘点主表
        /// </summary>
        /// <returns></returns>
        public int AddKCPDM(kcpdm m)
        {
            string data = Post(ADDRESS_AddKCPDM, m);
            ResultDto<int> result = JsonConvert.DeserializeObject<ResultDto<int>>(data);
            return result.Data;
        }
        /// <summary>
        /// 新增库存盘点
        /// </summary>
        /// <returns></returns>
        public bool AddKCPD(KCPD m)
        {
            string data = Post(ADDRESS_AddKCPD, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;
        }
        /// <summary>
        /// 新增库存盘点明细表
        /// </summary>
        /// <returns></returns>
        public int AddKCPDD(List<kcpdd> m)
        {
            string data = Post(ADDRESS_AddKCPDD, m);
            ResultDto<int> result = JsonConvert.DeserializeObject<ResultDto<int>>(data);
            return result.Data;
        }
        /// <summary>
        /// 修改库存盘点主表
        /// </summary>
        /// <returns></returns>
        public bool UpdateKCPDM(kcpdm m)
        {
            string data = Post(ADDRESS_UpdateKCPDM, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;
        }
        /// <summary>
        /// 批量修改库存盘点明细表审核状态
        /// </summary>
        /// <returns></returns>
        public bool UpdateKCPDMSH(List<kcpdm> m)
        {
            string data = Post(ADDRESS_UpdateKCPDMSH, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;
        }
        /// <summary>UpdateCGRKMSH
        /// 修改库存盘点明细表
        /// </summary>
        /// <returns></returns>
        public bool UpdateKCPDD(kcpdd m)
        {
            string data = Post(ADDRESS_UpdateKCPDD, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;
        }
        /// <summary>
        /// 修改库存盘点
        /// </summary>
        /// <returns></returns>
        public bool UpdateKCPD(KCPD m)
        {
            string data = Post(ADDRESS_UpdateKCPD, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;
        }
        /// <summary>
        /// 获取库存盘点单主表 未审核
        /// </summary>
        /// <returns></returns>
        public List<kcpdm> GetKCPDMListN()
        {
            string data = Get(ADDRESS_GetKCPDMListN);
            ResultDto<List<kcpdm>> result = JsonConvert.DeserializeObject<ResultDto<List<kcpdm>>>(data);
            return result.Data;
        }
        /// <summary>
        /// 获取库存盘点单主表 已审核
        /// </summary>
        /// <returns></returns>
        public List<kcpdm> GetKCPDMList()
        {
            string data = Get(ADDRESS_GetKCPDMList);
            ResultDto<List<kcpdm>> result = JsonConvert.DeserializeObject<ResultDto<List<kcpdm>>>(data);
            return result.Data;
        }

        /// <summary>
        /// 获取库存盘点单明细表
        /// </summary>
        /// <returns></returns>
        public List<kcpdd> GetKCPDDList(string code)
        {
            string data = Get(ADDRESS_GetKCPDDList + "?code=" + code);
            ResultDto<List<kcpdd>> result = JsonConvert.DeserializeObject<ResultDto<List<kcpdd>>>(data);
            return result.Data;
        }

        /// <summary>
        /// 删除库存盘点单明细表
        /// </summary>
        /// <returns></returns>
        public bool DelKCPDDList(List<kcpdd> m)
        {
            string data = Post(ADDRESS_DelKCPDDList, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;
        }
        /// <summary>
        /// 删除库存盘点单主表
        /// </summary>
        /// <returns></returns>
        public bool DelKCPDMList(List<kcpdm> m)
        {
            string data = Post(ADDRESS_DelKCPDMList, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;
        }
        /// <summary>
        /// 批量修改库存盘点明细表审核状态
        /// </summary>
        /// <returns></returns>
        public bool UpdateKCPDDList(List<kcpdd> m)
        {
            string data = Post(ADDRESS_UpdateKCPDDList, m);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;
        }

        #endregion

        #region 商品库存

        /// <summary>
        /// 商品业务主表
        /// </summary>
        /// <returns></returns>
        public List<SPKCCX> GetSPKCCXList(spxx m)
        {
            string data = Post(ADDRESS_GetSPKCCXList, m);
            ResultDto<List<SPKCCX>> result = JsonConvert.DeserializeObject<ResultDto<List<SPKCCX>>>(data);
            return result.Data;
        }
        /// <summary>
        /// 商品业务主表
        /// </summary>
        /// <returns></returns>
        public List<SPKCD> GetSPKCDList(spxx m)
        {
            string data = Post(ADDRESS_GetSPKCDList, m);
            ResultDto<List<SPKCD>> result = JsonConvert.DeserializeObject<ResultDto<List<SPKCD>>>(data);
            return result.Data;
        }

        #endregion
        #endregion
        #region 统计报表


        #endregion
        #region 往来帐款

        #region 应收款和应付款登记

        /// <summary>
        /// 应收款
        /// </summary>
        /// <returns></returns>
        public List<yfkdj> GetYFKDJ()
        {
            string data = Get(ADDRESS_GetYFKDJ);
            ResultDto<List<yfkdj>> result = JsonConvert.DeserializeObject<ResultDto<List<yfkdj>>>(data);
            return result.Data;
        }
        /// <summary>
        /// 应付款登记
        /// </summary>
        /// <returns></returns>
        public List<yskdj> GetYSKDJ()
        {
            string data = Get(ADDRESS_GetYSKDJ);
            ResultDto<List<yskdj>> result = JsonConvert.DeserializeObject<ResultDto<List<yskdj>>>(data);
            return result.Data;
        }

        #endregion

        #region 收款和付款登记

        /// <summary>
        /// 生成付款登记单号
        /// </summary>
        /// <returns></returns>
        public string GetFKDJCode()
        {
            string data = Get(ADDRESS_GetFKDJCode);
            ResultDto<string> result = JsonConvert.DeserializeObject<ResultDto<string>>(data);
            return result.Data;
        }
        /// <summary>
        /// 付款登记
        /// </summary>
        /// <returns></returns>
        public List<fkdj> GetFKDJ()
        {
            string data = Get(ADDRESS_GetFKDJ);
            ResultDto<List<fkdj>> result = JsonConvert.DeserializeObject<ResultDto<List<fkdj>>>(data);
            return result.Data;
        }
        /// <summary>
        /// 新增付款登记
        /// </summary>
        /// <returns></returns>
        public bool AddFKDJ(fkdj d)
        {
            string data = Post(ADDRESS_AddFKDJ,d);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;
        }
        /// <summary>
        /// 删除付款登记
        /// </summary>
        /// <returns></returns>
        public bool DeleteFKDJ(fkdj d)
        {
            string data = Post(ADDRESS_DeleteFKDJ,d);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;
        }

        /// <summary>
        /// 修改付款登记
        /// </summary>
        /// <returns></returns>
        public bool UpdateFKDJ(fkdj d)
        {
            string data = Post(ADDRESS_UpdateFKDJ,d);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;
        }
        /// <summary>
        /// 修改付款登记审核状态
        /// </summary>
        /// <returns></returns>
        public bool UpdateFKDJSH(fkdj d)
        {
            string data = Post(ADDRESS_UpdateFKDJSH,d);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;
        }

        /// <summary>
        /// 生成收款登记单号
        /// </summary>
        /// <returns></returns>
        public string GetSKDJCode()
        {
            string data = Get(ADDRESS_GetSKDJCode);
            ResultDto<string> result = JsonConvert.DeserializeObject<ResultDto<string>>(data);
            return result.Data;
        }
        /// <summary>
        /// 收款登记
        /// </summary>
        /// <returns></returns>
        public List<skdj> GetSKDJ()
        {
            string data = Get(ADDRESS_GetSKDJ);
            ResultDto<List<skdj>> result = JsonConvert.DeserializeObject<ResultDto<List<skdj>>>(data);
            return result.Data;
        }
        /// <summary>
        /// 新增收款登记
        /// </summary>
        /// <returns></returns>
        public bool AddSKDJ(skdj d)
        {
            string data = Post(ADDRESS_AddSKDJ,d);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;
        }
        /// <summary>
        /// 删除收款登记
        /// </summary>
        /// <returns></returns>
        public bool DeleteSKDJ(skdj d)
        {
            string data = Post(ADDRESS_DeleteSKDJ, d);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;
        }

        /// <summary>
        /// 修改收款登记
        /// </summary>
        /// <returns></returns>
        public bool UpdateSKDJ(skdj d)
        {
            string data = Post(ADDRESS_UpdateSKDJ, d);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;
        }
        /// <summary>
        /// 修改收款登记审核状态
        /// </summary>
        /// <returns></returns>
        public bool UpdateSKDJSH(skdj d)
        {
            string data = Post(ADDRESS_UpdateSKDJSH, d);
            ResultDto<bool> result = JsonConvert.DeserializeObject<ResultDto<bool>>(data);
            return result.Data;
        }
        #endregion
        #endregion

        #endregion

    }

    public class ResultDto<TResult>
    {
        public string? Msg { get; set; }
        public TResult Data { get; set; }
        public bool Success { get; set; }
    }
}
