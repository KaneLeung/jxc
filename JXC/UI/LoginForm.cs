﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SqlSugar;
using System.Xml.Linq;
using DevExpress.ClipboardSource.SpreadsheetML;
using JXC.Models;
using JXC.Template;
using MD5Hash;
using AutoUpdaterDotNET;
using JXC.Update;
using SqlSugar.Extensions;

namespace JXC.UI
{
    public partial class LoginForm : DevExpress.XtraEditors.XtraForm
    {
        private Point mPoint;

        public LoginForm()
        {
            InitializeComponent();
        }

        private void LoginForm_MouseDown(object sender, MouseEventArgs e)
        {
            mPoint = new Point(e.X, e.Y);
        }

        private void LoginForm_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Location = new Point(this.Location.X + e.X - mPoint.X, this.Location.Y + e.Y - mPoint.Y);
            }
        }

        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            login();
        }

        private void login()
        {
            if (string.IsNullOrEmpty(textEdit1.Text))
            {
                XtraMessageBox.Show("请输入用户名");
                textEdit1.Focus();
                return;
            }
            if (string.IsNullOrEmpty(textEdit2.Text))
            {
                XtraMessageBox.Show("请输入密码");
                textEdit2.Focus();
                return;
            }
            string username = textEdit1.Text;
            string password = textEdit2.Text.GetMD5().ToUpper();
            var http = new APIClient();
            if (http.Login(username, password))
            {
                this.Hide();
                if (new MainForm().ShowDialog() == DialogResult.Cancel)
                {
                    this.Close();
                }
            }

        }
        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            SqlSugarClient db = new SqlSugarClient(new ConnectionConfig()
            {
                ConnectionString = "server=127.0.0.1;port=3306;uid=root;pwd=root;database=jxc"
                ,
                DbType = DbType.MySql,
                IsAutoCloseConnection = true,
                InitKeyType = InitKeyType.Attribute
            });
            db.DbFirst.IsCreateAttribute().CreateClassFile("C:\\Users\\64842\\Desktop\\通用进销存软件\\JXC\\JXC.WebApi\\Entity");
            //查询所有表
            string sqlGetAlltable= "select table_name from information_schema.tables  where TABLE_schema='jxc'";
            var isc = new IServicesCreate();
            var sc = new ServicesCreate();
            var irc = new IRepositoryCreate();
            var rc = new RepositoryCreate();
            var ts = db.Ado.SqlQuery<string>(sqlGetAlltable);
            ts.ForEach(p =>
            {
                isc.Create(p.ToString(), "JXC", "C:\\Users\\64842\\Desktop\\通用进销存软件\\JXC\\JXC.WebApi");
                sc.Create(p.ToString(), "JXC", "C:\\Users\\64842\\Desktop\\通用进销存软件\\JXC\\JXC.WebApi");
                irc.Create(p.ToString(), "JXC", "C:\\Users\\64842\\Desktop\\通用进销存软件\\JXC\\JXC.WebApi");
                rc.Create(p.ToString(), "JXC", "C:\\Users\\64842\\Desktop\\通用进销存软件\\JXC\\JXC.WebApi");
            });
        }

        private void LoginForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                login();
            }
        }

        private void LoginForm_Load(object sender, EventArgs e)
        {
            if (ConfigurationManager.AppSettings["AtuoUpdate"].ObjToBool())
            {
                AutoUpdater.CheckForUpdateEvent += AutoUpdater_CheckForUpdateEvent;
                AutoUpdater.ApplicationExitEvent += AutoUpdater_ApplicationExitEvent;
                AutoUpdater.Start(ConfigurationManager.AppSettings["UpdateUrl"]);
            }

        }
        private void AutoUpdater_ApplicationExitEvent()
        {

        }

        private void AutoUpdater_CheckForUpdateEvent(UpdateInfoEventArgs args)
        {
            if (args != null && new Version(args.CurrentVersion).CompareTo(new Version(ConfigurationManager.AppSettings["Version"])) > 0)
            {
                FrmUpdate frm = new FrmUpdate();
                frm.url = args.ChangelogURL;
                frm.newVsersion = args.CurrentVersion;
                frm.oldVersion = ConfigurationManager.AppSettings["Version"];
                var dialogResult = frm.ShowDialog();
                if (dialogResult == DialogResult.Yes)
                {
                    if (AutoUpdater.DownloadUpdate(args))
                    {
                        Configuration configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                        configuration.AppSettings.Settings["Version"].Value = args.CurrentVersion;
                        configuration.Save(ConfigurationSaveMode.Modified);
                        Application.Exit();
                    }
                }
            }
        }
    }
}