﻿using DevExpress.XtraEditors;
using JXC.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.TextBox;

namespace JXC.UI.Warehouse
{
    public partial class KCPDEditForm : DevExpress.XtraEditors.XtraForm
    {
        public kcpdm data;

        public KCPDEditForm()
        {
            InitializeComponent();
        }

        private void KCPDEditForm_Load(object sender, EventArgs e)
        {
            if (data != null)
            {
                labelControl4.Text = data.Code;
                dateEdit1.DateTime = data.PDRQ.Value;
                textEdit1.Text = data.BZ;
                
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            data.BZ = textEdit1.Text;
            data.PDRQ=dateEdit1.DateTime;
            DialogResult= DialogResult.OK;
            this.Close();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}