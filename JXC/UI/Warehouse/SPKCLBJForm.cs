﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JXC.UI.Warehouse
{
    public partial class SPKCLBJForm : DevExpress.XtraEditors.XtraForm
    {
        private APIClient client=new APIClient();
        public SPKCLBJForm()
        {
            InitializeComponent();
        }

        private void SPKCLBJForm_Load(object sender, EventArgs e)
        {
            gridControl2.DataSource = client.GetSPList().Where(p => p.DQKC < p.KCXX);
        }

        private void simpleButton13_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(textEdit4.Text))
            {
                gridControl2.DataSource = client.GetSPList().Where(p=>p.SPCode.Equals(textEdit4.Text)).Where(p => p.DQKC < p.KCXX);
            }
            else
            {
                gridControl2.DataSource = client.GetSPList().Where(p => p.DQKC < p.KCXX);
            }
        }
    }
}