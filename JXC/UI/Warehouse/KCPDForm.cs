﻿using DevExpress.XtraEditors;
using JXC.Models;
using JXC.WebApi.EntityExtend;
using SqlSugar.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace JXC.UI.Warehouse
{
    public partial class KCPDForm : DevExpress.XtraEditors.XtraForm
    {
        public List<kcpdd> spxsdListR = new List<kcpdd>();
        private APIClient client = new APIClient();

        public KCPDForm()
        {
            InitializeComponent();
        }
        /// <summary>
        /// 商品添加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton3_Click(object sender, EventArgs e)
        {
            var form = new LSPTJForm();

            if (form.ShowDialog() == DialogResult.OK)
            {
                spxsdListR.AddRange(form.kcpddListR);
                gridControl1.DataSource = null;
                gridControl1.DataSource = spxsdListR;
                textEdit1.Text = spxsdListR.Sum(p => p.KCSL).ToString();
                textEdit2.Text = spxsdListR.Sum(p => p.PDSL).ToString();
                gridControl2.DataSource = client.GetKCPDMListN();
                gridControl4.DataSource = client.GetKCPDMList();
                if ((kcpdm)gridView2.GetRow(gridView2.FocusedRowHandle) != null)
                    gridControl3.DataSource = client.GetKCPDDList(((kcpdm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
                if ((kcpdm)gridView4.GetRow(gridView4.FocusedRowHandle) != null)
                    gridControl5.DataSource = client.GetKCPDDList(((kcpdm)gridView4.GetRow(gridView4.FocusedRowHandle)).Code);
            }
            
        }

        private void KCPDForm_Load(object sender, EventArgs e)
        {
            textEdit1.ReadOnly = true;
            labelControl1.Text = $"单号 {client.GetKCPDDCode()}";
            dateEdit1.DateTime = DateTime.Now;
            gridControl2.DataSource = client.GetKCPDMListN();
            gridControl4.DataSource = client.GetKCPDMList();
            if ((kcpdm)gridView2.GetRow(gridView2.FocusedRowHandle) != null)
                gridControl3.DataSource = client.GetKCPDDList(((kcpdm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
            if ((kcpdm)gridView4.GetRow(gridView4.FocusedRowHandle) != null)
                gridControl5.DataSource = client.GetKCPDDList(((kcpdm)gridView4.GetRow(gridView4.FocusedRowHandle)).Code);
        }
        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (gridView1.RowCount == 0)
            {
                XtraMessageBox.Show("没有业务数据");
                return;
            }
            kcpdm m = new kcpdm();
            m.Code = labelControl1.Text.Replace("单号 ", "");
            m.PDRQ = dateEdit1.DateTime;
            m.KCSL = textEdit1.Text.ObjToDecimal();
            m.PDSL = textEdit2.Text.ObjToDecimal();
            m.BZ = textEdit3.Text;
            m.IsSH = false;
            m.CYSL = m.PDSL-m.KCSL;
            m.CZYCode = AppInfo.user.CZYCode;
            m.CZYName=AppInfo.user.CZYName;
            var listD = (List<kcpdd>)gridControl1.DataSource;
            var splist = client.GetSPList();
            listD.ForEach(p =>
            {
                p.MCode = m.Code;
                p.CYSL = p.PDSL - p.KCSL;
                p.HSDJ = splist.FirstOrDefault(n => n.SPCode.Equals(p.SPCode)).CBZE;
                p.YKJE = p.CYSL * p.HSDJ;
            });
            KCPD d = new KCPD();
            d.m = m;
            d.d = listD;
            if (client.AddKCPD(d))
            {
                gridControl1.DataSource = null;
                textEdit1.Text = string.Empty;
                textEdit2.Text = string.Empty;
                textEdit3.Text = string.Empty;
                gridControl2.DataSource = client.GetKCPDMListN();
                if ((kcpdm)gridView2.GetRow(gridView2.FocusedRowHandle) != null)
                    gridControl3.DataSource = client.GetKCPDDList(((kcpdm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
                //gridControl3.DataSource = client.GetCGRKMList();
                if ((kcpdm)gridView4.GetRow(gridView4.FocusedRowHandle) != null)
                    gridControl5.DataSource = client.GetKCPDDList(((kcpdm)gridView4.GetRow(gridView4.FocusedRowHandle)).Code);
                spxsdListR.Clear();
                labelControl1.Text = $"单号 {client.GetKCPDDCode()}";
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton28_Click(object sender, EventArgs e)
        {
            if (gridView1.FocusedRowHandle < 0) return;
            var list = (List<kcpdd>)gridView1.DataSource;
            var data = (kcpdd)gridView1.GetRow(gridView1.FocusedRowHandle);
            PDSPSZForm form = new PDSPSZForm();
            form.odata = data;

            if (form.ShowDialog() == DialogResult.OK)
            {
                int index = list.IndexOf(data);
                list.RemoveRange(index, 1);
                list.InsertRange(index, new List<kcpdd>() { form.odata });
                gridControl1.DataSource = null;
                gridControl1.DataSource = list;
                textEdit1.Text = list.Sum(p => p.KCSL).ToString();
                textEdit2.Text = list.Sum(p => p.PDSL).ToString();
            }
            
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton27_Click(object sender, EventArgs e)
        {
            if (gridView1.FocusedRowHandle < 0)
            {
                return;
            }

            List<kcpdd> list = (List<kcpdd>)gridControl1.DataSource;
            list.Remove((kcpdd)gridView1.GetRow(gridView1.FocusedRowHandle));
            gridControl1.DataSource = null;
            gridControl1.DataSource = list;
        }

        private void gridControl2_Click(object sender, EventArgs e)
        {
            if ((kcpdm)gridView2.GetRow(gridView2.FocusedRowHandle) != null)
                gridControl3.DataSource = client.GetKCPDDList(((kcpdm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
        }

        private void gridControl3_DoubleClick(object sender, EventArgs e)
        {
            if (gridView3.FocusedRowHandle < 0) return;
            var frm = new PDSPSZForm();
            frm.odata = (kcpdd)gridView3.GetRow(gridView3.FocusedRowHandle);

            if (frm.ShowDialog() == DialogResult.OK)
            {
                if (client.UpdateKCPD(new KCPD()
                    {
                        m = (kcpdm)gridView2.GetRow(gridView2.FocusedRowHandle),
                        d = new List<kcpdd>() { frm.odata }
                    }))
                {
                    gridControl2.DataSource = client.GetKCPDMListN();
                    if ((kcpdm)gridView2.GetRow(gridView2.FocusedRowHandle) != null)
                        gridControl3.DataSource = client.GetKCPDDList(((kcpdm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
                }
            }
            
        }
        /// <summary>
        /// 删除单据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton7_Click(object sender, EventArgs e)
        {
            if (gridView2.FocusedRowHandle < 0) return;
            if (client.DelKCPDMList(new List<kcpdm>() { (kcpdm)gridView2.GetRow(gridView2.FocusedRowHandle) }))
            {

                gridControl2.DataSource = client.GetKCPDMListN();
                gridControl3.DataSource = null;
                if (gridView2.FocusedRowHandle >= 0)
                    gridControl3.DataSource = client.GetKCPDDList(((kcpdm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
            }
        }
        /// <summary>
        /// 审核数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton8_Click(object sender, EventArgs e)
        {
            if (gridView2.FocusedRowHandle < 0) return;
            var data = (kcpdm)gridView2.GetRow(gridView2.FocusedRowHandle);
            data.IsSH = true;
            data.SHRName = AppInfo.user.CZYName;
            data.SHRQ = DateTime.Now;
            if (client.UpdateKCPDMSH(new List<kcpdm>()
                {
                    data
                }))
            {
                gridControl2.DataSource = null;
                gridControl2.DataSource = client.GetKCPDMListN();
                gridControl3.DataSource = null;
                if (gridView2.FocusedRowHandle >= 0)
                    gridControl3.DataSource = client.GetKCPDDList(((kcpdm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
                gridControl4.DataSource = null;
                gridControl4.DataSource = client.GetKCPDMList();
                gridControl5.DataSource = null;
                if (gridView4.FocusedRowHandle >= 0)
                    gridControl5.DataSource = client.GetKCPDDList(((kcpdm)gridView4.GetRow(gridView4.FocusedRowHandle)).Code);

            }
        }
        /// <summary>
        /// 修改数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton9_Click(object sender, EventArgs e)
        {
            if (gridView2.FocusedRowHandle < 0) return;

            var frm = new KCPDEditForm();
            frm.data = (kcpdm)gridView2.GetRow(gridView2.FocusedRowHandle);

            if (frm.ShowDialog() == DialogResult.OK)
            {
                if (client.UpdateKCPDM(frm.data))
                {
                    gridControl2.DataSource = client.GetKCPDMListN();
                    gridControl3.DataSource = client.GetKCPDDList(((kcpdm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
                }
            }
            
        }
        /// <summary>
        /// 新增商品
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton15_Click(object sender, EventArgs e)
        {
            var frm = new LSPTJForm();
            frm.code = ((kcpdm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code;
            frm.data = ((kcpdm)gridView2.GetRow(gridView2.FocusedRowHandle));

            if (frm.ShowDialog() == DialogResult.OK)
            {
                gridControl3.DataSource = null;
                gridControl3.DataSource = client.GetKCPDDList(frm.code);
                gridControl2.DataSource = null;
                gridControl2.DataSource = client.GetKCPDMListN();
            }
            
        }
        /// <summary>
        /// 修改商品
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton14_Click(object sender, EventArgs e)
        {
            if (gridView3.FocusedRowHandle < 0) return;
            var frm = new PDSPSZForm();
            frm.odata = (kcpdd)gridView3.GetRow(gridView3.FocusedRowHandle);

            if (frm.ShowDialog() == DialogResult.OK)
            {
                if (client.UpdateKCPD(new KCPD()
                    {
                        m = (kcpdm)gridView2.GetRow(gridView2.FocusedRowHandle),
                        d = new List<kcpdd>() { frm.odata }
                    }))
                {
                    gridControl2.DataSource = client.GetKCPDMListN();
                    if ((kcpdm)gridView2.GetRow(gridView2.FocusedRowHandle) != null)
                        gridControl3.DataSource = client.GetKCPDDList(((kcpdm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
                }
            }
            
        }
        /// <summary>
        /// 删除商品
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton16_Click(object sender, EventArgs e)
        {
            var data = (kcpdd)gridView3.GetRow(gridView3.FocusedRowHandle);
            List<kcpdd> list = new List<kcpdd>();
            list.Add(data);
            client.DelKCPDDList(list);
            gridControl2.DataSource = client.GetKCPDMListN();
            gridControl4.DataSource = client.GetKCPDMList();
            if ((kcpdm)gridView2.GetRow(gridView2.FocusedRowHandle) != null)
                gridControl3.DataSource = client.GetKCPDDList(((kcpdm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
            else
            {
                gridControl3.DataSource = null;
            }
            if ((kcpdm)gridView4.GetRow(gridView4.FocusedRowHandle) != null)
                gridControl5.DataSource = client.GetKCPDDList(((kcpdm)gridView4.GetRow(gridView4.FocusedRowHandle)).Code);
            else
            {
                gridControl5.DataSource = null;
            }
        }
        /// <summary>
        /// 搜索1
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton13_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textEdit4.Text))
            {
                gridControl2.DataSource = client.GetKCPDMListN();

            }
            else
            {
                //todo 优化
                gridControl2.DataSource = client.GetKCPDMListN().Where(p => p.Code.Equals(textEdit4.Text));
            }
            if (gridView2.FocusedRowHandle >= 0)
            {
                gridControl3.DataSource = client.GetKCPDDList(((kcpdm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
            }
            else
            {
                gridControl3.DataSource = null;
            }
        }
        /// <summary>
        /// 取消审核
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton18_Click(object sender, EventArgs e)
        {
            if (gridView4.FocusedRowHandle < 0) return;
            var data = (kcpdm)gridView4.GetRow(gridView4.FocusedRowHandle);
            data.IsSH = false;
            data.SHRName = "";
            data.SHRQ = null;
            if (client.UpdateKCPDMSH(new List<kcpdm>()
                {
                    data
                }))
            {
                gridControl2.DataSource = null;
                gridControl2.DataSource = client.GetKCPDMListN();
                gridControl3.DataSource = null;
                if (gridView2.FocusedRowHandle >= 0)
                    gridControl3.DataSource = client.GetKCPDDList(((kcpdm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
                gridControl4.DataSource = null;
                gridControl4.DataSource = client.GetKCPDMList();
                gridControl5.DataSource = null;
                if (gridView4.FocusedRowHandle >= 0)
                    gridControl5.DataSource = client.GetKCPDDList(((kcpdm)gridView4.GetRow(gridView4.FocusedRowHandle)).Code);

            }
        }
        /// <summary>
        /// 搜索2
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton4_Click(object sender, EventArgs e)
        {

            if (string.IsNullOrEmpty(textEdit5.Text))
            {
                gridControl4.DataSource = client.GetKCPDMList();
            }
            else
            {
                //todo 优化
                gridControl4.DataSource = client.GetKCPDMList().Where(p => p.Code.Equals(textEdit5.Text));
            }
            if (gridView4.FocusedRowHandle >= 0)
            {
                gridControl5.DataSource = client.GetKCPDDList(((kcpdm)gridView4.GetRow(gridView4.FocusedRowHandle)).Code);
            }
            else
            {
                gridControl5.DataSource = null;
            }
        }

        private void gridControl4_Click(object sender, EventArgs e)
        {
            if ((kcpdm)gridView4.GetRow(gridView4.FocusedRowHandle) != null)
                gridControl5.DataSource = client.GetKCPDDList(((kcpdm)gridView4.GetRow(gridView4.FocusedRowHandle)).Code);
        }
    }
}