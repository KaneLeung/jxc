﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using JXC.Models;
using JXC.WebApi.EntityExtend;

namespace JXC.UI.Warehouse
{
    public partial class KCCXForm : XtraForm
    {
        private APIClient client = new();
        public KCCXForm()
        {
            InitializeComponent();
        }

        private void KCCXForm_Load(object sender, EventArgs e)
        {
            gridControl2.DataSource = client.GetSPKCCXList(new spxx(){SPCode = textEdit4.Text});
            if (gridView2.FocusedRowHandle < 0) return;
            var dd = (SPKCCX)gridView2.GetRow(gridView2.FocusedRowHandle);
            spxx x = new spxx()
            {
                SPCode = dd.SPCode,
                SPLBName = dd.SPLB,
            };
            gridControl1.DataSource = client.GetSPKCDList(x);
        }

        private void simpleButton13_Click(object sender, EventArgs e)
        {
            gridControl2.DataSource = client.GetSPKCCXList(new spxx() { SPCode = textEdit4.Text });
            if (gridView2.FocusedRowHandle < 0) return;
            var dd = (SPKCCX)gridView2.GetRow(gridView2.FocusedRowHandle);
            spxx x = new spxx()
            {
                SPCode = dd.SPCode,
                SPLBName = dd.SPLB,
            };
            gridControl1.DataSource = client.GetSPKCDList(x);
        }

        private void gridControl2_Click(object sender, EventArgs e)
        {
           
        }

        private void gridControl2_Click_1(object sender, EventArgs e)
        {
            if (gridView2.FocusedRowHandle < 0) return;
            var dd = (SPKCCX)gridView2.GetRow(gridView2.FocusedRowHandle);
            spxx x = new spxx()
            {
                SPCode = dd.SPCode,
                SPLBName = dd.SPLB,
            };
            gridControl1.DataSource = client.GetSPKCDList(x);
        }
    }
}
