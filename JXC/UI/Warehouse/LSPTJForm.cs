﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JXC.Models;
using JXC.UI.BaseData;
using JXC.WebApi.EntityExtend;
using SqlSugar.Extensions;

namespace JXC.UI.Warehouse
{
    public partial class LSPTJForm : XtraForm
    {
        public List<kcpdd> kcpddListR = new List<kcpdd>();
        public string code = string.Empty;
        public kcpdm data ; 
        private APIClient client=new APIClient();
        public LSPTJForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 增加新商品
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param> 
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            new ProductEditForm().ShowDialog();
        }

        /// <summary>
        /// 加入所选商品
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton2_Click(object sender, EventArgs e)
        {
            var frm = new PDSPSZForm();
            frm.data= (spxx)gridView1.GetRow(gridView1.FocusedRowHandle);

            if (frm.ShowDialog() == DialogResult.OK)
            {
                kcpddListR.Add(frm.odata);
                gridControl2.DataSource = null;
                gridControl2.DataSource = kcpddListR;
            }
           
        }

        private void LSPTJForm_Load(object sender, EventArgs e)
        {
            gridControl1.DataSource = client.GetSPList();
            gridView1.BestFitColumns();
            gridView2.BestFitColumns();
        }

        private void gridControl1_DoubleClick(object sender, EventArgs e)
        {
            var data = (spxx)gridView1.GetRow(gridView1.FocusedRowHandle);
            var form = new PDSPSZForm();
            form.data=data;

            if (form.ShowDialog() == DialogResult.OK)
            {
                kcpddListR.Add(form.odata);
                gridControl2.DataSource = null;
                gridControl2.DataSource = kcpddListR;
            }
            
        }

        private void gridControl2_DoubleClick(object sender, EventArgs e)
        {
            var data = (kcpdd)gridView2.GetRow(gridView2.FocusedRowHandle);
            var form = new PDSPSZForm();
            form.odata = data;

            if (form.ShowDialog() == DialogResult.OK)
            {
                kcpddListR[gridView2.FocusedRowHandle] = (form.odata);
                gridControl2.DataSource = null;
                gridControl2.DataSource = kcpddListR;
            }
           
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            var data = (kcpdd)gridView2.GetRow(gridView2.FocusedRowHandle);
            var form = new PDSPSZForm();
            form.odata = data;

            if (form.ShowDialog() == DialogResult.OK)
            {
                kcpddListR[gridView2.FocusedRowHandle] = (form.odata);
                gridControl2.DataSource = null;
                gridControl2.DataSource = kcpddListR;
            }
       
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            //var data = (cgrkd)gridView2.GetRow(gridView2.FocusedRowHandle);
            //var form = new SPXXForm();
            //form.odata = data;
            //form.ShowDialog();
            kcpddListR.RemoveAt(gridView2.FocusedRowHandle);;
            gridControl2.DataSource = null;
            gridControl2.DataSource = kcpddListR;
        }

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(code))
            {
                var data = (List<kcpdd>)gridView2.DataSource;
                data.ForEach(p =>
                {
                    p.MCode = code;
                });
                var d = new KCPD() { d = data,m=this.data };
                if (client.AddKCPD(d))
                {

                }
            }
            DialogResult= DialogResult.OK;
            this.Close();
        }

        private void simpleButton6_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
