﻿using DevExpress.CodeParser;
using DevExpress.XtraEditors;
using JXC.Models;
using SqlSugar.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.TextBox;

namespace JXC.UI.Warehouse
{
    public partial class PDSPSZForm : DevExpress.XtraEditors.XtraForm
    {
        private APIClient _client = new APIClient();
        public spxx data = null;
        public kcpdd odata = null;
        public PDSPSZForm()
        {
            InitializeComponent();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            var d = new kcpdd();
            d.SPCode = textEdit1.Text;
            d.SPName = textEdit2.Text;
            d.DW = textEdit4.Text;
            d.KCSL = textEdit5.Text.ObjToDecimal();
            d.PDSL = textEdit7.Text.ObjToDecimal();
            d.BZ = textEdit6.Text;
            d.GGXH = textEdit3.Text;
           
            if (odata != null && odata.Id > 0)
            {
                d.Id = odata.Id;
                d.MCode = odata.MCode;
                d.HSDJ=odata.HSDJ;
                d.CYSL = d.PDSL - d.KCSL;
                d.YKJE = d.CYSL * d.HSDJ;
            }

            odata = d;
            DialogResult = DialogResult.OK;
            this.Close();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void PDSPSZForm_Load(object sender, EventArgs e)
        {
            if (data != null)
            {
                textEdit1.Text = data.SPCode;
                textEdit2.Text = data.SPName;
                textEdit3.Text = data.GGXH;
                textEdit4.Text = data.DW;
                textEdit5.Text = data.DQKC.ToString();
                //textEdit5.Text = data.YSSJ.ToString();
                //textEdit8.EditValue = number;
            }
            else
            {
                textEdit1.Text = odata.SPCode;
                textEdit2.Text = odata.SPName;
                textEdit3.Text = odata.GGXH;
                textEdit4.Text = odata.DW;
                textEdit5.Text = odata.KCSL.ToString();
                textEdit7.Text = odata.PDSL.ToString();
                textEdit6.Text = odata.BZ;
            }
        }
    }
}