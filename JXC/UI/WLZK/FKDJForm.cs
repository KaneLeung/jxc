﻿using DevExpress.XtraEditors;
using JXC.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using JXC.Core;

namespace JXC.UI.WLZK
{
    public partial class FKDJForm : DevExpress.XtraEditors.XtraForm
    {
        private APIClient _client = new APIClient();

        public FKDJForm()
        {
            InitializeComponent();
        }

        private void simpleButton6_Click(object sender, EventArgs e)
        {
            var frm = new FKDJEditForm();
            frm.ShowDialog();
            gridControl2.DataSource = _client.GetFKDJ();
        }

        private void FKDJForm_Load(object sender, EventArgs e)
        {
            gridControl2.DataSource = _client.GetFKDJ();
        }

        private void simpleButton7_Click(object sender, EventArgs e)
        {
            if (gridView2.FocusedRowHandle < 0) return;
            var d= (fkdj)gridView2.GetRow(gridView2.FocusedRowHandle);
            if (d.IsSH.Value)
            {
                XtraMessageBox.Show("单据已审核不能修改");
                return;
            }
            var frm = new FKDJEditForm();
            frm.data = d;
            frm.ShowDialog();
            gridControl2.DataSource = _client.GetFKDJ();
        }

        private void simpleButton8_Click(object sender, EventArgs e)
        {
            if (gridView2.FocusedRowHandle < 0) return;
            var d = (fkdj)gridView2.GetRow(gridView2.FocusedRowHandle);
            if (d.IsSH.Value)
            {
                XtraMessageBox.Show("单据已审核不能删除");
                return;
            }
            _client.DeleteFKDJ(d);
            gridControl2.DataSource = _client.GetFKDJ();
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            if (gridView2.FocusedRowHandle < 0) return;
            var d = (fkdj)gridView2.GetRow(gridView2.FocusedRowHandle);
            if (d.IsSH.Value)
            {
                "单据已审核".ShowTips();
                return;

            }
            d.IsSH = true;
            d.SHRName = AppInfo.user.CZYName;

            _client.UpdateFKDJSH(d);
            gridControl2.DataSource = _client.GetFKDJ();
            MessageBox.Show("审核完成");
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(textEdit4.Text))
            {
                gridControl2.DataSource = _client.GetFKDJ().Where(p => p.Code.Equals(textEdit4.Text));

            }
            else
            {
                gridControl2.DataSource = _client.GetFKDJ();
            }
        }
    }
}