﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using JXC.Models;
using JXC.Core;

namespace JXC.UI.WLZK
{
    public partial class SKDJForm : DevExpress.XtraEditors.XtraForm
    {
        private APIClient _client=new APIClient();
        public SKDJForm()
        {
            InitializeComponent();
        }

        private void SKDJForm_Load(object sender, EventArgs e)
        {
            gridControl2.DataSource = _client.GetSKDJ();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(textEdit4.Text))
            {
                gridControl2.DataSource = _client.GetSKDJ().Where(p=>p.Code.Equals(textEdit4.Text));

            }
            else
            {
                gridControl2.DataSource = _client.GetSKDJ();
            }
        }
        /// <summary>
        /// 增加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton6_Click(object sender, EventArgs e)
        {
            var frm = new SKDJEditForm();
            frm.ShowDialog();
            gridControl2.DataSource = _client.GetSKDJ();

        }
        /// <summary>
        /// XIUGAI 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton7_Click(object sender, EventArgs e)
        {
            if (gridView2.FocusedRowHandle < 0) return;
            var d = (skdj)gridView2.GetRow(gridView2.FocusedRowHandle);
            if (d.IsSH.Value)
            {
                XtraMessageBox.Show("单据已审核不能修改");
                return;
            }
            var frm = new SKDJEditForm();

            frm.data = d;
            frm.ShowDialog();
            gridControl2.DataSource = _client.GetSKDJ();
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton8_Click(object sender, EventArgs e)
        {
            if (gridView2.FocusedRowHandle < 0) return;
            var d = (skdj)gridView2.GetRow(gridView2.FocusedRowHandle);
            if (d.IsSH.Value)
            {
                XtraMessageBox.Show("单据已审核不能修改");
                return;
            }
            _client.DeleteSKDJ(d);
            gridControl2.DataSource = _client.GetSKDJ();
        }
        /// <summary>
        /// 审核数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton4_Click(object sender, EventArgs e)
        {
            if (gridView2.FocusedRowHandle < 0) return;
            var d = (skdj)gridView2.GetRow(gridView2.FocusedRowHandle);
            if (d.IsSH.Value)
            {
                "单据已审核".ShowTips();
                return;
                
            }
            d.IsSH = true;
            d.SHRName = AppInfo.user.CZYName;
            
            _client.UpdateSKDJSH(d);
            gridControl2.DataSource = _client.GetSKDJ();
            MessageBox.Show("审核完成");
        }
    }
}