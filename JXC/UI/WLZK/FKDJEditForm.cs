﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using JXC.Models;
using SqlSugar.Extensions;

namespace JXC.UI.WLZK
{
    public partial class FKDJEditForm : DevExpress.XtraEditors.XtraForm
    {
        private APIClient client = new APIClient();
        public fkdj data;
        public FKDJEditForm()
        {
            InitializeComponent();
        }

        private void FKDJEditForm_Load(object sender, EventArgs e)
        {
            textEdit2.KeyDown += TextEdit2_KeyDown;
            textEdit7.EditValue = 0;
            if (data != null)
            {
                textEdit1.Text = data.Code;
                textEdit2.Text = data.SCode;
                textEdit7.Text = data.FKJE.ToString();
                textEdit8.Text = data.BZ;
                var yfk = client.GetYFKDJ().First(p => p.Code.Equals(data.SCode));
                textEdit3.Text = yfk.SCode;
                textEdit4.Text = yfk.YFJE.ToString();
                textEdit5.Text = yfk.GHSName;
                textEdit6.Text = yfk.DJLX;
            }
            else
            {
                textEdit1.Text = client.GetFKDJCode();
            }
        }

        private void TextEdit2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                var yfk = client.GetYFKDJ().First(p => p.Code.Equals(textEdit2.Text));
                textEdit3.Text = yfk.SCode;
                textEdit4.Text = yfk.YFJE.ToString();
                textEdit5.Text = yfk.GHSName;
                textEdit6.Text = yfk.DJLX;
            }
        }

        private void textEdit2_MouseDown(object sender, MouseEventArgs e)
        {

        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textEdit4.Text))
            {
                MessageBox.Show("应付金额不能为空");
                return;
            }
            if (textEdit7.Text.ObjToDecimal() > textEdit4.Text.ObjToDecimal())
            {
                MessageBox.Show("付款金额不能 大于应付款金额");
                return;
            }
            if (textEdit7.Text.ObjToDecimal() <= 0)
            {
                MessageBox.Show("付款金额必须大于0");
                return;
            }
            data = new fkdj();
            data.Code = textEdit1.Text;
            data.SCode = textEdit2.Text;
            data.FKJE = textEdit7.Text.ObjToDecimal();
            data.BZ = textEdit8.Text;
            data.GHSName = textEdit5.Text;
            data.KDRQ = DateTime.Now;
            data.IsSH = false;
            data.CZYName = AppInfo.user.CZYName;
            if (client.AddFKDJ(data))
            {
                textEdit1.Text = client.GetSKDJCode();
                textEdit2.Text = "";
                textEdit7.EditValue = 0;
                textEdit8.Text = "";

                textEdit3.Text = "";
                textEdit4.Text = "";
                textEdit5.Text = "";
                textEdit6.Text = "";
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}