﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.Mvvm.Native;
using DevExpress.XtraReports.UI;
using JXC.Models;
using SqlSugar.Extensions;

namespace JXC.UI.WLZK
{
    public partial class SKDJEditForm : DevExpress.XtraEditors.XtraForm
    {
        private APIClient client=new APIClient();
        public skdj data;
        public SKDJEditForm()
        {
            InitializeComponent();
        }

        private void FKDJEditForm_Load(object sender, EventArgs e)
        {
            textEdit7.EditValue=0;
            simpleButton1.Click += SimpleButton1_Click;
            simpleButton2.Click += SimpleButton2_Click;
            if (data != null)
            {
                textEdit1.Text = data.Code;
                textEdit2.Text = data.SCode;
                textEdit7.Text=data.SKJE.ToString();
                textEdit8.Text = data.BZ;
                var yfk = client.GetYSKDJ().First(p => p.Code.Equals(data.SCode));
                textEdit3.Text = yfk.SCode;
                textEdit4.Text = yfk.YSJE.ToString();
                textEdit5.Text = yfk.KHName;
                textEdit6.Text = yfk.DJLX;
            }
            else
            {
                textEdit1.Text = client.GetSKDJCode();
            }
        }
        /// <summary>
        /// quxiao
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <exception cref="NotImplementedException"></exception>
        private void SimpleButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <exception cref="NotImplementedException"></exception>
        private void SimpleButton1_Click(object sender, EventArgs e)
        {
           
        }

        private void TextEdit2_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                var yfk = client.GetYSKDJ().First(p => p.Code.Equals(textEdit2.Text));
                textEdit3.Text = yfk.SCode;
                textEdit4.Text = yfk.YSJE.ToString();
                textEdit5.Text = yfk.KHName;
                textEdit6.Text = yfk.DJLX;
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textEdit4.Text))
            {
                MessageBox.Show("应收金额不能为空");
                return;
            }
            if (textEdit7.Text.ObjToDecimal()>textEdit4.Text.ObjToDecimal())
            {
                MessageBox.Show("付款金额不能 大于应收款金额");
                return;
            }
            if (textEdit7.Text.ObjToDecimal() <=0)
            {
                MessageBox.Show("付款金额必须大于0");
                return;
            }
            data = new skdj();
            data.Code = textEdit1.Text;
            data.SCode = textEdit2.Text;
            data.SKJE = textEdit7.Text.ObjToDecimal();
            data.BZ = textEdit8.Text;
            data.KHName = textEdit5.Text;
            data.KDRQ=DateTime.Now;
            data.IsSH = false;
            data.CZYName = AppInfo.user.CZYName;
            if (client.AddSKDJ(data))
            {
                textEdit1.Text = client.GetSKDJCode();
                textEdit2.Text = "";
                textEdit7.EditValue = 0;
                textEdit8.Text = "";

                textEdit3.Text = "";
                textEdit4.Text = "";
                textEdit5.Text = "";
                textEdit6.Text = "";
            }
        }
    }
}