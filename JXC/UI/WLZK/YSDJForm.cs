﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JXC.UI.WLZK
{
    public partial class YSDJForm : DevExpress.XtraEditors.XtraForm
    {
        private APIClient client = new APIClient();

        public YSDJForm()
        {
            InitializeComponent();
        }

        private void YSDJForm_Load(object sender, EventArgs e)
        {
            gridControl2.DataSource = client.GetYSKDJ().Where(p => p.YSJE > 0);
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(textEdit4.Text))
            {
                gridControl2.DataSource = client.GetYSKDJ().Where(p=>p.Code.Equals(textEdit4.Text));
            }
            else
            {
                gridControl2.DataSource = client.GetYSKDJ().Where(p => p.YSJE > 0) ;

            }
        }
    }
}