﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace JXC.UI.WLZK
{
    public partial class YFKDJForm : XtraForm
    {
        private APIClient client = new APIClient();
        public YFKDJForm()
        {
            InitializeComponent();
        }

        private void YFKDJForm_Load(object sender, EventArgs e)
        {
            gridControl2.DataSource = client.GetYFKDJ().Where(p=>p.YFJE>0);
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(textEdit4.Text))
            {
                gridControl2.DataSource = client.GetYFKDJ().Where(p => p.Code.Equals(textEdit4.Text));
            }
            else
            {
                gridControl2.DataSource = client.GetYFKDJ().Where(p => p.YFJE > 0);

            }
        }
    }
}
