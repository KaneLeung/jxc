﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using JXC.UI.BaseData;
using JXC.UI.Main;
using DevExpress.XtraSplashScreen;
using JXC.Core;
using JXC.Models;
using JXC.UI.InWarehouse;
using JXC.UI.OutWarehouse;
using JXC.UI.Reports;
using JXC.UI.SystemSetup;
using JXC.UI.Warehouse;
using JXC.UI.WLZK;

namespace JXC.UI
{
    public partial class MainForm : DevExpress.XtraEditors.XtraForm
    {
        private string[] jichushujuList={"btn1", "btn2", "btn3"};
        private string[] rukuguanliList = { "btn14", "btn15",  "btn18", "btn19" };//"btn16", "btn17",
        private string[] chukuguanliList = { "btn20", "btn21",  "btn24", "btn25" };//"btn22", "btn23",
        private string[] tongjibaobiaoList = { "btn31", "btn26", "btn27", "btn28", "btn29", "btn30" };
        private string[] wanglaizhangkuanList = { "btn32", "btn33", "btn34", "btn35" };//, "btn36", "btn37"
        private string[] xitongshezhiList = { "btn5", "btn6", "btn7",  "btn41", "btn42" };//"btn4", "btn8", "btn9", "btn10", "btn11", "btn12", "btn13",
        private string[] kucunguanliList = { "btn38", "btn39", "btn40" };


        private APIClient client=new APIClient();
        private List<jsgn> flist=new List<jsgn>();
        public MainForm()
        {
            InitializeComponent();
        }
        private void MainForm_Load(object sender, EventArgs e)
        {

            flist = client.GetJSGNList(client.GetJSGLList().FirstOrDefault(p=>p.JSName.Equals(AppInfo.user.SSJSName)).Id);
            xtraTabPage2.Visible = true;
            labelControl1.Text = $"操作员:{AppInfo.user.CZYName}";
        }
        private void simpleButton4_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == "确定退出系统？".ShowYesNoAndTips())
            {
                Application.Exit();
            }
        }

        private void simpleButton11_Click_1(object sender, EventArgs e)
        {
            new SkinForm().ShowDialog();
        }
        /// <summary>
        /// 统计报表
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton7_Click(object sender, EventArgs e)
        {
            SetTabPage(5);
        }
        /// <summary>
        /// 入库管理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton10_Click(object sender, EventArgs e)
        {
            SetTabPage(2);
        }
        /// <summary>
        /// 出库管理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton5_Click(object sender, EventArgs e)
        {
            SetTabPage(3);
        }
        /// <summary>
        /// 库存管理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton6_Click(object sender, EventArgs e)
        {
            SetTabPage(4);

        }
        /// <summary>
        /// 往来帐款
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton8_Click(object sender, EventArgs e)
        {
            SetTabPage(6);

        }
        /// <summary>
        /// 系统设置
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton9_Click(object sender, EventArgs e)
        {
            SetTabPage(7);
        }
        /// <summary>
        /// 基础数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton12_Click(object sender, EventArgs e)
        {
            SetTabPage(1);
        }

        /// <summary>
        /// 根据导航栏点击设置tabpage
        /// </summary>
        /// <param name="index"></param>
        private void SetTabPage(int index)
        {
            SplashScreenManager.ShowForm(typeof(LoadingForm));
            xtraTabPage2.PageVisible=true;
            xtraTabControl1.SelectedTabPageIndex = 1;
            switch (index)
            {
                case 1:
                    xtraTabControl1.TabPages[1].Text = "基础数据";
                    SetFunction(jichushujuList);
                    break;
                case 2:
                    xtraTabControl1.TabPages[1].Text = "入库管理"; 
                    SetFunction(rukuguanliList);
                    break;
                case 3:
                    xtraTabControl1.TabPages[1].Text = "出库管理";
                    SetFunction(chukuguanliList);
                    break;
                case 4:
                    xtraTabControl1.TabPages[1].Text = "库存管理";
                    SetFunction(kucunguanliList);
                    break;
                case 5:
                    xtraTabControl1.TabPages[1].Text = "统计报表";
                    SetFunction(tongjibaobiaoList);
                    break;
                case 6:
                    xtraTabControl1.TabPages[1].Text = "往来帐款";
                    SetFunction(wanglaizhangkuanList);
                    break;
                case 7:
                    xtraTabControl1.TabPages[1].Text = "系统设置";
                    SetFunction(xitongshezhiList);
                    break;
            }
            SplashScreenManager.CloseForm();
        }

        /// <summary>
        /// 点击导航栏 设置功能
        /// </summary>
        /// <param name="index"></param>
        private void SetFunction(string[] list)
        {
            
            foreach (Control control in flowLayoutPanel3.Controls)
            {
                control.Visible = false;
            }
            foreach (string item in list)
            {
                foreach (var control in flowLayoutPanel3.Controls.Find(item, false))
                {
                    control.Visible = flist.Select(p=>p.GNBM).Contains(control.Name);
                }
            }
        }
        /// <summary>
        /// 商品管理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton13_Click(object sender, EventArgs e)
        {
            new ProductForm().ShowDialog();
        }

        /// <summary>
        /// 发货成本
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton42_Click(object sender, EventArgs e)
        {
            new FHCBTJForm().ShowDialog();
        }
        /// <summary>
        /// 供应商管理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn2_Click(object sender, EventArgs e)
        {
            new GYSForm().ShowDialog();
        }

        /// <summary>
        /// 客户管理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn3_Click(object sender, EventArgs e)
        {
            new KHForm().ShowDialog();
        }
        /// <summary>
        /// 采购入库
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn14_Click(object sender, EventArgs e)
        {
            new CGRKForm().ShowDialog();
        }

        /// <summary>
        /// 时间
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer1_Tick(object sender, EventArgs e)
        {
            labelControl2.Text = DateTime.Now.ToString();
        }
        /// <summary>
        /// 采购退货
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn15_Click(object sender, EventArgs e)
        {
            new CGTHForm().ShowDialog();
        }

        /// <summary>
        /// 入库统计
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn18_Click(object sender, EventArgs e)
        {
            new RKTJBForm().ShowDialog();
        }

        /// <summary>
        /// 退货统计
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn19_Click(object sender, EventArgs e)
        {
            new CTHTJBForm().ShowDialog();
        }

        /// <summary>
        /// 商品销售
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn20_Click(object sender, EventArgs e)
        {
            new SPXSForm().ShowDialog();
        }

        /// <summary>
        /// 顾客退货
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn21_Click(object sender, EventArgs e)
        {
            new GKTHForm().ShowDialog();
        }

        /// <summary>
        /// 出库统计
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn24_Click(object sender, EventArgs e)
        {
            new CKTJBForm().ShowDialog();
        }

        /// <summary>
        /// 退库统计
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn25_Click(object sender, EventArgs e)
        {
            new XTHTJBForm().ShowDialog();
        }

        /// <summary>
        /// 进销存明细
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn26_Click(object sender, EventArgs e)
        {
            new JXCMXBForm().ShowDialog();
        }

        /// <summary>
        /// 进销存汇总
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn27_Click(object sender, EventArgs e)
        {
            new JXCHZBForm().ShowDialog();
        }

        /// <summary>
        /// 收货对账单
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn28_Click(object sender, EventArgs e)
        {
            new SHDZDForm().ShowDialog();
        }

        /// <summary>
        /// 发货对账单
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn29_Click(object sender, EventArgs e)
        {
            new FHDZDForm().ShowDialog();
        }

        /// <summary>
        /// 销售毛利
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn31_Click(object sender, EventArgs e)
        {
            new XSMLTJBForm().ShowDialog();
        }

        /// <summary>
        /// 应收登记
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn32_Click(object sender, EventArgs e)
        {
            new YSDJForm().ShowDialog();
        }

        /// <summary>
        /// 应付登记
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn33_Click(object sender, EventArgs e)
        {
            new YFKDJForm().ShowDialog();
        }

        /// <summary>
        /// 收款登记
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn34_Click(object sender, EventArgs e)
        {
            new SKDJForm().ShowDialog();
        }
        /// <summary>
        /// 付款登记
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn35_Click(object sender, EventArgs e)
        {
            new FKDJForm().ShowDialog();
        }

        /// <summary>
        /// 应收帐表
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn36_Click(object sender, EventArgs e)
        {
            new YSKGLForm().ShowDialog();
        }
        /// <summary>
        /// 应付帐表
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn37_Click(object sender, EventArgs e)
        {
            new YFKGLForm().ShowDialog();
        }

        /// <summary>
        /// 库存盘点
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn38_Click(object sender, EventArgs e)
        {
            new KCPDForm().ShowDialog();
        }

        /// <summary>
        /// 商品库存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn39_Click(object sender, EventArgs e)
        {
            new KCCXForm().ShowDialog();
        }

        /// <summary>
        /// 商品库存量报警
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn40_Click(object sender, EventArgs e)
        {
            new SPKCLBJForm().ShowDialog();
        }


        /// <summary>
        /// 参数设置
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn5_Click(object sender, EventArgs e)
        {
            new SystemSetupForm().ShowDialog();
        }

        /// <summary>
        /// 操作员设置
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn6_Click(object sender, EventArgs e)
        {
            new CZYSZForm().ShowDialog();
        }

        /// <summary>
        /// 角色管理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn7_Click(object sender, EventArgs e)
        {
            new JSGLForm().ShowDialog();
        }
        /// <summary>
        /// 字典类型
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton15_Click(object sender, EventArgs e)
        {
            new DicTypeForm().ShowDialog();
        }
        /// <summary>
        /// 字典数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton16_Click(object sender, EventArgs e)
        {
            new DicDataForm().ShowDialog();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (DialogResult.Yes == "确定退出系统？".ShowYesNoAndTips())
                {
                    Application.Exit();
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
           
        }

        private void simpleButton4_Click_1(object sender, EventArgs e)
        {
            if (DialogResult.Yes == "确定退出系统？".ShowYesNoAndTips())
            {
                Application.Exit();
            }
        }
       
        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton2_Click(object sender, EventArgs e)
        {
            new PWDEditForm().ShowDialog();
        }
    }
}