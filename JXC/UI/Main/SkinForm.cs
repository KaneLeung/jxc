﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors.Controls;
using DevExpress.Xpf.Core;

namespace JXC.UI.Main
{
    public partial class SkinForm : DevExpress.XtraEditors.XtraForm
    {
        private string skinName;
        public SkinForm()
        {
            InitializeComponent();
        }

        private void frmSkins_Load(object sender, EventArgs e)
        {
            comboBoxEdit1.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
            skinName= DevExpress.LookAndFeel.UserLookAndFeel.Default.ActiveSkinName;
            comboBoxEdit1.EditValue= DevExpress.LookAndFeel.UserLookAndFeel.Default.ActiveSkinName;
            // 循环添加皮肤名称
            foreach (DevExpress.Skins.SkinContainer skin in DevExpress.Skins.SkinManager.Default.Skins)
                comboBoxEdit1.Properties.Items.Add(skin.SkinName);
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            DevExpress.LookAndFeel.UserLookAndFeel.Default.SetSkinStyle(skinName);
            this.Close();
        }

        private void comboBoxEdit1_SelectedIndexChanged(object sender, EventArgs e)
        {
            DevExpress.LookAndFeel.UserLookAndFeel.Default.SetSkinStyle(comboBoxEdit1.EditValue.ToString());
        }
    }
}