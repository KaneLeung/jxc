﻿namespace JXC.UI
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton11 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.simpleButton14 = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton13 = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn1 = new DevExpress.XtraEditors.SimpleButton();
            this.btn2 = new DevExpress.XtraEditors.SimpleButton();
            this.btn3 = new DevExpress.XtraEditors.SimpleButton();
            this.btn4 = new DevExpress.XtraEditors.SimpleButton();
            this.btn5 = new DevExpress.XtraEditors.SimpleButton();
            this.btn6 = new DevExpress.XtraEditors.SimpleButton();
            this.btn7 = new DevExpress.XtraEditors.SimpleButton();
            this.btn8 = new DevExpress.XtraEditors.SimpleButton();
            this.btn9 = new DevExpress.XtraEditors.SimpleButton();
            this.btn10 = new DevExpress.XtraEditors.SimpleButton();
            this.btn11 = new DevExpress.XtraEditors.SimpleButton();
            this.btn12 = new DevExpress.XtraEditors.SimpleButton();
            this.btn13 = new DevExpress.XtraEditors.SimpleButton();
            this.btn14 = new DevExpress.XtraEditors.SimpleButton();
            this.btn15 = new DevExpress.XtraEditors.SimpleButton();
            this.btn16 = new DevExpress.XtraEditors.SimpleButton();
            this.btn17 = new DevExpress.XtraEditors.SimpleButton();
            this.btn18 = new DevExpress.XtraEditors.SimpleButton();
            this.btn19 = new DevExpress.XtraEditors.SimpleButton();
            this.btn20 = new DevExpress.XtraEditors.SimpleButton();
            this.btn21 = new DevExpress.XtraEditors.SimpleButton();
            this.btn22 = new DevExpress.XtraEditors.SimpleButton();
            this.btn23 = new DevExpress.XtraEditors.SimpleButton();
            this.btn24 = new DevExpress.XtraEditors.SimpleButton();
            this.btn25 = new DevExpress.XtraEditors.SimpleButton();
            this.btn26 = new DevExpress.XtraEditors.SimpleButton();
            this.btn27 = new DevExpress.XtraEditors.SimpleButton();
            this.btn28 = new DevExpress.XtraEditors.SimpleButton();
            this.btn29 = new DevExpress.XtraEditors.SimpleButton();
            this.btn30 = new DevExpress.XtraEditors.SimpleButton();
            this.btn31 = new DevExpress.XtraEditors.SimpleButton();
            this.btn32 = new DevExpress.XtraEditors.SimpleButton();
            this.btn33 = new DevExpress.XtraEditors.SimpleButton();
            this.btn34 = new DevExpress.XtraEditors.SimpleButton();
            this.btn35 = new DevExpress.XtraEditors.SimpleButton();
            this.btn36 = new DevExpress.XtraEditors.SimpleButton();
            this.btn37 = new DevExpress.XtraEditors.SimpleButton();
            this.btn38 = new DevExpress.XtraEditors.SimpleButton();
            this.btn39 = new DevExpress.XtraEditors.SimpleButton();
            this.btn40 = new DevExpress.XtraEditors.SimpleButton();
            this.btn41 = new DevExpress.XtraEditors.SimpleButton();
            this.btn42 = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.simpleButton12 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton10 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton6 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton7 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton8 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton9 = new DevExpress.XtraEditors.SimpleButton();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage2.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.flowLayoutPanel1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Margin = new System.Windows.Forms.Padding(4);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1498, 66);
            this.panelControl1.TabIndex = 0;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.simpleButton2);
            this.flowLayoutPanel1.Controls.Add(this.simpleButton3);
            this.flowLayoutPanel1.Controls.Add(this.simpleButton11);
            this.flowLayoutPanel1.Controls.Add(this.simpleButton4);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(2, 2);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1494, 62);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // simpleButton2
            // 
            this.simpleButton2.Appearance.Font = new System.Drawing.Font("黑体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.simpleButton2.Appearance.Options.UseFont = true;
            this.simpleButton2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.simpleButton2.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.simpleButton2.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("simpleButton2.ImageOptions.SvgImage")));
            this.simpleButton2.Location = new System.Drawing.Point(6, 4);
            this.simpleButton2.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.simpleButton2.Size = new System.Drawing.Size(75, 56);
            this.simpleButton2.TabIndex = 1;
            this.simpleButton2.Text = "修改密码";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // simpleButton3
            // 
            this.simpleButton3.Appearance.Font = new System.Drawing.Font("黑体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.simpleButton3.Appearance.Options.UseFont = true;
            this.simpleButton3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.simpleButton3.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.simpleButton3.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("simpleButton3.ImageOptions.SvgImage")));
            this.simpleButton3.Location = new System.Drawing.Point(93, 4);
            this.simpleButton3.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.simpleButton3.Size = new System.Drawing.Size(75, 56);
            this.simpleButton3.TabIndex = 2;
            this.simpleButton3.Text = "软件帮助";
            this.simpleButton3.Visible = false;
            // 
            // simpleButton11
            // 
            this.simpleButton11.Appearance.Font = new System.Drawing.Font("黑体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.simpleButton11.Appearance.Options.UseFont = true;
            this.simpleButton11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.simpleButton11.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.simpleButton11.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("simpleButton11.ImageOptions.SvgImage")));
            this.simpleButton11.Location = new System.Drawing.Point(180, 4);
            this.simpleButton11.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.simpleButton11.Name = "simpleButton11";
            this.simpleButton11.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.simpleButton11.Size = new System.Drawing.Size(75, 56);
            this.simpleButton11.TabIndex = 4;
            this.simpleButton11.Text = "更换皮肤";
            this.simpleButton11.Click += new System.EventHandler(this.simpleButton11_Click_1);
            // 
            // simpleButton4
            // 
            this.simpleButton4.Appearance.Font = new System.Drawing.Font("黑体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.simpleButton4.Appearance.Options.UseFont = true;
            this.simpleButton4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.simpleButton4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton4.ImageOptions.Image")));
            this.simpleButton4.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.simpleButton4.Location = new System.Drawing.Point(267, 4);
            this.simpleButton4.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.simpleButton4.Size = new System.Drawing.Size(75, 56);
            this.simpleButton4.TabIndex = 5;
            this.simpleButton4.Text = "退出系统";
            this.simpleButton4.Click += new System.EventHandler(this.simpleButton4_Click_1);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.simpleButton14);
            this.panel1.Controls.Add(this.labelControl2);
            this.panel1.Controls.Add(this.labelControl1);
            this.panel1.Controls.Add(this.simpleButton13);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 742);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1498, 26);
            this.panel1.TabIndex = 1;
            // 
            // simpleButton14
            // 
            this.simpleButton14.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton14.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton14.ImageOptions.Image")));
            this.simpleButton14.Location = new System.Drawing.Point(1342, 5);
            this.simpleButton14.Name = "simpleButton14";
            this.simpleButton14.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.simpleButton14.Size = new System.Drawing.Size(23, 19);
            this.simpleButton14.TabIndex = 3;
            // 
            // labelControl2
            // 
            this.labelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl2.Location = new System.Drawing.Point(1370, 7);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(70, 14);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "labelControl2";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(33, 7);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(72, 14);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "操作员:admin";
            // 
            // simpleButton13
            // 
            this.simpleButton13.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton13.ImageOptions.Image")));
            this.simpleButton13.Location = new System.Drawing.Point(8, 4);
            this.simpleButton13.Name = "simpleButton13";
            this.simpleButton13.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.simpleButton13.Size = new System.Drawing.Size(23, 19);
            this.simpleButton13.TabIndex = 0;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl4);
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 66);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(4);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1498, 676);
            this.panelControl2.TabIndex = 2;
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.groupControl2);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(165, 2);
            this.panelControl4.Margin = new System.Windows.Forms.Padding(4);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(1331, 672);
            this.panelControl4.TabIndex = 1;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.xtraTabControl1);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl2.Location = new System.Drawing.Point(2, 2);
            this.groupControl2.Margin = new System.Windows.Forms.Padding(4);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(1327, 668);
            this.groupControl2.TabIndex = 0;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.AppearancePage.Header.Font = new System.Drawing.Font("黑体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.xtraTabControl1.AppearancePage.Header.Options.UseFont = true;
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(2, 23);
            this.xtraTabControl1.Margin = new System.Windows.Forms.Padding(4);
            this.xtraTabControl1.MultiLine = DevExpress.Utils.DefaultBoolean.False;
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(1323, 643);
            this.xtraTabControl1.TabIndex = 0;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Margin = new System.Windows.Forms.Padding(4);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(1321, 619);
            this.xtraTabPage1.Text = "主页";
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.flowLayoutPanel3);
            this.xtraTabPage2.Margin = new System.Windows.Forms.Padding(4);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.PageVisible = false;
            this.xtraTabPage2.Size = new System.Drawing.Size(1321, 619);
            this.xtraTabPage2.Text = "xtraTabPage2";
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Controls.Add(this.btn1);
            this.flowLayoutPanel3.Controls.Add(this.btn2);
            this.flowLayoutPanel3.Controls.Add(this.btn3);
            this.flowLayoutPanel3.Controls.Add(this.btn4);
            this.flowLayoutPanel3.Controls.Add(this.btn5);
            this.flowLayoutPanel3.Controls.Add(this.btn6);
            this.flowLayoutPanel3.Controls.Add(this.btn7);
            this.flowLayoutPanel3.Controls.Add(this.btn8);
            this.flowLayoutPanel3.Controls.Add(this.btn9);
            this.flowLayoutPanel3.Controls.Add(this.btn10);
            this.flowLayoutPanel3.Controls.Add(this.btn11);
            this.flowLayoutPanel3.Controls.Add(this.btn12);
            this.flowLayoutPanel3.Controls.Add(this.btn13);
            this.flowLayoutPanel3.Controls.Add(this.btn14);
            this.flowLayoutPanel3.Controls.Add(this.btn15);
            this.flowLayoutPanel3.Controls.Add(this.btn16);
            this.flowLayoutPanel3.Controls.Add(this.btn17);
            this.flowLayoutPanel3.Controls.Add(this.btn18);
            this.flowLayoutPanel3.Controls.Add(this.btn19);
            this.flowLayoutPanel3.Controls.Add(this.btn20);
            this.flowLayoutPanel3.Controls.Add(this.btn21);
            this.flowLayoutPanel3.Controls.Add(this.btn22);
            this.flowLayoutPanel3.Controls.Add(this.btn23);
            this.flowLayoutPanel3.Controls.Add(this.btn24);
            this.flowLayoutPanel3.Controls.Add(this.btn25);
            this.flowLayoutPanel3.Controls.Add(this.btn26);
            this.flowLayoutPanel3.Controls.Add(this.btn27);
            this.flowLayoutPanel3.Controls.Add(this.btn28);
            this.flowLayoutPanel3.Controls.Add(this.btn29);
            this.flowLayoutPanel3.Controls.Add(this.btn30);
            this.flowLayoutPanel3.Controls.Add(this.btn31);
            this.flowLayoutPanel3.Controls.Add(this.btn32);
            this.flowLayoutPanel3.Controls.Add(this.btn33);
            this.flowLayoutPanel3.Controls.Add(this.btn34);
            this.flowLayoutPanel3.Controls.Add(this.btn35);
            this.flowLayoutPanel3.Controls.Add(this.btn36);
            this.flowLayoutPanel3.Controls.Add(this.btn37);
            this.flowLayoutPanel3.Controls.Add(this.btn38);
            this.flowLayoutPanel3.Controls.Add(this.btn39);
            this.flowLayoutPanel3.Controls.Add(this.btn40);
            this.flowLayoutPanel3.Controls.Add(this.btn41);
            this.flowLayoutPanel3.Controls.Add(this.btn42);
            this.flowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel3.Margin = new System.Windows.Forms.Padding(4);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(1321, 619);
            this.flowLayoutPanel3.TabIndex = 0;
            // 
            // btn1
            // 
            this.btn1.Appearance.Font = new System.Drawing.Font("黑体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn1.Appearance.Options.UseFont = true;
            this.btn1.AppearancePressed.BackColor = System.Drawing.Color.White;
            this.btn1.AppearancePressed.Options.UseBackColor = true;
            this.btn1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn1.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btn1.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btn1.ImageOptions.SvgImage")));
            this.btn1.Location = new System.Drawing.Point(10, 4);
            this.btn1.Margin = new System.Windows.Forms.Padding(10, 4, 6, 4);
            this.btn1.Name = "btn1";
            this.btn1.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btn1.Size = new System.Drawing.Size(75, 56);
            this.btn1.TabIndex = 1;
            this.btn1.Text = "商品管理";
            this.btn1.Click += new System.EventHandler(this.simpleButton13_Click);
            // 
            // btn2
            // 
            this.btn2.Appearance.Font = new System.Drawing.Font("黑体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn2.Appearance.Options.UseFont = true;
            this.btn2.AppearancePressed.BackColor = System.Drawing.Color.White;
            this.btn2.AppearancePressed.Options.UseBackColor = true;
            this.btn2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn2.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btn2.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btn2.ImageOptions.SvgImage")));
            this.btn2.Location = new System.Drawing.Point(101, 4);
            this.btn2.Margin = new System.Windows.Forms.Padding(10, 4, 6, 4);
            this.btn2.Name = "btn2";
            this.btn2.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btn2.Size = new System.Drawing.Size(85, 56);
            this.btn2.TabIndex = 2;
            this.btn2.Text = "供货商管理";
            this.btn2.Click += new System.EventHandler(this.btn2_Click);
            // 
            // btn3
            // 
            this.btn3.Appearance.Font = new System.Drawing.Font("黑体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn3.Appearance.Options.UseFont = true;
            this.btn3.AppearancePressed.BackColor = System.Drawing.Color.White;
            this.btn3.AppearancePressed.Options.UseBackColor = true;
            this.btn3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn3.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btn3.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btn3.ImageOptions.SvgImage")));
            this.btn3.Location = new System.Drawing.Point(202, 4);
            this.btn3.Margin = new System.Windows.Forms.Padding(10, 4, 6, 4);
            this.btn3.Name = "btn3";
            this.btn3.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btn3.Size = new System.Drawing.Size(75, 56);
            this.btn3.TabIndex = 3;
            this.btn3.Text = "客户管理";
            this.btn3.Click += new System.EventHandler(this.btn3_Click);
            // 
            // btn4
            // 
            this.btn4.Appearance.Font = new System.Drawing.Font("黑体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn4.Appearance.Options.UseFont = true;
            this.btn4.AppearancePressed.BackColor = System.Drawing.Color.White;
            this.btn4.AppearancePressed.Options.UseBackColor = true;
            this.btn4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn4.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btn4.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btn4.ImageOptions.SvgImage")));
            this.btn4.Location = new System.Drawing.Point(293, 4);
            this.btn4.Margin = new System.Windows.Forms.Padding(10, 4, 6, 4);
            this.btn4.Name = "btn4";
            this.btn4.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btn4.Size = new System.Drawing.Size(75, 56);
            this.btn4.TabIndex = 4;
            this.btn4.Text = "部门管理";
            this.btn4.Visible = false;
            // 
            // btn5
            // 
            this.btn5.Appearance.Font = new System.Drawing.Font("黑体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn5.Appearance.Options.UseFont = true;
            this.btn5.AppearancePressed.BackColor = System.Drawing.Color.White;
            this.btn5.AppearancePressed.Options.UseBackColor = true;
            this.btn5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn5.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btn5.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btn5.ImageOptions.SvgImage")));
            this.btn5.Location = new System.Drawing.Point(384, 4);
            this.btn5.Margin = new System.Windows.Forms.Padding(10, 4, 6, 4);
            this.btn5.Name = "btn5";
            this.btn5.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btn5.Size = new System.Drawing.Size(75, 56);
            this.btn5.TabIndex = 5;
            this.btn5.Text = "参数设置";
            this.btn5.Click += new System.EventHandler(this.btn5_Click);
            // 
            // btn6
            // 
            this.btn6.Appearance.Font = new System.Drawing.Font("黑体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn6.Appearance.Options.UseFont = true;
            this.btn6.AppearancePressed.BackColor = System.Drawing.Color.White;
            this.btn6.AppearancePressed.Options.UseBackColor = true;
            this.btn6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn6.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btn6.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btn6.ImageOptions.SvgImage")));
            this.btn6.Location = new System.Drawing.Point(475, 4);
            this.btn6.Margin = new System.Windows.Forms.Padding(10, 4, 6, 4);
            this.btn6.Name = "btn6";
            this.btn6.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btn6.Size = new System.Drawing.Size(85, 56);
            this.btn6.TabIndex = 6;
            this.btn6.Text = "操作员设置";
            this.btn6.Click += new System.EventHandler(this.btn6_Click);
            // 
            // btn7
            // 
            this.btn7.Appearance.Font = new System.Drawing.Font("黑体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn7.Appearance.Options.UseFont = true;
            this.btn7.AppearancePressed.BackColor = System.Drawing.Color.White;
            this.btn7.AppearancePressed.Options.UseBackColor = true;
            this.btn7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn7.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btn7.ImageOptions.Image")));
            this.btn7.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btn7.Location = new System.Drawing.Point(576, 4);
            this.btn7.Margin = new System.Windows.Forms.Padding(10, 4, 6, 4);
            this.btn7.Name = "btn7";
            this.btn7.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btn7.Size = new System.Drawing.Size(75, 56);
            this.btn7.TabIndex = 7;
            this.btn7.Text = "角色管理";
            this.btn7.Click += new System.EventHandler(this.btn7_Click);
            // 
            // btn8
            // 
            this.btn8.Appearance.Font = new System.Drawing.Font("黑体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn8.Appearance.Options.UseFont = true;
            this.btn8.AppearancePressed.BackColor = System.Drawing.Color.White;
            this.btn8.AppearancePressed.Options.UseBackColor = true;
            this.btn8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn8.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btn8.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btn8.ImageOptions.SvgImage")));
            this.btn8.Location = new System.Drawing.Point(667, 4);
            this.btn8.Margin = new System.Windows.Forms.Padding(10, 4, 6, 4);
            this.btn8.Name = "btn8";
            this.btn8.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btn8.Size = new System.Drawing.Size(75, 56);
            this.btn8.TabIndex = 8;
            this.btn8.Text = "账套管理";
            this.btn8.Visible = false;
            // 
            // btn9
            // 
            this.btn9.Appearance.Font = new System.Drawing.Font("黑体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn9.Appearance.Options.UseFont = true;
            this.btn9.AppearancePressed.BackColor = System.Drawing.Color.White;
            this.btn9.AppearancePressed.Options.UseBackColor = true;
            this.btn9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn9.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btn9.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btn9.ImageOptions.SvgImage")));
            this.btn9.Location = new System.Drawing.Point(758, 4);
            this.btn9.Margin = new System.Windows.Forms.Padding(10, 4, 6, 4);
            this.btn9.Name = "btn9";
            this.btn9.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btn9.Size = new System.Drawing.Size(75, 56);
            this.btn9.TabIndex = 9;
            this.btn9.Text = "报表设计";
            this.btn9.Visible = false;
            // 
            // btn10
            // 
            this.btn10.Appearance.Font = new System.Drawing.Font("黑体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn10.Appearance.Options.UseFont = true;
            this.btn10.AppearancePressed.BackColor = System.Drawing.Color.White;
            this.btn10.AppearancePressed.Options.UseBackColor = true;
            this.btn10.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn10.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btn10.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btn10.ImageOptions.SvgImage")));
            this.btn10.Location = new System.Drawing.Point(849, 4);
            this.btn10.Margin = new System.Windows.Forms.Padding(10, 4, 6, 4);
            this.btn10.Name = "btn10";
            this.btn10.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btn10.Size = new System.Drawing.Size(75, 56);
            this.btn10.TabIndex = 10;
            this.btn10.Text = "数据备份";
            this.btn10.Visible = false;
            // 
            // btn11
            // 
            this.btn11.Appearance.Font = new System.Drawing.Font("黑体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn11.Appearance.Options.UseFont = true;
            this.btn11.AppearancePressed.BackColor = System.Drawing.Color.White;
            this.btn11.AppearancePressed.Options.UseBackColor = true;
            this.btn11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn11.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btn11.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btn11.ImageOptions.SvgImage")));
            this.btn11.Location = new System.Drawing.Point(940, 4);
            this.btn11.Margin = new System.Windows.Forms.Padding(10, 4, 6, 4);
            this.btn11.Name = "btn11";
            this.btn11.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btn11.Size = new System.Drawing.Size(75, 56);
            this.btn11.TabIndex = 11;
            this.btn11.Text = "数据清理";
            this.btn11.Visible = false;
            // 
            // btn12
            // 
            this.btn12.Appearance.Font = new System.Drawing.Font("黑体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn12.Appearance.Options.UseFont = true;
            this.btn12.AppearancePressed.BackColor = System.Drawing.Color.White;
            this.btn12.AppearancePressed.Options.UseBackColor = true;
            this.btn12.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn12.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btn12.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btn12.ImageOptions.SvgImage")));
            this.btn12.Location = new System.Drawing.Point(1031, 4);
            this.btn12.Margin = new System.Windows.Forms.Padding(10, 4, 6, 4);
            this.btn12.Name = "btn12";
            this.btn12.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btn12.Size = new System.Drawing.Size(75, 56);
            this.btn12.TabIndex = 12;
            this.btn12.Text = "查看日志";
            this.btn12.Visible = false;
            // 
            // btn13
            // 
            this.btn13.Appearance.Font = new System.Drawing.Font("黑体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn13.Appearance.Options.UseFont = true;
            this.btn13.AppearancePressed.BackColor = System.Drawing.Color.White;
            this.btn13.AppearancePressed.Options.UseBackColor = true;
            this.btn13.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn13.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btn13.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btn13.ImageOptions.SvgImage")));
            this.btn13.Location = new System.Drawing.Point(1122, 4);
            this.btn13.Margin = new System.Windows.Forms.Padding(10, 4, 6, 4);
            this.btn13.Name = "btn13";
            this.btn13.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btn13.Size = new System.Drawing.Size(75, 56);
            this.btn13.TabIndex = 13;
            this.btn13.Text = "数据结算";
            this.btn13.Visible = false;
            // 
            // btn14
            // 
            this.btn14.Appearance.Font = new System.Drawing.Font("黑体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn14.Appearance.Options.UseFont = true;
            this.btn14.AppearancePressed.BackColor = System.Drawing.Color.White;
            this.btn14.AppearancePressed.Options.UseBackColor = true;
            this.btn14.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn14.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btn14.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btn14.ImageOptions.SvgImage")));
            this.btn14.Location = new System.Drawing.Point(1213, 4);
            this.btn14.Margin = new System.Windows.Forms.Padding(10, 4, 6, 4);
            this.btn14.Name = "btn14";
            this.btn14.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btn14.Size = new System.Drawing.Size(75, 56);
            this.btn14.TabIndex = 15;
            this.btn14.Text = "采购入库";
            this.btn14.Click += new System.EventHandler(this.btn14_Click);
            // 
            // btn15
            // 
            this.btn15.Appearance.Font = new System.Drawing.Font("黑体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn15.Appearance.Options.UseFont = true;
            this.btn15.AppearancePressed.BackColor = System.Drawing.Color.White;
            this.btn15.AppearancePressed.Options.UseBackColor = true;
            this.btn15.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn15.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btn15.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btn15.ImageOptions.SvgImage")));
            this.btn15.Location = new System.Drawing.Point(10, 68);
            this.btn15.Margin = new System.Windows.Forms.Padding(10, 4, 6, 4);
            this.btn15.Name = "btn15";
            this.btn15.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btn15.Size = new System.Drawing.Size(75, 56);
            this.btn15.TabIndex = 16;
            this.btn15.Text = "采购退货";
            this.btn15.Click += new System.EventHandler(this.btn15_Click);
            // 
            // btn16
            // 
            this.btn16.Appearance.Font = new System.Drawing.Font("黑体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn16.Appearance.Options.UseFont = true;
            this.btn16.AppearancePressed.BackColor = System.Drawing.Color.White;
            this.btn16.AppearancePressed.Options.UseBackColor = true;
            this.btn16.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn16.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btn16.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btn16.ImageOptions.SvgImage")));
            this.btn16.Location = new System.Drawing.Point(101, 68);
            this.btn16.Margin = new System.Windows.Forms.Padding(10, 4, 6, 4);
            this.btn16.Name = "btn16";
            this.btn16.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btn16.Size = new System.Drawing.Size(75, 56);
            this.btn16.TabIndex = 17;
            this.btn16.Text = "生产入库";
            this.btn16.Visible = false;
            // 
            // btn17
            // 
            this.btn17.Appearance.Font = new System.Drawing.Font("黑体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn17.Appearance.Options.UseFont = true;
            this.btn17.AppearancePressed.BackColor = System.Drawing.Color.White;
            this.btn17.AppearancePressed.Options.UseBackColor = true;
            this.btn17.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn17.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btn17.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btn17.ImageOptions.SvgImage")));
            this.btn17.Location = new System.Drawing.Point(192, 68);
            this.btn17.Margin = new System.Windows.Forms.Padding(10, 4, 6, 4);
            this.btn17.Name = "btn17";
            this.btn17.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btn17.Size = new System.Drawing.Size(75, 56);
            this.btn17.TabIndex = 18;
            this.btn17.Text = "次品返工";
            this.btn17.Visible = false;
            // 
            // btn18
            // 
            this.btn18.Appearance.Font = new System.Drawing.Font("黑体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn18.Appearance.Options.UseFont = true;
            this.btn18.AppearancePressed.BackColor = System.Drawing.Color.White;
            this.btn18.AppearancePressed.Options.UseBackColor = true;
            this.btn18.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn18.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btn18.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btn18.ImageOptions.SvgImage")));
            this.btn18.Location = new System.Drawing.Point(283, 68);
            this.btn18.Margin = new System.Windows.Forms.Padding(10, 4, 6, 4);
            this.btn18.Name = "btn18";
            this.btn18.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btn18.Size = new System.Drawing.Size(75, 56);
            this.btn18.TabIndex = 19;
            this.btn18.Text = "入库统计";
            this.btn18.Click += new System.EventHandler(this.btn18_Click);
            // 
            // btn19
            // 
            this.btn19.Appearance.Font = new System.Drawing.Font("黑体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn19.Appearance.Options.UseFont = true;
            this.btn19.AppearancePressed.BackColor = System.Drawing.Color.White;
            this.btn19.AppearancePressed.Options.UseBackColor = true;
            this.btn19.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn19.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btn19.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btn19.ImageOptions.SvgImage")));
            this.btn19.Location = new System.Drawing.Point(374, 68);
            this.btn19.Margin = new System.Windows.Forms.Padding(10, 4, 6, 4);
            this.btn19.Name = "btn19";
            this.btn19.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btn19.Size = new System.Drawing.Size(75, 56);
            this.btn19.TabIndex = 20;
            this.btn19.Text = "退货统计";
            this.btn19.Click += new System.EventHandler(this.btn19_Click);
            // 
            // btn20
            // 
            this.btn20.Appearance.Font = new System.Drawing.Font("黑体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn20.Appearance.Options.UseFont = true;
            this.btn20.AppearancePressed.BackColor = System.Drawing.Color.White;
            this.btn20.AppearancePressed.Options.UseBackColor = true;
            this.btn20.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn20.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btn20.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btn20.ImageOptions.SvgImage")));
            this.btn20.Location = new System.Drawing.Point(465, 68);
            this.btn20.Margin = new System.Windows.Forms.Padding(10, 4, 6, 4);
            this.btn20.Name = "btn20";
            this.btn20.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btn20.Size = new System.Drawing.Size(75, 56);
            this.btn20.TabIndex = 14;
            this.btn20.Text = "商品销售";
            this.btn20.Click += new System.EventHandler(this.btn20_Click);
            // 
            // btn21
            // 
            this.btn21.Appearance.Font = new System.Drawing.Font("黑体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn21.Appearance.Options.UseFont = true;
            this.btn21.AppearancePressed.BackColor = System.Drawing.Color.White;
            this.btn21.AppearancePressed.Options.UseBackColor = true;
            this.btn21.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn21.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btn21.ImageOptions.Image")));
            this.btn21.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btn21.Location = new System.Drawing.Point(556, 68);
            this.btn21.Margin = new System.Windows.Forms.Padding(10, 4, 6, 4);
            this.btn21.Name = "btn21";
            this.btn21.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btn21.Size = new System.Drawing.Size(75, 56);
            this.btn21.TabIndex = 21;
            this.btn21.Text = "顾客退货";
            this.btn21.Click += new System.EventHandler(this.btn21_Click);
            // 
            // btn22
            // 
            this.btn22.Appearance.Font = new System.Drawing.Font("黑体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn22.Appearance.Options.UseFont = true;
            this.btn22.AppearancePressed.BackColor = System.Drawing.Color.White;
            this.btn22.AppearancePressed.Options.UseBackColor = true;
            this.btn22.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn22.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btn22.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btn22.ImageOptions.SvgImage")));
            this.btn22.Location = new System.Drawing.Point(647, 68);
            this.btn22.Margin = new System.Windows.Forms.Padding(10, 4, 6, 4);
            this.btn22.Name = "btn22";
            this.btn22.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btn22.Size = new System.Drawing.Size(75, 56);
            this.btn22.TabIndex = 22;
            this.btn22.Text = "部门领用";
            this.btn22.Visible = false;
            // 
            // btn23
            // 
            this.btn23.Appearance.Font = new System.Drawing.Font("黑体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn23.Appearance.Options.UseFont = true;
            this.btn23.AppearancePressed.BackColor = System.Drawing.Color.White;
            this.btn23.AppearancePressed.Options.UseBackColor = true;
            this.btn23.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn23.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btn23.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btn23.ImageOptions.SvgImage")));
            this.btn23.Location = new System.Drawing.Point(738, 68);
            this.btn23.Margin = new System.Windows.Forms.Padding(10, 4, 6, 4);
            this.btn23.Name = "btn23";
            this.btn23.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btn23.Size = new System.Drawing.Size(75, 56);
            this.btn23.TabIndex = 23;
            this.btn23.Text = "部门退回";
            this.btn23.Visible = false;
            // 
            // btn24
            // 
            this.btn24.Appearance.Font = new System.Drawing.Font("黑体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn24.Appearance.Options.UseFont = true;
            this.btn24.AppearancePressed.BackColor = System.Drawing.Color.White;
            this.btn24.AppearancePressed.Options.UseBackColor = true;
            this.btn24.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn24.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btn24.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btn24.ImageOptions.SvgImage")));
            this.btn24.Location = new System.Drawing.Point(829, 68);
            this.btn24.Margin = new System.Windows.Forms.Padding(10, 4, 6, 4);
            this.btn24.Name = "btn24";
            this.btn24.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btn24.Size = new System.Drawing.Size(75, 56);
            this.btn24.TabIndex = 24;
            this.btn24.Text = "出库统计";
            this.btn24.Click += new System.EventHandler(this.btn24_Click);
            // 
            // btn25
            // 
            this.btn25.Appearance.Font = new System.Drawing.Font("黑体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn25.Appearance.Options.UseFont = true;
            this.btn25.AppearancePressed.BackColor = System.Drawing.Color.White;
            this.btn25.AppearancePressed.Options.UseBackColor = true;
            this.btn25.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn25.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btn25.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btn25.ImageOptions.SvgImage")));
            this.btn25.Location = new System.Drawing.Point(920, 68);
            this.btn25.Margin = new System.Windows.Forms.Padding(10, 4, 6, 4);
            this.btn25.Name = "btn25";
            this.btn25.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btn25.Size = new System.Drawing.Size(75, 56);
            this.btn25.TabIndex = 25;
            this.btn25.Text = "退库统计";
            this.btn25.Click += new System.EventHandler(this.btn25_Click);
            // 
            // btn26
            // 
            this.btn26.Appearance.Font = new System.Drawing.Font("黑体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn26.Appearance.Options.UseFont = true;
            this.btn26.AppearancePressed.BackColor = System.Drawing.Color.White;
            this.btn26.AppearancePressed.Options.UseBackColor = true;
            this.btn26.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn26.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btn26.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btn26.ImageOptions.SvgImage")));
            this.btn26.Location = new System.Drawing.Point(1011, 68);
            this.btn26.Margin = new System.Windows.Forms.Padding(10, 4, 6, 4);
            this.btn26.Name = "btn26";
            this.btn26.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btn26.Size = new System.Drawing.Size(85, 56);
            this.btn26.TabIndex = 26;
            this.btn26.Text = "进销存明细";
            this.btn26.Click += new System.EventHandler(this.btn26_Click);
            // 
            // btn27
            // 
            this.btn27.Appearance.Font = new System.Drawing.Font("黑体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn27.Appearance.Options.UseFont = true;
            this.btn27.AppearancePressed.BackColor = System.Drawing.Color.White;
            this.btn27.AppearancePressed.Options.UseBackColor = true;
            this.btn27.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn27.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btn27.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btn27.ImageOptions.SvgImage")));
            this.btn27.Location = new System.Drawing.Point(1112, 68);
            this.btn27.Margin = new System.Windows.Forms.Padding(10, 4, 6, 4);
            this.btn27.Name = "btn27";
            this.btn27.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btn27.Size = new System.Drawing.Size(85, 56);
            this.btn27.TabIndex = 27;
            this.btn27.Text = "进销存汇总";
            this.btn27.Click += new System.EventHandler(this.btn27_Click);
            // 
            // btn28
            // 
            this.btn28.Appearance.Font = new System.Drawing.Font("黑体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn28.Appearance.Options.UseFont = true;
            this.btn28.AppearancePressed.BackColor = System.Drawing.Color.White;
            this.btn28.AppearancePressed.Options.UseBackColor = true;
            this.btn28.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn28.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btn28.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btn28.ImageOptions.SvgImage")));
            this.btn28.Location = new System.Drawing.Point(1213, 68);
            this.btn28.Margin = new System.Windows.Forms.Padding(10, 4, 6, 4);
            this.btn28.Name = "btn28";
            this.btn28.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btn28.Size = new System.Drawing.Size(85, 56);
            this.btn28.TabIndex = 28;
            this.btn28.Text = "收货对账单";
            this.btn28.Click += new System.EventHandler(this.btn28_Click);
            // 
            // btn29
            // 
            this.btn29.Appearance.Font = new System.Drawing.Font("黑体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn29.Appearance.Options.UseFont = true;
            this.btn29.AppearancePressed.BackColor = System.Drawing.Color.White;
            this.btn29.AppearancePressed.Options.UseBackColor = true;
            this.btn29.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn29.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btn29.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btn29.ImageOptions.SvgImage")));
            this.btn29.Location = new System.Drawing.Point(10, 132);
            this.btn29.Margin = new System.Windows.Forms.Padding(10, 4, 6, 4);
            this.btn29.Name = "btn29";
            this.btn29.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btn29.Size = new System.Drawing.Size(85, 56);
            this.btn29.TabIndex = 29;
            this.btn29.Text = "发货对账单";
            this.btn29.Click += new System.EventHandler(this.btn29_Click);
            // 
            // btn30
            // 
            this.btn30.Appearance.Font = new System.Drawing.Font("黑体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn30.Appearance.Options.UseFont = true;
            this.btn30.AppearancePressed.BackColor = System.Drawing.Color.White;
            this.btn30.AppearancePressed.Options.UseBackColor = true;
            this.btn30.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn30.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btn30.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btn30.ImageOptions.SvgImage")));
            this.btn30.Location = new System.Drawing.Point(111, 132);
            this.btn30.Margin = new System.Windows.Forms.Padding(10, 4, 6, 4);
            this.btn30.Name = "btn30";
            this.btn30.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btn30.Size = new System.Drawing.Size(75, 56);
            this.btn30.TabIndex = 30;
            this.btn30.Text = "发货成本";
            this.btn30.Click += new System.EventHandler(this.simpleButton42_Click);
            // 
            // btn31
            // 
            this.btn31.Appearance.Font = new System.Drawing.Font("黑体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn31.Appearance.Options.UseFont = true;
            this.btn31.AppearancePressed.BackColor = System.Drawing.Color.White;
            this.btn31.AppearancePressed.Options.UseBackColor = true;
            this.btn31.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn31.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btn31.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btn31.ImageOptions.SvgImage")));
            this.btn31.Location = new System.Drawing.Point(202, 132);
            this.btn31.Margin = new System.Windows.Forms.Padding(10, 4, 6, 4);
            this.btn31.Name = "btn31";
            this.btn31.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btn31.Size = new System.Drawing.Size(75, 56);
            this.btn31.TabIndex = 31;
            this.btn31.Text = "销售毛利";
            this.btn31.Click += new System.EventHandler(this.btn31_Click);
            // 
            // btn32
            // 
            this.btn32.Appearance.Font = new System.Drawing.Font("黑体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn32.Appearance.Options.UseFont = true;
            this.btn32.AppearancePressed.BackColor = System.Drawing.Color.White;
            this.btn32.AppearancePressed.Options.UseBackColor = true;
            this.btn32.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn32.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btn32.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btn32.ImageOptions.SvgImage")));
            this.btn32.Location = new System.Drawing.Point(293, 132);
            this.btn32.Margin = new System.Windows.Forms.Padding(10, 4, 6, 4);
            this.btn32.Name = "btn32";
            this.btn32.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btn32.Size = new System.Drawing.Size(75, 56);
            this.btn32.TabIndex = 32;
            this.btn32.Text = "应收登记";
            this.btn32.Click += new System.EventHandler(this.btn32_Click);
            // 
            // btn33
            // 
            this.btn33.Appearance.Font = new System.Drawing.Font("黑体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn33.Appearance.Options.UseFont = true;
            this.btn33.AppearancePressed.BackColor = System.Drawing.Color.White;
            this.btn33.AppearancePressed.Options.UseBackColor = true;
            this.btn33.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn33.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btn33.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btn33.ImageOptions.SvgImage")));
            this.btn33.Location = new System.Drawing.Point(384, 132);
            this.btn33.Margin = new System.Windows.Forms.Padding(10, 4, 6, 4);
            this.btn33.Name = "btn33";
            this.btn33.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btn33.Size = new System.Drawing.Size(75, 56);
            this.btn33.TabIndex = 33;
            this.btn33.Text = "应付登记";
            this.btn33.Click += new System.EventHandler(this.btn33_Click);
            // 
            // btn34
            // 
            this.btn34.Appearance.Font = new System.Drawing.Font("黑体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn34.Appearance.Options.UseFont = true;
            this.btn34.AppearancePressed.BackColor = System.Drawing.Color.White;
            this.btn34.AppearancePressed.Options.UseBackColor = true;
            this.btn34.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn34.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btn34.ImageOptions.Image")));
            this.btn34.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btn34.Location = new System.Drawing.Point(475, 132);
            this.btn34.Margin = new System.Windows.Forms.Padding(10, 4, 6, 4);
            this.btn34.Name = "btn34";
            this.btn34.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btn34.Size = new System.Drawing.Size(75, 56);
            this.btn34.TabIndex = 34;
            this.btn34.Text = "收款登记";
            this.btn34.Click += new System.EventHandler(this.btn34_Click);
            // 
            // btn35
            // 
            this.btn35.Appearance.Font = new System.Drawing.Font("黑体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn35.Appearance.Options.UseFont = true;
            this.btn35.AppearancePressed.BackColor = System.Drawing.Color.White;
            this.btn35.AppearancePressed.Options.UseBackColor = true;
            this.btn35.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn35.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btn35.ImageOptions.Image")));
            this.btn35.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btn35.Location = new System.Drawing.Point(566, 132);
            this.btn35.Margin = new System.Windows.Forms.Padding(10, 4, 6, 4);
            this.btn35.Name = "btn35";
            this.btn35.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btn35.Size = new System.Drawing.Size(75, 56);
            this.btn35.TabIndex = 35;
            this.btn35.Text = "付款登记";
            this.btn35.Click += new System.EventHandler(this.btn35_Click);
            // 
            // btn36
            // 
            this.btn36.Appearance.Font = new System.Drawing.Font("黑体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn36.Appearance.Options.UseFont = true;
            this.btn36.AppearancePressed.BackColor = System.Drawing.Color.White;
            this.btn36.AppearancePressed.Options.UseBackColor = true;
            this.btn36.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn36.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btn36.ImageOptions.Image")));
            this.btn36.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btn36.Location = new System.Drawing.Point(657, 132);
            this.btn36.Margin = new System.Windows.Forms.Padding(10, 4, 6, 4);
            this.btn36.Name = "btn36";
            this.btn36.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btn36.Size = new System.Drawing.Size(75, 56);
            this.btn36.TabIndex = 36;
            this.btn36.Text = "应收帐表";
            this.btn36.Visible = false;
            this.btn36.Click += new System.EventHandler(this.btn36_Click);
            // 
            // btn37
            // 
            this.btn37.Appearance.Font = new System.Drawing.Font("黑体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn37.Appearance.Options.UseFont = true;
            this.btn37.AppearancePressed.BackColor = System.Drawing.Color.White;
            this.btn37.AppearancePressed.Options.UseBackColor = true;
            this.btn37.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn37.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btn37.ImageOptions.Image")));
            this.btn37.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btn37.Location = new System.Drawing.Point(748, 132);
            this.btn37.Margin = new System.Windows.Forms.Padding(10, 4, 6, 4);
            this.btn37.Name = "btn37";
            this.btn37.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btn37.Size = new System.Drawing.Size(75, 56);
            this.btn37.TabIndex = 37;
            this.btn37.Text = "应付帐表";
            this.btn37.Visible = false;
            this.btn37.Click += new System.EventHandler(this.btn37_Click);
            // 
            // btn38
            // 
            this.btn38.Appearance.Font = new System.Drawing.Font("黑体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn38.Appearance.Options.UseFont = true;
            this.btn38.AppearancePressed.BackColor = System.Drawing.Color.White;
            this.btn38.AppearancePressed.Options.UseBackColor = true;
            this.btn38.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn38.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btn38.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btn38.ImageOptions.SvgImage")));
            this.btn38.Location = new System.Drawing.Point(839, 132);
            this.btn38.Margin = new System.Windows.Forms.Padding(10, 4, 6, 4);
            this.btn38.Name = "btn38";
            this.btn38.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btn38.Size = new System.Drawing.Size(75, 56);
            this.btn38.TabIndex = 38;
            this.btn38.Text = "库存盘点";
            this.btn38.Click += new System.EventHandler(this.btn38_Click);
            // 
            // btn39
            // 
            this.btn39.Appearance.Font = new System.Drawing.Font("黑体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn39.Appearance.Options.UseFont = true;
            this.btn39.AppearancePressed.BackColor = System.Drawing.Color.White;
            this.btn39.AppearancePressed.Options.UseBackColor = true;
            this.btn39.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn39.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btn39.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btn39.ImageOptions.SvgImage")));
            this.btn39.Location = new System.Drawing.Point(930, 132);
            this.btn39.Margin = new System.Windows.Forms.Padding(10, 4, 6, 4);
            this.btn39.Name = "btn39";
            this.btn39.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btn39.Size = new System.Drawing.Size(75, 56);
            this.btn39.TabIndex = 39;
            this.btn39.Text = "商品库存";
            this.btn39.Click += new System.EventHandler(this.btn39_Click);
            // 
            // btn40
            // 
            this.btn40.Appearance.Font = new System.Drawing.Font("黑体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn40.Appearance.Options.UseFont = true;
            this.btn40.AppearancePressed.BackColor = System.Drawing.Color.White;
            this.btn40.AppearancePressed.Options.UseBackColor = true;
            this.btn40.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn40.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btn40.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btn40.ImageOptions.SvgImage")));
            this.btn40.Location = new System.Drawing.Point(1021, 132);
            this.btn40.Margin = new System.Windows.Forms.Padding(10, 4, 6, 4);
            this.btn40.Name = "btn40";
            this.btn40.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btn40.Size = new System.Drawing.Size(75, 56);
            this.btn40.TabIndex = 40;
            this.btn40.Text = "库存报警";
            this.btn40.Click += new System.EventHandler(this.btn40_Click);
            // 
            // btn41
            // 
            this.btn41.Appearance.Font = new System.Drawing.Font("黑体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn41.Appearance.Options.UseFont = true;
            this.btn41.AppearancePressed.BackColor = System.Drawing.Color.White;
            this.btn41.AppearancePressed.Options.UseBackColor = true;
            this.btn41.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn41.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btn41.ImageOptions.Image")));
            this.btn41.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btn41.Location = new System.Drawing.Point(1112, 132);
            this.btn41.Margin = new System.Windows.Forms.Padding(10, 4, 6, 4);
            this.btn41.Name = "btn41";
            this.btn41.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btn41.Size = new System.Drawing.Size(75, 56);
            this.btn41.TabIndex = 41;
            this.btn41.Text = "字典类型";
            this.btn41.Click += new System.EventHandler(this.simpleButton15_Click);
            // 
            // btn42
            // 
            this.btn42.Appearance.Font = new System.Drawing.Font("黑体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn42.Appearance.Options.UseFont = true;
            this.btn42.AppearancePressed.BackColor = System.Drawing.Color.White;
            this.btn42.AppearancePressed.Options.UseBackColor = true;
            this.btn42.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn42.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btn42.ImageOptions.Image")));
            this.btn42.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btn42.Location = new System.Drawing.Point(1203, 132);
            this.btn42.Margin = new System.Windows.Forms.Padding(10, 4, 6, 4);
            this.btn42.Name = "btn42";
            this.btn42.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btn42.Size = new System.Drawing.Size(75, 56);
            this.btn42.TabIndex = 42;
            this.btn42.Text = "字典数据";
            this.btn42.Click += new System.EventHandler(this.simpleButton16_Click);
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.groupControl1);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl3.Location = new System.Drawing.Point(2, 2);
            this.panelControl3.Margin = new System.Windows.Forms.Padding(4);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(163, 672);
            this.panelControl3.TabIndex = 0;
            // 
            // groupControl1
            // 
            this.groupControl1.AutoSize = true;
            this.groupControl1.Controls.Add(this.flowLayoutPanel2);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(2, 2);
            this.groupControl1.Margin = new System.Windows.Forms.Padding(4);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(159, 668);
            this.groupControl1.TabIndex = 0;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.simpleButton12);
            this.flowLayoutPanel2.Controls.Add(this.simpleButton10);
            this.flowLayoutPanel2.Controls.Add(this.simpleButton5);
            this.flowLayoutPanel2.Controls.Add(this.simpleButton6);
            this.flowLayoutPanel2.Controls.Add(this.simpleButton7);
            this.flowLayoutPanel2.Controls.Add(this.simpleButton8);
            this.flowLayoutPanel2.Controls.Add(this.simpleButton9);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(2, 23);
            this.flowLayoutPanel2.Margin = new System.Windows.Forms.Padding(4);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(155, 643);
            this.flowLayoutPanel2.TabIndex = 0;
            // 
            // simpleButton12
            // 
            this.simpleButton12.Appearance.Font = new System.Drawing.Font("黑体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.simpleButton12.Appearance.Options.UseFont = true;
            this.simpleButton12.Cursor = System.Windows.Forms.Cursors.Hand;
            this.simpleButton12.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.simpleButton12.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("simpleButton12.ImageOptions.SvgImage")));
            this.simpleButton12.Location = new System.Drawing.Point(0, 5);
            this.simpleButton12.Margin = new System.Windows.Forms.Padding(0, 5, 5, 10);
            this.simpleButton12.Name = "simpleButton12";
            this.simpleButton12.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.simpleButton12.Size = new System.Drawing.Size(153, 35);
            this.simpleButton12.TabIndex = 12;
            this.simpleButton12.Text = "基础数据";
            this.simpleButton12.Click += new System.EventHandler(this.simpleButton12_Click);
            // 
            // simpleButton10
            // 
            this.simpleButton10.Appearance.Font = new System.Drawing.Font("黑体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.simpleButton10.Appearance.Options.UseFont = true;
            this.simpleButton10.Cursor = System.Windows.Forms.Cursors.Hand;
            this.simpleButton10.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton10.ImageOptions.Image")));
            this.simpleButton10.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.simpleButton10.Location = new System.Drawing.Point(0, 55);
            this.simpleButton10.Margin = new System.Windows.Forms.Padding(0, 5, 5, 10);
            this.simpleButton10.Name = "simpleButton10";
            this.simpleButton10.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.simpleButton10.Size = new System.Drawing.Size(153, 35);
            this.simpleButton10.TabIndex = 6;
            this.simpleButton10.Text = "入库管理";
            this.simpleButton10.Click += new System.EventHandler(this.simpleButton10_Click);
            // 
            // simpleButton5
            // 
            this.simpleButton5.Appearance.Font = new System.Drawing.Font("黑体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.simpleButton5.Appearance.Options.UseFont = true;
            this.simpleButton5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.simpleButton5.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton5.ImageOptions.Image")));
            this.simpleButton5.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.simpleButton5.Location = new System.Drawing.Point(0, 105);
            this.simpleButton5.Margin = new System.Windows.Forms.Padding(0, 5, 5, 10);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.simpleButton5.Size = new System.Drawing.Size(153, 35);
            this.simpleButton5.TabIndex = 7;
            this.simpleButton5.Text = "出库管理";
            this.simpleButton5.Click += new System.EventHandler(this.simpleButton5_Click);
            // 
            // simpleButton6
            // 
            this.simpleButton6.Appearance.Font = new System.Drawing.Font("黑体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.simpleButton6.Appearance.Options.UseFont = true;
            this.simpleButton6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.simpleButton6.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton6.ImageOptions.Image")));
            this.simpleButton6.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.simpleButton6.Location = new System.Drawing.Point(0, 155);
            this.simpleButton6.Margin = new System.Windows.Forms.Padding(0, 5, 5, 10);
            this.simpleButton6.Name = "simpleButton6";
            this.simpleButton6.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.simpleButton6.Size = new System.Drawing.Size(153, 35);
            this.simpleButton6.TabIndex = 8;
            this.simpleButton6.Text = "库存管理";
            this.simpleButton6.Click += new System.EventHandler(this.simpleButton6_Click);
            // 
            // simpleButton7
            // 
            this.simpleButton7.Appearance.Font = new System.Drawing.Font("黑体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.simpleButton7.Appearance.Options.UseFont = true;
            this.simpleButton7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.simpleButton7.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.simpleButton7.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("simpleButton7.ImageOptions.SvgImage")));
            this.simpleButton7.Location = new System.Drawing.Point(0, 205);
            this.simpleButton7.Margin = new System.Windows.Forms.Padding(0, 5, 5, 10);
            this.simpleButton7.Name = "simpleButton7";
            this.simpleButton7.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.simpleButton7.Size = new System.Drawing.Size(153, 35);
            this.simpleButton7.TabIndex = 9;
            this.simpleButton7.Text = "统计报表";
            this.simpleButton7.Click += new System.EventHandler(this.simpleButton7_Click);
            // 
            // simpleButton8
            // 
            this.simpleButton8.Appearance.Font = new System.Drawing.Font("黑体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.simpleButton8.Appearance.Options.UseFont = true;
            this.simpleButton8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.simpleButton8.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.simpleButton8.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("simpleButton8.ImageOptions.SvgImage")));
            this.simpleButton8.Location = new System.Drawing.Point(0, 255);
            this.simpleButton8.Margin = new System.Windows.Forms.Padding(0, 5, 5, 10);
            this.simpleButton8.Name = "simpleButton8";
            this.simpleButton8.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.simpleButton8.Size = new System.Drawing.Size(153, 35);
            this.simpleButton8.TabIndex = 10;
            this.simpleButton8.Text = "往来帐款";
            this.simpleButton8.Click += new System.EventHandler(this.simpleButton8_Click);
            // 
            // simpleButton9
            // 
            this.simpleButton9.Appearance.Font = new System.Drawing.Font("黑体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.simpleButton9.Appearance.Options.UseFont = true;
            this.simpleButton9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.simpleButton9.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.simpleButton9.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("simpleButton9.ImageOptions.SvgImage")));
            this.simpleButton9.Location = new System.Drawing.Point(0, 305);
            this.simpleButton9.Margin = new System.Windows.Forms.Padding(0, 5, 5, 10);
            this.simpleButton9.Name = "simpleButton9";
            this.simpleButton9.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.simpleButton9.Size = new System.Drawing.Size(153, 35);
            this.simpleButton9.TabIndex = 11;
            this.simpleButton9.Text = "系统设置";
            this.simpleButton9.Click += new System.EventHandler(this.simpleButton9_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // MainForm
            // 
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1498, 768);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panelControl1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(1300, 800);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "通用进销存管理系统";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage2.ResumeLayout(false);
            this.flowLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.flowLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton simpleButton10;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private DevExpress.XtraEditors.SimpleButton simpleButton6;
        private DevExpress.XtraEditors.SimpleButton simpleButton7;
        private DevExpress.XtraEditors.SimpleButton simpleButton8;
        private DevExpress.XtraEditors.SimpleButton simpleButton9;
        private DevExpress.XtraEditors.SimpleButton simpleButton11;
        private DevExpress.XtraEditors.SimpleButton simpleButton12;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private DevExpress.XtraEditors.SimpleButton btn2;
        private DevExpress.XtraEditors.SimpleButton btn3;
        private DevExpress.XtraEditors.SimpleButton btn4;
        private DevExpress.XtraEditors.SimpleButton btn5;
        private DevExpress.XtraEditors.SimpleButton btn6;
        private DevExpress.XtraEditors.SimpleButton btn7;
        private DevExpress.XtraEditors.SimpleButton btn8;
        private DevExpress.XtraEditors.SimpleButton btn9;
        private DevExpress.XtraEditors.SimpleButton btn10;
        private DevExpress.XtraEditors.SimpleButton btn11;
        private DevExpress.XtraEditors.SimpleButton btn12;
        private DevExpress.XtraEditors.SimpleButton btn13;
        private DevExpress.XtraEditors.SimpleButton btn20;
        private DevExpress.XtraEditors.SimpleButton btn14;
        private DevExpress.XtraEditors.SimpleButton btn15;
        private DevExpress.XtraEditors.SimpleButton btn16;
        private DevExpress.XtraEditors.SimpleButton btn17;
        private DevExpress.XtraEditors.SimpleButton btn18;
        private DevExpress.XtraEditors.SimpleButton btn19;
        private DevExpress.XtraEditors.SimpleButton btn21;
        private DevExpress.XtraEditors.SimpleButton btn22;
        private DevExpress.XtraEditors.SimpleButton btn23;
        private DevExpress.XtraEditors.SimpleButton btn24;
        private DevExpress.XtraEditors.SimpleButton btn25;
        private DevExpress.XtraEditors.SimpleButton btn26;
        private DevExpress.XtraEditors.SimpleButton btn27;
        private DevExpress.XtraEditors.SimpleButton btn28;
        private DevExpress.XtraEditors.SimpleButton btn29;
        private DevExpress.XtraEditors.SimpleButton btn30;
        private DevExpress.XtraEditors.SimpleButton btn31;
        private DevExpress.XtraEditors.SimpleButton btn32;
        private DevExpress.XtraEditors.SimpleButton btn33;
        private DevExpress.XtraEditors.SimpleButton btn34;
        private DevExpress.XtraEditors.SimpleButton btn35;
        private DevExpress.XtraEditors.SimpleButton btn36;
        private DevExpress.XtraEditors.SimpleButton btn37;
        private DevExpress.XtraEditors.SimpleButton btn1;
        private DevExpress.XtraEditors.SimpleButton btn38;
        private DevExpress.XtraEditors.SimpleButton btn39;
        private DevExpress.XtraEditors.SimpleButton btn40;
        private DevExpress.XtraEditors.SimpleButton simpleButton13;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton14;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private System.Windows.Forms.Timer timer1;
        private DevExpress.XtraEditors.SimpleButton btn41;
        private DevExpress.XtraEditors.SimpleButton btn42;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
    }
}