﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraVerticalGrid;
using JXC.Core;
using JXC.Models;
using SqlSugar.Extensions;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.TextBox;

namespace JXC.UI.BaseData
{
    public partial class GYSEditForm : XtraForm
    {
        private APIClient client = new APIClient();
        public ghsxx data;
        public bool IsEdit = false;
        public GridControl mg;

        public GYSEditForm()
        {
            InitializeComponent();
        }

        private void GYSEditForm_Load(object sender, EventArgs e)
        {
            comboBoxEdit1.Properties.Items.AddRange(client.GetDicDataList().Where(p => p.DicTypeName.Equals("供货商类型")).Select(p => p.DicData).ToList());
            comboBoxEdit2.Properties.Items.AddRange(client.GetDicDataList().Where(p => p.DicTypeName.Equals("所属地区")).Select(p => p.DicData).ToList());
            if (IsEdit)
            {
                textEdit1.ReadOnly = true;
                textEdit4.ReadOnly = true;
                textEdit1.Text = data.GHSName;
                comboBoxEdit1.Text = data.GHSLXName;
                comboBoxEdit2.Text = data.SSDQ;
                textEdit2.Text = data.LXR;
                textEdit3.Text = data.LXDH;
                textEdit4.Text = data.WFYFJE.ToString();
                textEdit5.Text = data.LXDZ;
                textEdit6.Text = data.BZ;
                checkEdit1.Checked = data.IsMR.Value;
                checkEdit2.Checked = data.IsJY.Value;
            }
            else
            {
                comboBoxEdit1.SelectedIndex = 0;
                comboBoxEdit2.SelectedIndex = 0;
            }
           
        }
        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (IsEdit)
            {
                data.GHSName = textEdit1.Text;
                data.GHSLXName = comboBoxEdit1.Text;
                data.SSDQ = comboBoxEdit2.Text;
                data.LXR = textEdit2.Text;
                data.LXDH = textEdit3.Text;
                data.WFYFJE = textEdit4.Text.ObjToDecimal();
                data.LXDZ = textEdit5.Text;
                data.BZ = textEdit6.Text;
                data.IsMR = checkEdit1.Checked;
                data.IsJY = checkEdit2.Checked;
                if (client.UpdateGHS(new List<ghsxx>() { data }))
                {
                    this.Close();
                }
            }
            else
            {
                if (string.IsNullOrEmpty(textEdit1.Text))
                {
                    XtraMessageBox.Show("供货商名称不能为空");
                    return;
                }
                if (client.CheckGHSName(textEdit1.Text))
                {
                    "供货商名称已存在".ShowTips();
                    return;
                }
                ghsxx m = new ghsxx();
                m.GHSName = textEdit1.Text;
                m.GHSLXName = comboBoxEdit1.Text;
                m.SSDQ = comboBoxEdit2.Text;
                m.LXR = textEdit2.Text;
                m.LXDH = textEdit3.Text;
                m.WFYFJE = textEdit4.Text.ObjToDecimal();
                m.LXDZ = textEdit5.Text;
                m.BZ = textEdit6.Text;
                m.IsMR = checkEdit1.Checked;
                m.IsJY = checkEdit2.Checked;
                if (client.AddGHS(m))
                {
                    textEdit1.Text = "";
                    comboBoxEdit1.SelectedIndex = 0;
                    comboBoxEdit2.SelectedIndex = 0;
                    textEdit2.Text = "";
                    textEdit3.Text = "";
                    textEdit4.Text = "";
                    textEdit5.Text = "";
                    textEdit6.Text = "";
                    checkEdit1.Checked = false;
                    checkEdit2.Checked = false;
                }

            }
            mg.DataSource = client.GetGHSList();
        }
        /// <summary>
        /// 取消
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
