﻿using DevExpress.XtraEditors;
using JXC.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JXC.UI.BaseData
{
    public partial class KHForm : DevExpress.XtraEditors.XtraForm
    {
        private APIClient client = new APIClient();

        public KHForm()
        {
            InitializeComponent();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            var frm=new KHEditForm();
            frm.mg = gridControl1;
            frm.ShowDialog();
        }

        private void KHForm_Load(object sender, EventArgs e)
        {
            gridControl1.DataSource = client.GetKHList();
            gridView1.OptionsBehavior.Editable = false;
            gridView1.BestFitColumns();
        }
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton3_Click(object sender, EventArgs e)
        {
            if (gridView1.FocusedRowHandle < 0) return;
            var frm = new KHEditForm();
            frm.IsEdit = true;
            frm.data = (khxx)gridView1.GetRow(gridView1.FocusedRowHandle);
            frm.mg= gridControl1;
            frm.ShowDialog();
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton4_Click(object sender, EventArgs e)
        {
            if (gridView1.FocusedRowHandle < 0) return;
            client.DeleteKH(new List<khxx>() { (khxx)gridView1.GetRow(gridView1.FocusedRowHandle) });
            var data = client.GetKHList();
            gridControl1.DataSource = data;
        }

        private void simpleButton10_Click(object sender, EventArgs e)
        {
            var data = client.GetKHList();
            if (!string.IsNullOrEmpty(textEdit2.Text))
            {
                data = data.Where(p => p.KHName.Equals(textEdit2.Text)).ToList();
            }

            gridControl1.DataSource = data;
        }
    }
}