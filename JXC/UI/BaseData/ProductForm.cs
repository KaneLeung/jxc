﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JXC.Models;

namespace JXC.UI.BaseData
{
    public partial class ProductForm : XtraForm
    {
        private APIClient _client=new APIClient();
        public ProductForm()
        {
            InitializeComponent();
        }

        private void ProductForm_Load(object sender, EventArgs e)
        {
            gridView1.OptionsBehavior.Editable = false;
            for (int i = 0; i < this.gridView1.Columns.Count; i++)
            {
                this.gridView1.Columns[i].AppearanceHeader.ForeColor = Color.Black;
            }
            var data=_client.GetSPList();
            gridControl1.DataSource = data;
            gridView1.BestFitColumns(true);
        }
        /// <summary>
        /// 增加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton2_Click(object sender, EventArgs e)
        {
            new ProductEditForm().ShowDialog();
        }

        /// <summary>
        /// 搜索
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton10_Click(object sender, EventArgs e)
        {
            var list = _client.GetSPList();
            if (!string.IsNullOrEmpty(textEdit1.Text))
            {
                list = list.Where(p => p.SPCode.Equals(textEdit1.Text)).ToList();
            }
            if (!string.IsNullOrEmpty(textEdit2.Text))
            {
                list = list.Where(p => p.SPName.Equals(textEdit2.Text)).ToList();
            }

            gridControl1.DataSource = list;
        }

        private void simpleButton2_Click_1(object sender, EventArgs e)
        {
            var frm=new ProductEditForm();
            frm.mg = gridControl1;
            frm.ShowDialog();
        }
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton3_Click(object sender, EventArgs e)
        {
            if (gridView1.FocusedRowHandle < 0) return;
            var frm=new ProductEditForm();
            frm.IsEdit=true;
            frm.data = (spxx)gridView1.GetRow(gridView1.FocusedRowHandle);
            frm.mg = gridControl1;
            frm.ShowDialog();
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton4_Click(object sender, EventArgs e)
        {
            if (gridView1.FocusedRowHandle < 0) return;
            _client.DeleteSP(new List<spxx>() { (spxx)gridView1.GetRow(gridView1.FocusedRowHandle) });
            var data = _client.GetSPList();
            gridControl1.DataSource = data;
        }

        private void groupControl2_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
