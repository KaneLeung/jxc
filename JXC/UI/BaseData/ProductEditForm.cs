﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraReports.UI;
using JXC.Models;
using SqlSugar.Extensions;

namespace JXC.UI.BaseData
{
    public partial class ProductEditForm : XtraForm
    {
        private APIClient _client=new APIClient();
        public spxx data;
        public bool IsEdit = false;
        public GridControl mg; 
        public ProductEditForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton2_Click(object sender, EventArgs e)
        {
            if (IsEdit)
            {
                data.SPLBName = comboBoxEdit2.Text;//所属类别
                data.GGXH = textEdit3.Text;//规格型号
                data.YSSJ = textEdit4.Text.ObjToDecimal();//预设售价
                data.SPName = textEdit5.Text;//商品名称
                data.SPCode = textEdit2.Text;//商品编码
                data.IsZP = checkEdit2.Checked;//该商品是否可作为赠品
                data.DW = comboBoxEdit1.Text;//单位
                data.PFJ = textEdit7.Text.ObjToDecimal();//批发价
                data.YSJJ = textEdit6.Text.ObjToDecimal();//预设进价
                //data.DQKC = textEdit11.Text.ObjToDecimal();//当前库存
                data.IsYSXS = checkEdit3.Checked;//预设进价作为销售成本价
                data.SPTM = textEdit9.Text;//商品条码
                data.KCXX = textEdit10.Text.ObjToDecimal();//库存下线
                data.QCKC = textEdit11.Text.ObjToDecimal();//期初库存
                data.SPJJ = textEdit12.Text.ObjToDecimal();//商品均价
                data.CBZE = textEdit13.Text.ObjToDecimal();//成本总额
                data.SPTP = "";//商品图片
                data.BZ = textEdit14.Text;
                if (_client.UpdateSP(new List<spxx>() { data }))
                {
                    mg.DataSource = _client.GetSPList();
                    this.Close();
                }
            }
            else
            {
                //新增
                spxx m = new spxx();
                if (string.IsNullOrEmpty(textEdit5.Text))
                {
                    XtraMessageBox.Show("商品名称不能为空");
                    return;
                }
                if (string.IsNullOrEmpty(textEdit2.Text))
                {
                    XtraMessageBox.Show("商品编码不能为空");
                    return;
                }
                if (!checkEdit3.Checked&&string.IsNullOrEmpty(textEdit13.Text))
                {
                    XtraMessageBox.Show("成本总额不能为空");
                    return;
                }
                
                m.SPLBName = comboBoxEdit2.Text;//所属类别
                m.GGXH = textEdit3.Text;//规格型号
                m.YSSJ = textEdit4.Text.ObjToDecimal();//预设售价
                m.SPName = textEdit5.Text;//商品名称
                m.SPCode = textEdit2.Text;//商品编码
                m.IsZP= checkEdit2.Checked;//该商品是否可作为赠品
                m.DW = comboBoxEdit1.Text;//单位
                m.PFJ = textEdit7.Text.ObjToDecimal();//批发价
                m.YSJJ= textEdit6.Text.ObjToDecimal();//预设进价
                m.DQKC= textEdit11.Text.ObjToDecimal();//当前库存
                m.IsYSXS = checkEdit3.Checked;//预设进价作为销售成本价
                m.SPTM = textEdit9.Text;//商品条码
                m.KCXX = textEdit10.Text.ObjToDecimal();//库存下线
                m.QCKC = textEdit11.Text.ObjToDecimal();//期初库存
                m.SPJJ= textEdit12.Text.ObjToDecimal();//商品均价
                m.CBZE = textEdit13.Text.ObjToDecimal();//成本总额
                m.SPTP = "";//商品图片
                m.BZ = textEdit14.Text;
                if (_client.AddSP(m))
                {
                    mg.DataSource = _client.GetSPList();
                    string spcode = (_client.GetLastSPCode().TrimStart("SP".ToArray()).ObjToInt() + 1).ToString("00000");
                    textEdit2.Text = $"SP{spcode}";
                    comboBoxEdit2.Text = "";//所属类别
                    textEdit3.Text = "";//规格型号
                    textEdit4.Text = "";//预设售价
                    textEdit5.Text = "";//商品名称
                    textEdit2.Text = "";//商品编码
                    checkEdit2.Checked = false;//该商品是否可作为赠品
                    comboBoxEdit1.Text = "";//单位
                    textEdit7.Text = "";//批发价
                    textEdit6.Text = "";//预设进价
                    checkEdit3.Checked = false;//预设进价作为销售成本价
                    textEdit9.Text = "";//商品条码
                    textEdit10.Text = "";//库存下线
                    textEdit11.Text = "";//期初库存
                    textEdit12.Text = "";//商品均价
                    textEdit12.Text = "";//成本总额
                    textEdit14.Text = "";
                }
                
            }
        }

        private void ProductEditForm_Load(object sender, EventArgs e)
        {
            if(data!=null)
            {
                comboBoxEdit2.Properties.Items.AddRange(_client.GetDicDataList().Where(p => p.DicTypeName.Equals("商品类别")).Select(p => p.DicData).ToList());
                comboBoxEdit1.Properties.Items.AddRange(_client.GetDicDataList().Where(p => p.DicTypeName.Equals("商品单位")).Select(p => p.DicData).ToList());
                textEdit5.ReadOnly = true;
                textEdit2.ReadOnly= true;
                textEdit11.ReadOnly= true;
                textEdit5.Text = data.SPName;
                textEdit2.Text = data.SPCode;
                textEdit3.Text = data.GGXH;
                textEdit4.Text = data.YSSJ.ToString();
                comboBoxEdit2.Text = data.SPLBName;
                checkEdit2.Checked = data.IsZP.Value;
                comboBoxEdit1.Text = data.DW;
                textEdit7.Text = data.PFJ.ToString();
                textEdit6.Text = data.YSJJ.ToString();
                checkEdit3.Checked = data.IsYSXS.Value;
                textEdit9.Text = data.SPTM;
                textEdit10.Text = data.KCXX.ToString();
                textEdit11.Text = data.QCKC.ToString();
                textEdit12.Text = data.SPJJ.ToString();
                textEdit13.Text = data.CBZE.ToString();
                textEdit14.Text= data.BZ.ToString();
            }
            else
            {
                //设置商品编码
                string spcode = (_client.GetLastSPCode().TrimStart("SP".ToArray()).ObjToInt() + 1).ToString("00000");
                textEdit2.Text = $"SP{spcode}";
                comboBoxEdit2.Properties.Items.AddRange(_client.GetDicDataList().Where(p => p.DicTypeName.Equals("商品类别")).Select(p => p.DicData).ToList());
                comboBoxEdit2.SelectedIndex = 0;
                comboBoxEdit1.Properties.Items.AddRange(_client.GetDicDataList().Where(p => p.DicTypeName.Equals("商品单位")).Select(p => p.DicData).ToList());
                comboBoxEdit1.SelectedIndex = 0;
            }
        }
        /// <summary>
        /// 取消
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton3_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void checkEdit3_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit3.Checked)
            {
                textEdit13.Text = textEdit6.Text;
            }
        }

        private void textEdit6_EditValueChanged(object sender, EventArgs e)
        {
            if (checkEdit3.Checked)
            {
                textEdit13.Text = textEdit6.Text;
            }
        }
    }
}
