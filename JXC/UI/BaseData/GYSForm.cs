﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JXC.Models;

namespace JXC.UI.BaseData
{
    public partial class GYSForm : XtraForm
    {
        private APIClient client=new APIClient();
        public GYSForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 增加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton2_Click(object sender, EventArgs e)
        {
            var frm=new GYSEditForm();
            frm.mg = gridControl1;
            frm.ShowDialog();
        }

        private void GYSForm_Load(object sender, EventArgs e)
        {
            gridControl1.DataSource = client.GetGHSList();
            gridView1.OptionsBehavior.Editable=false;
            gridView1.BestFitColumns();
        }

        private void groupControl2_Paint(object sender, PaintEventArgs e)
        {

        }
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (gridView1.FocusedRowHandle < 0) return;
            var frm = new GYSEditForm();
            frm.IsEdit = true;
            frm.data = (ghsxx)gridView1.GetRow(gridView1.FocusedRowHandle);
            frm.mg= gridControl1;
            frm.ShowDialog();
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton4_Click(object sender, EventArgs e)
        {
            if (gridView1.FocusedRowHandle < 0) return;
            client.DeleteGHS(new List<ghsxx>() { (ghsxx)gridView1.GetRow(gridView1.FocusedRowHandle) });
            var data = client.GetGHSList();
            gridControl1.DataSource = data;
        }

        private void simpleButton10_Click(object sender, EventArgs e)
        {
            var data = client.GetGHSList();
            if (!string.IsNullOrEmpty(textEdit2.Text))
            {
                data=data.Where(p => p.GHSName.Equals(textEdit2.Text)).ToList();
            }

            gridControl1.DataSource = data;
        }
    }
}
