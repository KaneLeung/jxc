﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using JXC.Models;
using JXC.UI.BaseData;
using JXC.WebApi.EntityExtend;
using SqlSugar.Extensions;

namespace JXC.UI.InWarehouse
{
    public partial class CGRKForm : DevExpress.XtraEditors.XtraForm
    {
        public List<cgrkd> cgrkdListR = new List<cgrkd>();

        private APIClient client = new APIClient();
        public CGRKForm()
        {
            InitializeComponent();
        }

        #region 采购入库

        /// <summary>
        /// 老商品添加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton3_Click(object sender, EventArgs e)
        {
            var form = new LSPTJForm();

            if (form.ShowDialog() == DialogResult.OK)
            {
                cgrkdListR.AddRange(form.cgrkdListR);
                gridControl1.DataSource = null;
                gridControl1.DataSource = cgrkdListR;
                textEdit1.Text = cgrkdListR.Sum(p => p.ZJE).ToString();
                textEdit2.Text = cgrkdListR.Sum(p => p.ZJE).ToString();
                gridControl2.DataSource = client.GetCGRKMListN();
                gridControl4.DataSource = client.GetCGRKMList();
                if ((cgrkm)gridView2.GetRow(gridView2.FocusedRowHandle) != null)
                    gridControl3.DataSource = client.GetCGRKDList(((cgrkm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
                if ((cgrkm)gridView4.GetRow(gridView4.FocusedRowHandle) != null)
                    gridControl5.DataSource = client.GetCGRKDList(((cgrkm)gridView4.GetRow(gridView4.FocusedRowHandle)).Code);
            }
            
        }
        /// <summary>
        /// 新商品添加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton4_Click(object sender, EventArgs e)
        {
            new ProductEditForm().ShowDialog();
        }
        /// <summary>
        /// 单据导入
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton5_Click(object sender, EventArgs e)
        {

        }

        #endregion

        #region 待审核采购单


        /// <summary>
        /// 查看单据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton6_Click(object sender, EventArgs e)
        {
            if (gridView2.FocusedRowHandle < 0) return;
        }

        /// <summary>
        /// 删除单据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton7_Click(object sender, EventArgs e)
        {
            if (gridView2.FocusedRowHandle < 0) return;
            if (client.DelCGRKMList(new List<cgrkm>() { (cgrkm)gridView2.GetRow(gridView2.FocusedRowHandle) }))
            {

                gridControl2.DataSource = client.GetCGRKMListN();
                gridControl3.DataSource = null;
                if (gridView2.FocusedRowHandle >= 0)
                    gridControl3.DataSource = client.GetCGRKDList(((cgrkm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
            }
        }
        /// <summary>
        /// 审核数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton8_Click(object sender, EventArgs e)
        {
            if (gridView2.FocusedRowHandle < 0) return;
            var data = (cgrkm)gridView2.GetRow(gridView2.FocusedRowHandle);
            data.IsSH = true;
            data.SHRName = AppInfo.user.CZYName;
            data.SHTime = DateTime.Now;
            if (client.UpdateCGRKMSH(new List<cgrkm>()
                {
                    data
                }))
            {
                gridControl2.DataSource = null;
                gridControl2.DataSource = client.GetCGRKMListN();
                gridControl3.DataSource = null;
                if (gridView2.FocusedRowHandle >= 0)
                    gridControl3.DataSource = client.GetCGRKDList(((cgrkm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
                gridControl4.DataSource = null;
                gridControl4.DataSource = client.GetCGRKMList();
                gridControl5.DataSource = null;
                if (gridView4.FocusedRowHandle >= 0)
                    gridControl5.DataSource = client.GetCGRKDList(((cgrkm)gridView4.GetRow(gridView4.FocusedRowHandle)).Code);

            }
        }
        /// <summary>
        /// 修改数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton9_Click(object sender, EventArgs e)
        {
            if (gridView2.FocusedRowHandle < 0) return;

            var frm = new CGRKEditForm();
            frm.data = (cgrkm)gridView2.GetRow(gridView2.FocusedRowHandle);

            if (frm.ShowDialog() == DialogResult.OK)
            {
                if (client.UpdateCGRKM(frm.data))
                {
                    gridControl2.DataSource = client.GetCGRKMListN();
                    gridControl3.DataSource = client.GetCGRKDList(((cgrkm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
                }
            }
            

        }
        /// <summary>
        /// 导出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton10_Click(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// 打印
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton11_Click(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// 退出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton12_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        /// <summary>
        /// 新增商品
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton15_Click(object sender, EventArgs e)
        {
            if (gridView2.FocusedRowHandle < 0) return;
            var frm = new LSPTJForm();
            frm.code = ((cgrkm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code;
            frm.data = ((cgrkm)gridView2.GetRow(gridView2.FocusedRowHandle));

            if (frm.ShowDialog() == DialogResult.OK)
            {
                gridControl3.DataSource = null;
                gridControl3.DataSource = client.GetCGRKDList(frm.code);
                gridControl2.DataSource = null;
                gridControl2.DataSource = client.GetCGRKMListN();
            }
            
        }
        /// <summary>
        /// 编辑商品
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton14_Click(object sender, EventArgs e)
        {
            if (gridView3.FocusedRowHandle < 0) return;
            var frm = new SPXXForm();
            frm.odata = (cgrkd)gridView3.GetRow(gridView3.FocusedRowHandle);

            if (frm.ShowDialog() == DialogResult.OK)
            {
                if (client.UpdateCGRK(new CGRK()
                    {
                        m = (cgrkm)gridView2.GetRow(gridView2.FocusedRowHandle),
                        d = new List<cgrkd>() { frm.odata }
                    }))
                {
                    gridControl2.DataSource = client.GetCGRKMListN();
                    if ((cgrkm)gridView2.GetRow(gridView2.FocusedRowHandle) != null)
                        gridControl3.DataSource = client.GetCGRKDList(((cgrkm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
                }
            }
            

            //cgrkdListR[gridView2.FocusedRowHandle] = (form.odata);
            //gridControl2.DataSource = null;
            //gridControl2.DataSource = cgrkdListR;
        }
        #endregion

        #region 采购入库查询

        /// <summary>
        /// 查看单据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton17_Click(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// 取消审核
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton18_Click(object sender, EventArgs e)
        {
            if (gridView4.FocusedRowHandle < 0) return;
            var data = (cgrkm)gridView4.GetRow(gridView4.FocusedRowHandle);
            data.IsSH = false;
            data.SHRName = "";
            data.SHTime = null;
            if (client.UpdateCGRKMSH(new List<cgrkm>()
                {
                    data
                }))
            {
                gridControl2.DataSource = null;
                gridControl2.DataSource = client.GetCGRKMListN();
                gridControl3.DataSource = null;
                if (gridView2.FocusedRowHandle >= 0)
                    gridControl3.DataSource = client.GetCGRKDList(((cgrkm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
                gridControl4.DataSource = null;
                gridControl4.DataSource = client.GetCGRKMList();
                gridControl5.DataSource = null;
                if (gridView4.FocusedRowHandle >= 0)
                    gridControl5.DataSource = client.GetCGRKDList(((cgrkm)gridView4.GetRow(gridView4.FocusedRowHandle)).Code);

            }
        }
        /// <summary>
        /// 整单退货
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton19_Click(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// 导出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton21_Click(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// 打印
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton22_Click(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// 整单打印
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton20_Click(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// 退出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton23_Click(object sender, EventArgs e)
        {

        }


        #endregion

        private void CGRKForm_Load(object sender, EventArgs e)
        {
            textEdit1.ReadOnly = true;
            labelControl1.Text = $"单号 {client.GetCGRKDCode()}";
            dateEdit1.DateTime = DateTime.Now;
            comboBoxEdit2.Properties.Items.AddRange(client.GetGHSList().Where(p=>!p.IsJY.Value).Select(p => p.GHSName).ToList());
            comboBoxEdit2.SelectedItem = client.GetGHSList().Where(p => !p.IsJY.Value).FirstOrDefault(p => p.IsMR.Value).GHSName;
            gridControl2.DataSource = client.GetCGRKMListN();
            gridControl4.DataSource = client.GetCGRKMList();
            if ((cgrkm)gridView2.GetRow(gridView2.FocusedRowHandle) != null)
                gridControl3.DataSource = client.GetCGRKDList(((cgrkm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
            if ((cgrkm)gridView4.GetRow(gridView4.FocusedRowHandle) != null)
                gridControl5.DataSource = client.GetCGRKDList(((cgrkm)gridView4.GetRow(gridView4.FocusedRowHandle)).Code);

        }
        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (gridView1.RowCount == 0)
            {
                XtraMessageBox.Show("没有业务数据");
                return;
            }
            cgrkm m = new cgrkm();
            m.Code = labelControl1.Text.Replace("单号 ", "");
            m.CodeTime = dateEdit1.DateTime;
            m.GHSName = comboBoxEdit2.Text;
            m.YFJE = textEdit1.Text.ObjToDecimal();
            m.SFJE = textEdit2.Text.ObjToDecimal();
            m.Remark = textEdit3.Text;
            m.JSFS = comboBoxEdit1.Text;
            m.IsSH = false;
            m.QKJE = m.JSFS.Equals("按实付款结算") ? 0 : m.YFJE == m.SFJE ? 0 : m.YFJE - m.SFJE;
            m.CZYCode = AppInfo.user.CZYCode;
            m.CZYName = AppInfo.user.CZYName;
            var listD = (List<cgrkd>)gridControl1.DataSource;
            listD.ForEach(p =>
            {
                p.MCode = m.Code;
            });
            CGRK d = new CGRK();
            d.m = m;
            d.d = listD;
            if (client.AddCGRK(d))
            {
                gridControl1.DataSource = null;
                comboBoxEdit2.SelectedItem = client.GetGHSList().Where(p => !p.IsJY.Value).FirstOrDefault(p => p.IsMR.Value).GHSName;
                textEdit1.Text = string.Empty;
                textEdit2.Text = string.Empty;
                textEdit3.Text = string.Empty;
                gridControl2.DataSource = client.GetCGRKMListN();
                if ((cgrkm)gridView2.GetRow(gridView2.FocusedRowHandle) != null)
                    gridControl3.DataSource = client.GetCGRKDList(((cgrkm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
                //gridControl3.DataSource = client.GetCGRKMList();
                if ((cgrkm)gridView4.GetRow(gridView4.FocusedRowHandle) != null)
                    gridControl5.DataSource = client.GetCGRKDList(((cgrkm)gridView4.GetRow(gridView4.FocusedRowHandle)).Code);
                cgrkdListR.Clear();
                labelControl1.Text = $"单号 {client.GetCGRKDCode()}";
            }
        }

        private void gridControl2_Click(object sender, EventArgs e)
        {
            if ((cgrkm)gridView2.GetRow(gridView2.FocusedRowHandle) != null)
                gridControl3.DataSource = client.GetCGRKDList(((cgrkm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
        }

        private void gridControl4_Click(object sender, EventArgs e)
        {
            if ((cgrkm)gridView4.GetRow(gridView4.FocusedRowHandle) != null)
                gridControl5.DataSource = client.GetCGRKDList(((cgrkm)gridView4.GetRow(gridView4.FocusedRowHandle)).Code);
        }

        private void gridControl3_DoubleClick(object sender, EventArgs e)
        {
            var frm = new SPXXForm();
            frm.odata = (cgrkd)gridView3.GetRow(gridView3.FocusedRowHandle);

            if (frm.ShowDialog() == DialogResult.OK)
            {
                client.UpdateCGRKD(frm.odata);
                if ((cgrkm)gridView2.GetRow(gridView2.FocusedRowHandle) != null)
                    gridControl3.DataSource = client.GetCGRKDList(((cgrkm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
            }
           
        }
        /// <summary>
        /// 删除商品
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton16_Click(object sender, EventArgs e)
        {
            var data = (cgrkd)gridView3.GetRow(gridView3.FocusedRowHandle);
            List<cgrkd> list = new List<cgrkd>();
            list.Add(data);
            client.DelCGRKDList(list);
            gridControl2.DataSource = client.GetCGRKMListN();
            gridControl4.DataSource = client.GetCGRKMList();
            if ((cgrkm)gridView2.GetRow(gridView2.FocusedRowHandle) != null)
                gridControl3.DataSource = client.GetCGRKDList(((cgrkm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
            else
            {
                gridControl3.DataSource = null;
            }
            if ((cgrkm)gridView4.GetRow(gridView4.FocusedRowHandle) != null)
                gridControl5.DataSource = client.GetCGRKDList(((cgrkm)gridView4.GetRow(gridView4.FocusedRowHandle)).Code);
            else
            {
                gridControl5.DataSource = null;
            }
        }
        /// <summary>
        /// 未审核
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton13_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textEdit4.Text))
            {
                gridControl2.DataSource = client.GetCGRKMListN();

            }
            else
            {
                //todo 优化
                gridControl2.DataSource = client.GetCGRKMListN().Where(p => p.Code.Equals(textEdit4.Text));
            }
            if (gridView2.FocusedRowHandle >= 0)
            {
                gridControl3.DataSource = client.GetCGRKDList(((cgrkm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
            }
            else
            {
                gridControl3.DataSource = null;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton24_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textEdit5.Text))
            {
                gridControl4.DataSource = client.GetCGRKMList();
            }
            else
            {
                //todo 优化
                gridControl4.DataSource = client.GetCGRKMList().Where(p => p.Code.Equals(textEdit5.Text));
            }
            if (gridView4.FocusedRowHandle >= 0)
            {
                gridControl5.DataSource = client.GetCGRKDList(((cgrkm)gridView4.GetRow(gridView4.FocusedRowHandle)).Code);
            }
            else
            {
                gridControl5.DataSource = null;
            }
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton25_Click(object sender, EventArgs e)
        {
            if (gridView1.FocusedRowHandle < 0)
            {
                return;
            }

            List<cgrkd> list = (List<cgrkd>)gridControl1.DataSource;
            list.Remove((cgrkd)gridView1.GetRow(gridView1.FocusedRowHandle));
            gridControl1.DataSource = null;
            gridControl1.DataSource = list;
        }
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton27_Click(object sender, EventArgs e)
        {
            if (gridView1.FocusedRowHandle < 0) return;
            var list = (List<cgrkd>)gridView1.DataSource;
            var data = (cgrkd)gridView1.GetRow(gridView1.FocusedRowHandle);
            SPXXForm form = new SPXXForm();
            form.odata = data;

            if (form.ShowDialog() == DialogResult.OK)
            {
                int index = list.IndexOf(data);
                list.RemoveRange(index, 1);
                list.InsertRange(index, new List<cgrkd>() { form.odata });
                gridControl1.DataSource = null;
                gridControl1.DataSource = list;
                textEdit1.Text = list.Sum(p => p.ZJE).ToString();
                textEdit2.Text = list.Sum(p => p.ZJE).ToString();
            }
            

        }
        /// <summary>
        /// 打印
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton26_Click(object sender, EventArgs e)
        {
            // C: \Users\64842\Desktop\Devexpress进销存\JXC\JXC\JXC\Report\cgjh.fr3
            //MyReport r = new MyReport();

            //r.ShowRibbonDesigner();
        }
    }
}