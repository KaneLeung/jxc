﻿using DevExpress.XtraEditors;
using JXC.Models;
using JXC.WebApi.EntityExtend;
using Newtonsoft.Json;
using SqlSugar.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JXC.UI.InWarehouse
{
    public partial class CGTHForm : DevExpress.XtraEditors.XtraForm
    {
        public List<cgthd> cgrkdListR = new List<cgthd>();

        private APIClient client=new APIClient();
        public CGTHForm()
        {
            InitializeComponent();
        }
        /// <summary>
        /// 商品添加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton3_Click(object sender, EventArgs e)
        {
            var form = new LSPTJForm();
            form.title = "商品添加(采购退货)";
            form.type = "CGTH";
            if (form.ShowDialog() == DialogResult.OK)
            {
                cgrkdListR.AddRange(JsonConvert.DeserializeObject<List<cgthd>>(JsonConvert.SerializeObject(form.cgrkdListR)));
                gridControl1.DataSource = null;
                gridControl1.DataSource = cgrkdListR;
                textEdit1.Text = cgrkdListR.Sum(p => p.ZJE).ToString();
                textEdit2.Text = cgrkdListR.Sum(p => p.ZJE).ToString();
                gridControl2.DataSource = client.GetCGTHMListN();
                gridControl4.DataSource = client.GetCGTHMList();
                if ((cgthm)gridView2.GetRow(gridView2.FocusedRowHandle) != null)
                    gridControl3.DataSource = client.GetCGTHDList(((cgthm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
                if ((cgthm)gridView4.GetRow(gridView4.FocusedRowHandle) != null)
                    gridControl5.DataSource = client.GetCGTHDList(((cgthm)gridView4.GetRow(gridView4.FocusedRowHandle)).Code);
            }
           
        }

        private void CGTHForm_Load(object sender, EventArgs e)
        {
            textEdit1.ReadOnly = true;
            labelControl1.Text = $"单号 {client.GetCGTHDCode()}";
            dateEdit1.DateTime = DateTime.Now;
            comboBoxEdit1.Properties.Items.AddRange(client.GetGHSList().Select(p => p.GHSName).ToList());
            comboBoxEdit1.SelectedItem = client.GetGHSList().FirstOrDefault(p => p.IsMR.Value).GHSName;
            gridControl2.DataSource = client.GetCGTHMListN();
            gridControl4.DataSource = client.GetCGTHMList();
            if ((cgthm)gridView2.GetRow(gridView2.FocusedRowHandle) != null)
                gridControl3.DataSource = client.GetCGTHDList(((cgthm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
            if ((cgthm)gridView4.GetRow(gridView4.FocusedRowHandle) != null)
                gridControl5.DataSource = client.GetCGTHDList(((cgthm)gridView4.GetRow(gridView4.FocusedRowHandle)).Code);
        }
        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (gridView1.RowCount == 0)
            {
                XtraMessageBox.Show("没有业务数据");
                return;
            }
            cgthm m = new cgthm();
            m.Code = labelControl1.Text.Replace("单号 ", "");
            m.CodeTime = dateEdit1.DateTime;
            m.GHSName = comboBoxEdit1.Text;
            m.YSJE = textEdit1.Text.ObjToDecimal();
            m.SSJE = textEdit2.Text.ObjToDecimal();
            m.Remark = textEdit3.Text;
            //m.JSFS = comboBoxEdit1.Text;
            m.IsSH = false;
            m.QKJE =  m.YSJE - m.SSJE;
            m.CZYCode = AppInfo.user.CZYCode;
            m.CZYName = AppInfo.user.CZYName;
            var listD = (List<cgthd>)gridControl1.DataSource;
            listD.ForEach(p =>
            {
                p.MCode = m.Code;
            });
            CGTH d = new CGTH();
            d.m = m;
            d.d = listD;
            if (client.AddCGTH(d))
            {
                gridControl1.DataSource = null;
                comboBoxEdit1.SelectedItem = client.GetGHSList().FirstOrDefault(p => p.IsMR.Value).GHSName;
                textEdit1.Text = string.Empty;
                textEdit2.Text = string.Empty;
                textEdit3.Text = string.Empty;
                gridControl2.DataSource = client.GetCGTHMListN();
                if ((cgthm)gridView2.GetRow(gridView2.FocusedRowHandle) != null)
                    gridControl3.DataSource = client.GetCGTHDList(((cgthm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
                //gridControl3.DataSource = client.GetCGRKMList();
                if ((cgthm)gridView4.GetRow(gridView4.FocusedRowHandle) != null)
                    gridControl5.DataSource = client.GetCGTHDList(((cgthm)gridView4.GetRow(gridView4.FocusedRowHandle)).Code);
                cgrkdListR.Clear();
                labelControl1.Text = $"单号 {client.GetCGTHDCode()}";
            }
        }
        /// <summary>
        /// 退货审核搜索
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton13_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textEdit4.Text))
            {
                gridControl2.DataSource = client.GetCGTHMListN();

            }
            else
            {
                //todo 优化
                gridControl2.DataSource = client.GetCGTHMListN().Where(p => p.Code.Equals(textEdit4.Text));
            }
            if (gridView2.FocusedRowHandle >= 0)
            {
                gridControl3.DataSource = client.GetCGTHDList(((cgthm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
            }
            else
            {
                gridControl3.DataSource = null;
            }
        }

        private void gridControl2_Click(object sender, EventArgs e)
        {
            if ((cgthm)gridView2.GetRow(gridView2.FocusedRowHandle) != null)
                gridControl3.DataSource = client.GetCGTHDList(((cgthm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
        }

        private void gridControl2_DoubleClick(object sender, EventArgs e)
        {

        }

        private void gridControl3_DoubleClick(object sender, EventArgs e)
        {
            var frm = new SPXXForm();
            frm.odata =JsonConvert.DeserializeObject<cgrkd>(JsonConvert.SerializeObject(gridView3.GetRow(gridView3.FocusedRowHandle)));
            if (frm.ShowDialog() == DialogResult.OK)
            {
                client.UpdateCGTHD(JsonConvert.DeserializeObject<cgthd>(JsonConvert.SerializeObject(frm.odata)));
                if ((cgthm)gridView2.GetRow(gridView2.FocusedRowHandle) != null)
                    gridControl3.DataSource = client.GetCGRKDList(((cgthm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
            }
            
        }
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton27_Click(object sender, EventArgs e)
        {
            if (gridView1.FocusedRowHandle < 0) return;
            var list = (List<cgthd>)gridView1.DataSource;
            var data = (cgthd)gridView1.GetRow(gridView1.FocusedRowHandle);
            SPXXForm form = new SPXXForm();
            form.odata =JsonConvert.DeserializeObject<cgrkd>(JsonConvert.SerializeObject(data)) ;

            if (form.ShowDialog() == DialogResult.OK)
            {
                int index = list.IndexOf(data);
                list.RemoveRange(index, 1);
                list.InsertRange(index, new List<cgthd>() { JsonConvert.DeserializeObject<cgthd>(JsonConvert.SerializeObject(form.odata)) });
                gridControl1.DataSource = null;
                gridControl1.DataSource = list;
                textEdit1.Text = list.Sum(p => p.ZJE).ToString();
                textEdit2.Text = list.Sum(p => p.ZJE).ToString();
            }
            
        }
        /// <summary>
        ///
        /// 删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton25_Click(object sender, EventArgs e)
        {
            if (gridView1.FocusedRowHandle < 0)
            {
                return;
            }

            List<cgthd> list = (List<cgthd>)gridControl1.DataSource;
            list.Remove((cgthd)gridView1.GetRow(gridView1.FocusedRowHandle));
            gridControl1.DataSource = null;
            gridControl1.DataSource = list;
        }
        /// <summary>
        /// 删除单据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton7_Click(object sender, EventArgs e)
        {
            if (gridView2.FocusedRowHandle < 0) return;
            if (client.DelCGTHMList(new List<cgthm>() { (cgthm)gridView2.GetRow(gridView2.FocusedRowHandle) }))
            {

                gridControl2.DataSource = client.GetCGTHMListN();
                gridControl3.DataSource = null;
                if (gridView2.FocusedRowHandle >= 0)
                    gridControl3.DataSource = client.GetCGTHDList(((cgthm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
            }
        }
        /// <summary>
        /// 审核数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton8_Click(object sender, EventArgs e)
        {
            if (gridView2.FocusedRowHandle < 0) return;
            var data = (cgthm)gridView2.GetRow(gridView2.FocusedRowHandle);
            data.IsSH = true;
            data.SHRName = AppInfo.user.CZYName;
            data.SHTime = DateTime.Now;
            if (client.UpdateCGTHMSH(new List<cgthm>()
                {
                    data
                }))
            {
                gridControl2.DataSource = null;
                gridControl2.DataSource = client.GetCGTHMListN();
                gridControl3.DataSource = null;
                if (gridView2.FocusedRowHandle >= 0)
                    gridControl3.DataSource = client.GetCGTHDList(((cgthm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
                gridControl4.DataSource = null;
                gridControl4.DataSource = client.GetCGTHMList();
                gridControl5.DataSource = null;
                if (gridView4.FocusedRowHandle >= 0)
                    gridControl5.DataSource = client.GetCGTHDList(((cgthm)gridView4.GetRow(gridView4.FocusedRowHandle)).Code);

            }
        }
        /// <summary>
        /// 修改单据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton9_Click(object sender, EventArgs e)
        {
            if (gridView2.FocusedRowHandle < 0) return;

            var frm = new CGRKEditForm();
            frm.data =JsonConvert.DeserializeObject<cgrkm>(JsonConvert.SerializeObject(gridView2.GetRow(gridView2.FocusedRowHandle))) ;
            if (frm.ShowDialog() == DialogResult.OK)
            {
                if (client.UpdateCGTHM(JsonConvert.DeserializeObject<cgthm>(JsonConvert.SerializeObject(frm.data))))
                {
                    gridControl2.DataSource = client.GetCGTHMListN();
                    gridControl3.DataSource = client.GetCGTHDList(((cgthm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
                }
            }
        }
        /// <summary>
        /// 新增商品
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton15_Click(object sender, EventArgs e)
        {
            var frm = new LSPTJForm();
            frm.code = ((cgthm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code;
            frm.data = (JsonConvert.DeserializeObject<cgrkm>(JsonConvert.SerializeObject(gridView2.GetRow(gridView2.FocusedRowHandle))));

            if (frm.ShowDialog() == DialogResult.OK)
            {
                gridControl3.DataSource = null;
                gridControl3.DataSource = client.GetCGTHDList(frm.code);
                gridControl2.DataSource = null;
                gridControl2.DataSource = client.GetCGTHMListN();
            }
          
        }
        /// <summary>
        /// 修改商品
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton14_Click(object sender, EventArgs e)
        {
            if (gridView3.FocusedRowHandle < 0) return;
            var frm = new SPXXForm();
            frm.odata =JsonConvert.DeserializeObject<cgrkd>(JsonConvert.SerializeObject(gridView3.GetRow(gridView3.FocusedRowHandle))) ;

            if (frm.ShowDialog() == DialogResult.OK)
            {
                if (client.UpdateCGTH(new CGTH()
                    {
                        m = (cgthm)gridView2.GetRow(gridView2.FocusedRowHandle),
                        d = new List<cgthd>() { JsonConvert.DeserializeObject<cgthd>(JsonConvert.SerializeObject(frm.odata)) }
                    }))
                {
                    gridControl2.DataSource = client.GetCGTHMListN();
                    if ((cgthm)gridView2.GetRow(gridView2.FocusedRowHandle) != null)
                        gridControl3.DataSource = client.GetCGTHDList(((cgthm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
                }
            }
            

        }
        /// <summary>
        /// 删除商品
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton16_Click(object sender, EventArgs e)
        {
            var data = (cgthd)gridView3.GetRow(gridView3.FocusedRowHandle);
            List<cgthd> list = new List<cgthd>();
            list.Add(data);
            client.DelCGTHDList(list);
            gridControl2.DataSource = client.GetCGTHMListN();
            gridControl4.DataSource = client.GetCGTHMList();
            if ((cgthm)gridView2.GetRow(gridView2.FocusedRowHandle) != null)
                gridControl3.DataSource = client.GetCGTHDList(((cgthm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
            else
            {
                gridControl3.DataSource = null;
            }
            if ((cgthm)gridView4.GetRow(gridView4.FocusedRowHandle) != null)
                gridControl5.DataSource = client.GetCGTHDList(((cgthm)gridView4.GetRow(gridView4.FocusedRowHandle)).Code);
            else
            {
                gridControl5.DataSource = null;
            }
        }
        /// <summary>
        /// 取消审核
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton18_Click(object sender, EventArgs e)
        {
            if (gridView4.FocusedRowHandle < 0) return;
            var data = (cgthm)gridView4.GetRow(gridView4.FocusedRowHandle);
            data.IsSH = false;
            data.SHRName = "";
            data.SHTime = null;
            if (client.UpdateCGTHMSH(new List<cgthm>()
                {
                    data
                }))
            {
                gridControl2.DataSource = null;
                gridControl2.DataSource = client.GetCGTHMListN();
                gridControl3.DataSource = null;
                if (gridView2.FocusedRowHandle >= 0)
                    gridControl3.DataSource = client.GetCGTHDList(((cgthm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
                gridControl4.DataSource = null;
                gridControl4.DataSource = client.GetCGTHMList();
                gridControl5.DataSource = null;
                if (gridView4.FocusedRowHandle >= 0)
                    gridControl5.DataSource = client.GetCGTHDList(((cgthm)gridView4.GetRow(gridView4.FocusedRowHandle)).Code);

            }
        }
        /// <summary>
        /// 搜索
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton4_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textEdit5.Text))
            {
                gridControl4.DataSource = client.GetCGTHMList();
            }
            else
            {
                //todo 优化
                gridControl4.DataSource = client.GetCGTHMList().Where(p => p.Code.Equals(textEdit5.Text));
            }
            if (gridView4.FocusedRowHandle >= 0)
            {
                gridControl5.DataSource = client.GetCGTHDList(((cgthm)gridView4.GetRow(gridView4.FocusedRowHandle)).Code);
            }
            else
            {
                gridControl5.DataSource = null;
            }
        }

        private void gridControl4_Click(object sender, EventArgs e)
        {
            if ((cgthm)gridView4.GetRow(gridView4.FocusedRowHandle) != null)
                gridControl5.DataSource = client.GetCGTHDList(((cgthm)gridView4.GetRow(gridView4.FocusedRowHandle)).Code);
        }
    }
}