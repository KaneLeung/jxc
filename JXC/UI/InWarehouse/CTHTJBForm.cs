﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JXC.UI.InWarehouse
{
    public partial class CTHTJBForm : DevExpress.XtraEditors.XtraForm
    {
        private APIClient client=new APIClient();
        public CTHTJBForm()
        {
            InitializeComponent();
        }
        /// <summary>
        /// 关闭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton12_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        /// <summary>
        /// 搜索
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton13_Click(object sender, EventArgs e)
        {
            gridControl2.DataSource = client.GetCGTHTJBList(textEdit4.Text, textEdit1.Text);
        }

        private void CTHTJBForm_Load(object sender, EventArgs e)
        {
            gridView2.OptionsBehavior.Editable = false;

            gridControl2.DataSource = client.GetCGTHTJBList(textEdit4.Text,textEdit1.Text);
        }
    }
}