﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using JXC.Models;
using SqlSugar.Extensions;

namespace JXC.UI.InWarehouse
{
    public partial class CGRKEditForm : DevExpress.XtraEditors.XtraForm
    {
        private APIClient client=new APIClient();
        public cgrkm data;
        public CGRKEditForm()
        {
            InitializeComponent();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CGRKEditForm_Load(object sender, EventArgs e)
        {
            comboBoxEdit1.Properties.Items.AddRange(client.GetGHSList().Select(p=>p.GHSName).ToList());
            if (data != null)
            {
                labelControl2.Text = data.Code;
                comboBoxEdit1.SelectedItem = data.GHSName;
                dateEdit1.DateTime = data.CodeTime.Value;
                textEdit1.Text = data.YFJE.ToString();
                textEdit2.Text = data.SFJE.ToString();
                comboBoxEdit5.SelectedItem = data.JSFS;
                textEdit3.Text = data.Remark;
            }
        }
        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            data.GHSName = comboBoxEdit1.Text;
            data.CodeTime = dateEdit1.DateTime;
            data.SFJE = textEdit2.Text.ObjToDecimal();
            data.QKJE = data.YFJE - data.SFJE;
            data.JSFS = comboBoxEdit5.Text;
            data.Remark = textEdit3.Text;
            DialogResult= DialogResult.OK;
            this.Close();
        }
    }
}