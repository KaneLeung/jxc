﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JXC.Models;
using JXC.UI.BaseData;
using JXC.WebApi.EntityExtend;
using SqlSugar.Extensions;

namespace JXC.UI.InWarehouse
{
    public partial class LSPTJForm : XtraForm
    {
        public List<cgrkd> cgrkdListL=new List<cgrkd>();
        public List<cgrkd> cgrkdListR = new List<cgrkd>();
        public string code = string.Empty;
        public cgrkm data ; 
        private APIClient client=new APIClient();
        public string type = "CGJH";//默认采购进货  采购退货CGTH
        public string title = "商品添加(采购进货)";
        public LSPTJForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 增加新商品
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param> 
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            new ProductEditForm().ShowDialog();
        }

        /// <summary>
        /// 加入所选商品
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton2_Click(object sender, EventArgs e)
        {
            var frm = new SPXXForm();
            frm.type=type;
            frm.data= (spxx)gridView1.GetRow(gridView1.FocusedRowHandle);
            frm.number = textEdit2.Text.ObjToDecimal();
            if (frm.ShowDialog() == DialogResult.OK)
            {
                cgrkdListR.Add(frm.odata);
                gridControl2.DataSource = null;
                gridControl2.DataSource = cgrkdListR;
            }
           
        }

        private void LSPTJForm_Load(object sender, EventArgs e)
        {
            gridControl1.DataSource = client.GetSPList();
            this.Text = title;
        }

        private void gridControl1_DoubleClick(object sender, EventArgs e)
        {
            var data = (spxx)gridView1.GetRow(gridView1.FocusedRowHandle);
            var form = new SPXXForm();
            form.type = type;
            form.number = textEdit2.Text.ObjToDecimal();
            form.data=data;
            if (form.ShowDialog() == DialogResult.OK)
            {
                cgrkdListR.Add(form.odata);
                gridControl2.DataSource = null;
                gridControl2.DataSource = cgrkdListR;
            }
           
        }

        private void gridControl2_DoubleClick(object sender, EventArgs e)
        {
            var data = (cgrkd)gridView2.GetRow(gridView2.FocusedRowHandle);
            var form = new SPXXForm();
            form.type = type;
            form.odata = data;
            if (form.ShowDialog() == DialogResult.OK)
            {
                cgrkdListR[gridView2.FocusedRowHandle] = (form.odata);
                gridControl2.DataSource = null;
                gridControl2.DataSource = cgrkdListR;
            }
            
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            var data = (cgrkd)gridView2.GetRow(gridView2.FocusedRowHandle);
            var form = new SPXXForm();
            form.odata = data;
            form.type = type;
            if (form.ShowDialog() == DialogResult.OK)
            {
                cgrkdListR[gridView2.FocusedRowHandle] = (form.odata);
                gridControl2.DataSource = null;
                gridControl2.DataSource = cgrkdListR;
            }

        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            //var data = (cgrkd)gridView2.GetRow(gridView2.FocusedRowHandle);
            //var form = new SPXXForm();
            //form.odata = data;
            //form.ShowDialog();
            cgrkdListR.RemoveAt(gridView2.FocusedRowHandle);;
            gridControl2.DataSource = null;
            gridControl2.DataSource = cgrkdListR;
        }

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(code))
            {
                var data = (List<cgrkd>)gridView2.DataSource;
                data.ForEach(p =>
                {
                    p.MCode = code;
                });
                var d = new CGRK() { d = data,m=this.data };
                if (client.AddCGRK(d))
                {

                }
            }
            DialogResult= DialogResult.OK;
            this.Close();
        }

        private void simpleButton6_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
