﻿using DevExpress.XtraEditors;
using JXC.Models;
using JXC.UI.InWarehouse;
using JXC.WebApi.EntityExtend;
using SqlSugar.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JXC.UI.OutWarehouse
{
    public partial class SPXSForm : DevExpress.XtraEditors.XtraForm
    {
        public List<spxsd> spxsdListR = new List<spxsd>();

        private APIClient client = new APIClient();
        public SPXSForm()
        {
            InitializeComponent();
        }

        private void SPXSForm_Load(object sender, EventArgs e)
        {
            textEdit1.ReadOnly = true;
            labelControl1.Text = $"单号 {client.GetSPXSDCode()}";
            dateEdit1.DateTime = DateTime.Now;
            comboBoxEdit2.Properties.Items.AddRange(client.GetKHList().Select(p => p.KHName).ToList());
            comboBoxEdit2.SelectedItem = client.GetKHList().FirstOrDefault(p => p.IsMR.Value).KHName;
            gridControl2.DataSource = client.GetSPXSMListN();
            gridControl4.DataSource = client.GetSPXSMList();
            if ((spxsm)gridView2.GetRow(gridView2.FocusedRowHandle) != null)
                gridControl3.DataSource = client.GetSPXSDList(((spxsm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
            if ((spxsm)gridView4.GetRow(gridView4.FocusedRowHandle) != null)
                gridControl5.DataSource = client.GetSPXSDList(((spxsm)gridView4.GetRow(gridView4.FocusedRowHandle)).Code);
        }
        /// <summary>
        /// 商品添加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton3_Click(object sender, EventArgs e)
        {
            var form = new LSPTJForm();

            if (form.ShowDialog() == DialogResult.OK)
            {
                spxsdListR.AddRange(form.spxsdListR);
                gridControl1.DataSource = null;
                gridControl1.DataSource = spxsdListR;
                textEdit1.Text = spxsdListR.Sum(p => p.ZJE).ToString();
                textEdit2.Text = spxsdListR.Sum(p => p.ZJE).ToString();
                gridControl2.DataSource = client.GetSPXSMListN();
                gridControl4.DataSource = client.GetSPXSMList();
                if ((spxsm)gridView2.GetRow(gridView2.FocusedRowHandle) != null)
                    gridControl3.DataSource = client.GetSPXSDList(((spxsm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
                if ((spxsm)gridView4.GetRow(gridView4.FocusedRowHandle) != null)
                    gridControl5.DataSource = client.GetSPXSDList(((spxsm)gridView4.GetRow(gridView4.FocusedRowHandle)).Code);
            }
            
        }
        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (gridView1.RowCount == 0)
            {
                XtraMessageBox.Show("没有业务数据");
                return;
            }
            spxsm m = new spxsm();
            m.Code = labelControl1.Text.Replace("单号 ", "");
            m.CHRQ = dateEdit1.DateTime;
            m.KHName = comboBoxEdit2.Text;
            m.YSJE = textEdit1.Text.ObjToDecimal();
            m.SSJE = textEdit2.Text.ObjToDecimal();
            m.BZ = textEdit3.Text;
            m.JSFS = comboBoxEdit1.Text;
            m.IsSH = false;
            m.QKJE = m.JSFS.Equals("按实付款结算") ? 0 : m.YSJE == m.SSJE ? 0 : m.YSJE - m.SSJE;
            m.CZYCode = AppInfo.user.CZYCode;
            m.CZYName = AppInfo.user.CZYName;
            var listD = (List<spxsd>)gridControl1.DataSource;
            listD.ForEach(p =>
            {
                p.MCode = m.Code;
            });
            SPXS d = new SPXS();
            d.m = m;
            d.d = listD;
            if (client.AddSPXS(d))
            {
                gridControl1.DataSource = null;
                comboBoxEdit2.SelectedItem = client.GetKHList().FirstOrDefault(p => p.IsMR.Value).KHName;
                textEdit1.Text = string.Empty;
                textEdit2.Text = string.Empty;
                textEdit3.Text = string.Empty;
                gridControl2.DataSource = client.GetSPXSMListN();
                if ((spxsm)gridView2.GetRow(gridView2.FocusedRowHandle) != null)
                    gridControl3.DataSource = client.GetSPXSDList(((spxsm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
                //gridControl3.DataSource = client.GetCGRKMList();
                if ((spxsm)gridView4.GetRow(gridView4.FocusedRowHandle) != null)
                    gridControl5.DataSource = client.GetSPXSDList(((spxsm)gridView4.GetRow(gridView4.FocusedRowHandle)).Code);
                spxsdListR.Clear();
                labelControl1.Text = $"单号 {client.GetSPXSDCode()}";
            }
        }
        /// <summary>
        /// 删除单据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton7_Click(object sender, EventArgs e)
        {
            if (gridView2.FocusedRowHandle < 0) return;
            if (client.DelSPXSMList(new List<spxsm>() { (spxsm)gridView2.GetRow(gridView2.FocusedRowHandle) }))
            {

                gridControl2.DataSource = client.GetSPXSMListN();
                gridControl3.DataSource = null;
                if (gridView2.FocusedRowHandle>=0)
                    gridControl3.DataSource = client.GetSPXSDList(((spxsm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
            }
        }
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton28_Click(object sender, EventArgs e)
        {
            if (gridView1.FocusedRowHandle < 0) return;
            var list = (List<spxsd>)gridView1.DataSource;
            var data = (spxsd)gridView1.GetRow(gridView1.FocusedRowHandle);
            SPXXForm form = new SPXXForm();
            form.odata = data;
            if (form.ShowDialog() == DialogResult.OK)
            {
                int index = list.IndexOf(data);
                list.RemoveRange(index, 1);
                list.InsertRange(index, new List<spxsd>() { form.odata });
                gridControl1.DataSource = null;
                gridControl1.DataSource = list;
                textEdit1.Text = list.Sum(p => p.ZJE).ToString();
                textEdit2.Text = list.Sum(p => p.ZJE).ToString();
            }
           
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton27_Click(object sender, EventArgs e)
        {
            if (gridView1.FocusedRowHandle < 0)
            {
                return;
            }

            List<spxsd> list = (List<spxsd>)gridControl1.DataSource;
            list.Remove((spxsd)gridView1.GetRow(gridView1.FocusedRowHandle));
            gridControl1.DataSource = null;
            gridControl1.DataSource = list;
        }
        /// <summary>
        /// 打印
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton26_Click(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// 社和数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton8_Click(object sender, EventArgs e)
        {
            if (gridView2.FocusedRowHandle < 0) return;
            var data = (spxsm)gridView2.GetRow(gridView2.FocusedRowHandle);
            data.IsSH = true;
            data.SHRName = AppInfo.user.CZYName;
            data.SHRQ = DateTime.Now;
            if (client.UpdateSPXSMSH(new List<spxsm>()
                {
                    data
                }))
            {
                gridControl2.DataSource = null;
                gridControl2.DataSource = client.GetSPXSMListN();
                gridControl3.DataSource = null;
                if (gridView2.FocusedRowHandle >= 0)
                    gridControl3.DataSource = client.GetSPXSDList(((spxsm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
                gridControl4.DataSource = null;
                gridControl4.DataSource = client.GetSPXSMList();
                gridControl5.DataSource = null;
                if (gridView4.FocusedRowHandle >= 0)
                    gridControl5.DataSource = client.GetSPXSDList(((spxsm)gridView4.GetRow(gridView4.FocusedRowHandle)).Code);

            }
        }
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton9_Click(object sender, EventArgs e)
        {
            if (gridView2.FocusedRowHandle < 0) return;

            var frm = new CGRKEditForm();
            frm.data = (spxsm)gridView2.GetRow(gridView2.FocusedRowHandle);

            if (frm.ShowDialog() == DialogResult.OK)
            {
                if (client.UpdateSPXSM(frm.data))
                {
                    gridControl2.DataSource = client.GetSPXSMListN();
                    gridControl3.DataSource = client.GetSPXSDList(((spxsm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
                }
            }
          
        }
        /// <summary>
        /// 取消审核
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton18_Click(object sender, EventArgs e)
        {
            if (gridView4.FocusedRowHandle < 0) return;
            var data = (spxsm)gridView4.GetRow(gridView4.FocusedRowHandle);
            data.IsSH = false;
            data.SHRName = "";
            data.SHRQ = null;
            if (client.UpdateSPXSMSH(new List<spxsm>()
                {
                    data
                }))
            {
                gridControl2.DataSource = null;
                gridControl2.DataSource = client.GetSPXSMListN();
                gridControl3.DataSource = null;
                if (gridView2.FocusedRowHandle >= 0)
                    gridControl3.DataSource = client.GetSPXSDList(((spxsm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
                gridControl4.DataSource = null;
                gridControl4.DataSource = client.GetSPXSMList();
                gridControl5.DataSource = null;
                if (gridView4.FocusedRowHandle >= 0)
                    gridControl5.DataSource = client.GetSPXSDList(((spxsm)gridView4.GetRow(gridView4.FocusedRowHandle)).Code);

            }
        }
        /// <summary>
        /// 新增商品
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton15_Click(object sender, EventArgs e)
        {
            var frm = new LSPTJForm();
            frm.code = ((spxsm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code;
            frm.data = ((spxsm)gridView2.GetRow(gridView2.FocusedRowHandle));

            if (frm.ShowDialog() == DialogResult.OK)
            {
                gridControl3.DataSource = null;
                gridControl3.DataSource = client.GetSPXSDList(frm.code);
                gridControl2.DataSource = null;
                gridControl2.DataSource = client.GetSPXSMListN();
            }
            
        }
        /// <summary>
        /// 修改商品
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton14_Click(object sender, EventArgs e)
        {
            if (gridView3.FocusedRowHandle < 0) return;
            var frm = new SPXXForm();
            frm.odata = (spxsd)gridView3.GetRow(gridView3.FocusedRowHandle);

            if (frm.ShowDialog() == DialogResult.OK)
            {
                if (client.UpdateSPXS(new SPXS()
                    {
                        m = (spxsm)gridView2.GetRow(gridView2.FocusedRowHandle),
                        d = new List<spxsd>() { frm.odata }
                    }))
                {
                    gridControl2.DataSource = client.GetSPXSMListN();
                    if ((spxsm)gridView2.GetRow(gridView2.FocusedRowHandle) != null)
                        gridControl3.DataSource = client.GetSPXSDList(((spxsm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
                }
            }
            

           
        }
        /// <summary>
        /// 删除商品
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton16_Click(object sender, EventArgs e)
        {
            var data = (spxsd)gridView3.GetRow(gridView3.FocusedRowHandle);
            List<spxsd> list = new List<spxsd>();
            list.Add(data);
            client.DelSPXSDList(list);
            gridControl2.DataSource = client.GetSPXSMListN();
            gridControl4.DataSource = client.GetSPXSMList();
            if ((spxsm)gridView2.GetRow(gridView2.FocusedRowHandle) != null)
                gridControl3.DataSource = client.GetSPXSDList(((spxsm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
            else
            {
                gridControl3.DataSource = null;
            }
            if ((spxsm)gridView4.GetRow(gridView4.FocusedRowHandle) != null)
                gridControl5.DataSource = client.GetSPXSDList(((spxsm)gridView4.GetRow(gridView4.FocusedRowHandle)).Code);
            else
            {
                gridControl5.DataSource = null;
            }
        }
        /// <summary>
        /// 搜索1
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton13_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textEdit4.Text))
            {
                gridControl2.DataSource = client.GetSPXSMListN();

            }
            else
            {
                //todo 优化
                gridControl2.DataSource = client.GetSPXSMListN().Where(p => p.Code.Equals(textEdit4.Text));
            }
            if (gridView2.FocusedRowHandle >= 0)
            {
                gridControl3.DataSource = client.GetSPXSDList(((spxsm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
            }
            else
            {
                gridControl3.DataSource = null;
            }
        }
        /// <summary>
        /// 搜索2
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton24_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textEdit5.Text))
            {
                gridControl4.DataSource = client.GetSPXSMList();
            }
            else
            {
                //todo 优化
                gridControl4.DataSource = client.GetSPXSMList().Where(p => p.Code.Equals(textEdit5.Text));
            }
            if (gridView4.FocusedRowHandle >= 0)
            {
                gridControl5.DataSource = client.GetSPXSDList(((spxsm)gridView4.GetRow(gridView4.FocusedRowHandle)).Code);
            }
            else
            {
                gridControl5.DataSource = null;
            }
        }
        /// <summary>
        /// 退出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void gridControl3_DoubleClick(object sender, EventArgs e)
        {
            if (gridView3.FocusedRowHandle < 0) return;
            var frm = new SPXXForm();
            frm.odata = (spxsd)gridView3.GetRow(gridView3.FocusedRowHandle);
            if (frm.ShowDialog() == DialogResult.OK)
            {
                if (client.UpdateSPXS(new SPXS()
                    {
                        m = (spxsm)gridView2.GetRow(gridView2.FocusedRowHandle),
                        d = new List<spxsd>() { frm.odata }
                    }))
                {
                    gridControl2.DataSource = client.GetSPXSMListN();
                    if ((spxsm)gridView2.GetRow(gridView2.FocusedRowHandle) != null)
                        gridControl3.DataSource = client.GetSPXSDList(((spxsm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
                }
            }
           
        }

        private void splitContainerControl2_Panel1_Click(object sender, EventArgs e)
        {
          
        }

        private void gridControl4_Click(object sender, EventArgs e)
        {
            if ((spxsm)gridView4.GetRow(gridView4.FocusedRowHandle) != null)
                gridControl5.DataSource = client.GetSPXSDList(((spxsm)gridView4.GetRow(gridView4.FocusedRowHandle)).Code);
        }

        private void gridControl2_Click(object sender, EventArgs e)
        {
            if ((spxsm)gridView2.GetRow(gridView2.FocusedRowHandle) != null)
                gridControl3.DataSource = client.GetSPXSDList(((spxsm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
        }
    }
}