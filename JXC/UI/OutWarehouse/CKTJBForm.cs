﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JXC.UI.OutWarehouse
{
    public partial class CKTJBForm : DevExpress.XtraEditors.XtraForm
    {
        private APIClient client = new APIClient();

        public CKTJBForm()
        {
            InitializeComponent();
        }
        /// <summary>
        /// 搜索
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton13_Click(object sender, EventArgs e)
        {
            gridControl2.DataSource = client.GetXSCKTJBList(textEdit4.Text, textEdit1.Text);

        }

        private void CKTJBForm_Load(object sender, EventArgs e)
        {
            gridControl2.DataSource = client.GetXSCKTJBList(textEdit4.Text, textEdit1.Text);

        }
    }
}