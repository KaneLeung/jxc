﻿using DevExpress.XtraEditors;
using JXC.Models;
using JXC.WebApi.EntityExtend;
using Newtonsoft.Json;
using SqlSugar.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JXC.UI.OutWarehouse
{
    public partial class GKTHForm : DevExpress.XtraEditors.XtraForm
    {
        public List<xsthd> spxsdListR = new List<xsthd>();

        private APIClient client = new APIClient();
        public GKTHForm()
        {
            InitializeComponent();
        }
        /// <summary>
        /// 商品添加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton3_Click(object sender, EventArgs e)
        {
            var form = new LSPTJForm();
            form.title = "商品添加(销售退货)";
            if (form.ShowDialog() == DialogResult.OK)
            {
                spxsdListR.AddRange(JsonConvert.DeserializeObject<List<xsthd>>(JsonConvert.SerializeObject(form.spxsdListR)));
                gridControl1.DataSource = null;
                gridControl1.DataSource = spxsdListR;
                textEdit1.Text = spxsdListR.Sum(p => p.ZJE).ToString();
                textEdit2.Text = spxsdListR.Sum(p => p.ZJE).ToString();
                gridControl2.DataSource = client.GetXSTHMListN();
                gridControl4.DataSource = client.GetXSTHMList();
                if ((xsthm)gridView2.GetRow(gridView2.FocusedRowHandle) != null)
                    gridControl3.DataSource = client.GetXSTHDList(((xsthm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
                if ((xsthm)gridView4.GetRow(gridView4.FocusedRowHandle) != null)
                    gridControl5.DataSource = client.GetXSTHDList(((xsthm)gridView4.GetRow(gridView4.FocusedRowHandle)).Code);
            }
           
        }
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton28_Click(object sender, EventArgs e)
        {
            if (gridView1.FocusedRowHandle < 0) return;
            var list = (List<xsthd>)gridView1.DataSource;
            var data = (xsthd)gridView1.GetRow(gridView1.FocusedRowHandle);
            SPXXForm form = new SPXXForm();
            form.odata =JsonConvert.DeserializeObject<spxsd>(JsonConvert.SerializeObject(data)) ;

            if (form.ShowDialog() == DialogResult.OK)
            {
                int index = list.IndexOf(data);
                list.RemoveRange(index, 1);
                list.InsertRange(index, new List<xsthd>() { JsonConvert.DeserializeObject<xsthd>(JsonConvert.SerializeObject(form.odata)) });
                gridControl1.DataSource = null;
                gridControl1.DataSource = list;
                textEdit1.Text = list.Sum(p => p.ZJE).ToString();
                textEdit2.Text = list.Sum(p => p.ZJE).ToString();
            }
            
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton27_Click(object sender, EventArgs e)
        {
            if (gridView1.FocusedRowHandle < 0)
            {
                return;
            }

            List<xsthd> list = (List<xsthd>)gridControl1.DataSource;
            list.Remove((xsthd)gridView1.GetRow(gridView1.FocusedRowHandle));
            gridControl1.DataSource = null;
            gridControl1.DataSource = list;
        }

        private void GKTHForm_Load(object sender, EventArgs e)
        {
            textEdit1.ReadOnly = true;
            labelControl1.Text = $"单号 {client.GetXSTHDCode()}";
            dateEdit1.DateTime = DateTime.Now;
            comboBoxEdit1.Properties.Items.AddRange(client.GetKHList().Select(p => p.KHName).ToList());
            comboBoxEdit1.SelectedItem = client.GetKHList().FirstOrDefault(p => p.IsMR.Value).KHName;
            gridControl2.DataSource = client.GetXSTHMListN();
            gridControl4.DataSource = client.GetXSTHMList();
            if ((xsthm)gridView2.GetRow(gridView2.FocusedRowHandle) != null)
                gridControl3.DataSource = client.GetXSTHDList(((xsthm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
            if ((xsthm)gridView4.GetRow(gridView4.FocusedRowHandle) != null)
                gridControl5.DataSource = client.GetXSTHDList(((xsthm)gridView4.GetRow(gridView4.FocusedRowHandle)).Code);
        }
        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (gridView1.RowCount == 0)
            {
                XtraMessageBox.Show("没有业务数据");
                return;
            }
            xsthm m = new xsthm();
            m.Code = labelControl1.Text.Replace("单号 ", "");
            m.CHRQ = dateEdit1.DateTime;
            m.KHName = comboBoxEdit1.Text;
            m.YSJE = textEdit1.Text.ObjToDecimal();
            m.SSJE = textEdit2.Text.ObjToDecimal();
            m.BZ = textEdit3.Text;
            m.JSFS = labelControl14.Text;
            m.IsSH = false;
            m.QKJE = m.YSJE - m.SSJE;
            m.CZYCode = AppInfo.user.CZYCode;
            m.CZYName = AppInfo.user.CZYName;
            var listD = (List<xsthd>)gridControl1.DataSource;
            listD.ForEach(p =>
            {
                p.MCode = m.Code;
            });
            XSTH d = new XSTH();
            d.m = m;
            d.d = listD;
            if (client.AddXSTH(d))
            {
                gridControl1.DataSource = null;
                comboBoxEdit1.SelectedItem = client.GetKHList().FirstOrDefault(p => p.IsMR.Value).KHName;
                textEdit1.Text = string.Empty;
                textEdit2.Text = string.Empty;
                textEdit3.Text = string.Empty;
                gridControl2.DataSource = client.GetXSTHMListN();
                if ((xsthm)gridView2.GetRow(gridView2.FocusedRowHandle) != null)
                    gridControl3.DataSource = client.GetXSTHDList(((xsthm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
                //gridControl3.DataSource = client.GetCGRKMList();
                if ((xsthm)gridView4.GetRow(gridView4.FocusedRowHandle) != null)
                    gridControl5.DataSource = client.GetXSTHDList(((xsthm)gridView4.GetRow(gridView4.FocusedRowHandle)).Code);
                spxsdListR.Clear();
                labelControl1.Text = $"单号 {client.GetXSTHDCode()}";
            }
        }
        /// <summary>
        /// 删除单据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton7_Click(object sender, EventArgs e)
        {
            if (gridView2.FocusedRowHandle < 0) return;
            if (client.DelXSTHMList(new List<xsthm>() { (xsthm)gridView2.GetRow(gridView2.FocusedRowHandle) }))
            {

                gridControl2.DataSource = client.GetXSTHMListN();
                gridControl3.DataSource = null;
                if (gridView2.FocusedRowHandle >= 0)
                    gridControl3.DataSource = client.GetXSTHDList(((xsthm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
            }
        }
        /// <summary>
        /// 审核数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton8_Click(object sender, EventArgs e)
        {
            if (gridView2.FocusedRowHandle < 0) return;
            var data = (xsthm)gridView2.GetRow(gridView2.FocusedRowHandle);
            data.IsSH = true;
            data.SHRName = AppInfo.user.CZYName;
            data.SHRQ = DateTime.Now;
            if (client.UpdateXSTHMSH(new List<xsthm>()
                {
                    data
                }))
            {
                gridControl2.DataSource = null;
                gridControl2.DataSource = client.GetXSTHMListN();
                gridControl3.DataSource = null;
                if (gridView2.FocusedRowHandle >= 0)
                    gridControl3.DataSource = client.GetXSTHDList(((xsthm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
                gridControl4.DataSource = null;
                gridControl4.DataSource = client.GetXSTHMList();
                gridControl5.DataSource = null;
                if (gridView4.FocusedRowHandle >= 0)
                    gridControl5.DataSource = client.GetXSTHDList(((xsthm)gridView4.GetRow(gridView4.FocusedRowHandle)).Code);

            }
        }
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton9_Click(object sender, EventArgs e)
        {
            if (gridView2.FocusedRowHandle < 0) return;

            var frm = new CGRKEditForm();
            frm.data = JsonConvert.DeserializeObject<spxsm>(JsonConvert.SerializeObject((xsthm)gridView2.GetRow(gridView2.FocusedRowHandle)));

            if (frm.ShowDialog() == DialogResult.OK)
            {
                if (client.UpdateXSTHM(JsonConvert.DeserializeObject<xsthm>(JsonConvert.SerializeObject(frm.data))))
                {
                    gridControl2.DataSource = client.GetXSTHMListN();
                    gridControl3.DataSource = client.GetXSTHDList(((xsthm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
                }
            }
            
        }
        /// <summary>
        /// 搜索1
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton13_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textEdit4.Text))
            {
                gridControl2.DataSource = client.GetXSTHMListN();

            }
            else
            {
                //todo 优化
                gridControl2.DataSource = client.GetXSTHMListN().Where(p => p.Code.Equals(textEdit4.Text));
            }
            if (gridView2.FocusedRowHandle >= 0)
            {
                gridControl3.DataSource = client.GetXSTHDList(((xsthm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
            }
            else
            {
                gridControl3.DataSource = null;
            }
        }
        /// <summary>
        /// 新增商品
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton15_Click(object sender, EventArgs e)
        {
            var frm = new LSPTJForm();
            frm.code = ((xsthm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code;
            frm.data = JsonConvert.DeserializeObject<spxsm>(JsonConvert.SerializeObject((xsthm)gridView2.GetRow(gridView2.FocusedRowHandle)));

            if (frm.ShowDialog() == DialogResult.OK)
            {
                gridControl3.DataSource = null;
                gridControl3.DataSource = client.GetXSTHDList(frm.code);
                gridControl2.DataSource = null;
                gridControl2.DataSource = client.GetXSTHMListN();
            }
           
        }
        /// <summary>
        /// 修改商品
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton14_Click(object sender, EventArgs e)
        {
            if (gridView3.FocusedRowHandle < 0) return;
            var frm = new SPXXForm();
            frm.odata =JsonConvert.DeserializeObject<spxsd>(JsonConvert.SerializeObject((xsthd)gridView3.GetRow(gridView3.FocusedRowHandle))) ;

            if (frm.ShowDialog() == DialogResult.OK)
            {
                if (client.UpdateXSTH(new XSTH()
                    {
                        m = (xsthm)gridView2.GetRow(gridView2.FocusedRowHandle),
                        d = new List<xsthd>() { JsonConvert.DeserializeObject<xsthd>(JsonConvert.SerializeObject(frm.odata)) }
                    }))
                {
                    gridControl2.DataSource = client.GetXSTHMListN();
                    if ((xsthm)gridView2.GetRow(gridView2.FocusedRowHandle) != null)
                        gridControl3.DataSource = client.GetXSTHDList(((xsthm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
                }
            }
            
        }
        /// <summary>
        /// 删除商品
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton16_Click(object sender, EventArgs e)
        {
            var data = (xsthd)gridView3.GetRow(gridView3.FocusedRowHandle);
            List<xsthd> list = new List<xsthd>();
            list.Add(data);
            client.DelXSTHDList(list);
            gridControl2.DataSource = client.GetXSTHMListN();
            gridControl4.DataSource = client.GetXSTHMList();
            if ((xsthm)gridView2.GetRow(gridView2.FocusedRowHandle) != null)
                gridControl3.DataSource = client.GetXSTHDList(((xsthm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
            else
            {
                gridControl3.DataSource = null;
            }
            if ((xsthm)gridView4.GetRow(gridView4.FocusedRowHandle) != null)
                gridControl5.DataSource = client.GetXSTHDList(((xsthm)gridView4.GetRow(gridView4.FocusedRowHandle)).Code);
            else
            {
                gridControl5.DataSource = null;
            }
        }

        private void gridControl3_DoubleClick(object sender, EventArgs e)
        {
            if (gridView3.FocusedRowHandle < 0) return;
            var frm = new SPXXForm();
            frm.odata = JsonConvert.DeserializeObject<spxsd>(JsonConvert.SerializeObject((xsthd)gridView3.GetRow(gridView3.FocusedRowHandle)));

            if (frm.ShowDialog() == DialogResult.OK)
            {
                if (client.UpdateXSTH(new XSTH()
                    {
                        m = (xsthm)gridView2.GetRow(gridView2.FocusedRowHandle),
                        d = new List<xsthd>() { JsonConvert.DeserializeObject<xsthd>(JsonConvert.SerializeObject(frm.odata)) }
                    }))
                {
                    gridControl2.DataSource = client.GetXSTHMListN();
                    if ((xsthm)gridView2.GetRow(gridView2.FocusedRowHandle) != null)
                        gridControl3.DataSource = client.GetXSTHDList(((xsthm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
                }
            }
            
        }
        /// <summary>
        /// 搜索2
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton4_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textEdit5.Text))
            {
                gridControl4.DataSource = client.GetXSTHMList();
            }
            else
            {
                //todo 优化
                gridControl4.DataSource = client.GetXSTHMList().Where(p => p.Code.Equals(textEdit5.Text));
            }
            if (gridView4.FocusedRowHandle >= 0)
            {
                gridControl5.DataSource = client.GetXSTHDList(((xsthm)gridView4.GetRow(gridView4.FocusedRowHandle)).Code);
            }
            else
            {
                gridControl5.DataSource = null;
            }
        }
        /// <summary>
        /// 取消审核
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton18_Click(object sender, EventArgs e)
        {
            if (gridView4.FocusedRowHandle < 0) return;
            var data = (xsthm)gridView4.GetRow(gridView4.FocusedRowHandle);
            data.IsSH = false;
            data.SHRName = "";
            data.SHRQ = null;
            if (client.UpdateXSTHMSH(new List<xsthm>()
                {
                    data
                }))
            {
                gridControl2.DataSource = null;
                gridControl2.DataSource = client.GetXSTHMListN();
                gridControl3.DataSource = null;
                if (gridView2.FocusedRowHandle >= 0)
                    gridControl3.DataSource = client.GetXSTHDList(((xsthm)gridView2.GetRow(gridView2.FocusedRowHandle)).Code);
                gridControl4.DataSource = null;
                gridControl4.DataSource = client.GetXSTHMList();
                gridControl5.DataSource = null;
                if (gridView4.FocusedRowHandle >= 0)
                    gridControl5.DataSource = client.GetXSTHDList(((xsthm)gridView4.GetRow(gridView4.FocusedRowHandle)).Code);

            }
        }

        private void gridControl4_Click(object sender, EventArgs e)
        {
            if ((xsthm)gridView4.GetRow(gridView4.FocusedRowHandle) != null)
                gridControl5.DataSource = client.GetXSTHDList(((xsthm)gridView4.GetRow(gridView4.FocusedRowHandle)).Code);
        }
    }
}