﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JXC.UI.OutWarehouse
{
    public partial class XTHTJBForm : DevExpress.XtraEditors.XtraForm
    {
        private APIClient client = new APIClient();

        public XTHTJBForm()
        {
            InitializeComponent();
        }
        /// <summary>
        /// 搜索
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton13_Click(object sender, EventArgs e)
        {
            gridControl2.DataSource = client.GetXSTHTJBList(textEdit4.Text, textEdit1.Text);

        }

        private void XTHTJBForm_Load(object sender, EventArgs e)
        {
            gridControl2.DataSource = client.GetXSTHTJBList(textEdit4.Text, textEdit1.Text);

        }
    }
}