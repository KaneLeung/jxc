﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using JXC.Models;
using SqlSugar.Extensions;

namespace JXC.UI.OutWarehouse
{
    public partial class CGRKEditForm : DevExpress.XtraEditors.XtraForm
    {
        private APIClient client=new APIClient();
        public spxsm data;
        public CGRKEditForm()
        {
            InitializeComponent();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CGRKEditForm_Load(object sender, EventArgs e)
        {
            comboBoxEdit1.Properties.Items.AddRange(client.GetKHList().Select(p=>p.KHName).ToList());
            if (data != null)
            {
                labelControl2.Text = data.Code;
                comboBoxEdit1.SelectedItem = data.KHName;
                dateEdit1.DateTime = data.CHRQ.Value;
                textEdit1.Text = data.YSJE.ToString();
                textEdit2.Text = data.SSJE.ToString();
                comboBoxEdit5.SelectedItem = data.JSFS;
                textEdit3.Text = data.BZ;
            }
        }
        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            data.KHName = comboBoxEdit1.Text;
            data.CHRQ = dateEdit1.DateTime;
            data.SSJE = textEdit2.Text.ObjToDecimal();
            data.QKJE = data.YSJE - data.SSJE;
            data.JSFS = comboBoxEdit5.Text;
            data.BZ = textEdit3.Text;
            DialogResult= DialogResult.OK;
            this.Close();
        }
    }
}