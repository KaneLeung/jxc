﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using JXC.Models;
using SqlSugar.Extensions;

namespace JXC.UI.OutWarehouse
{
    public partial class SPXXForm : DevExpress.XtraEditors.XtraForm
    {
        private APIClient _client=new APIClient();
        public spxx data=null;
        public spxsd odata = null;
        public decimal number;//默认数量
        public SPXXForm()
        {
            InitializeComponent();
        }

        private void SPXXForm_Load(object sender, EventArgs e)
        {
            if (data != null)
            {
                textEdit1.Text = data.SPCode;
                textEdit2.Text = data.SPName;
                textEdit4.Text = data.GGXH;
                textEdit3.Text = data.DW;
                textEdit6.Text = data.DQKC.ToString();
                textEdit5.Text = data.YSSJ.ToString();
                textEdit8.EditValue = number;
            }
            else
            {
                textEdit1.Text = odata.SPCode;
                textEdit2.Text = odata.SPName;
                textEdit4.Text = odata.GGXH;
                textEdit3.Text = odata.DW;
                textEdit6.Text = _client.GetSPList().FirstOrDefault(p=>p.SPCode.Equals(odata.SPCode)).DQKC.ToString();
                textEdit5.Text = odata.DJ.ToString();
                textEdit8.Text=odata.SL.ToString();
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            
            var d = new spxsd();
            d.SPCode= textEdit1.Text;
            d.SPName= textEdit2.Text;
            d.DW= textEdit3.Text;
            d.SL = textEdit8.Text.ObjToDecimal();
            d.DJ= textEdit5.Text.ObjToDecimal();
            d.ZJE= textEdit7.Text.ObjToDecimal();
            d.BZ = textEdit10.Text;
            d.GGXH = textEdit4.Text;
            if (odata!=null&&odata.Id > 0)
            {
                d.Id = odata.Id;
                d.MCode=odata.MCode;
            }
               
            odata = d;
            DialogResult= DialogResult.OK;
            this.Close();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void textEdit8_EditValueChanged(object sender, EventArgs e)
        {
            textEdit7.Text=(textEdit5.Text.ObjToDecimal()*textEdit8.Text.ObjToDecimal()).ToString();
        }
    }
}