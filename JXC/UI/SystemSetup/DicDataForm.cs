﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using JXC.WebApi.Entity;

namespace JXC.UI.SystemSetup
{
    public partial class DicDataForm : DevExpress.XtraEditors.XtraForm
    {
        private APIClient client=new APIClient();
        public DicDataForm()
        {
            InitializeComponent();
        }

        private void simpleButton6_Click(object sender, EventArgs e)
        {
            new DicDataEditForm().ShowDialog();
            gridControl2.DataSource = client.GetDicDataList();

        }

        private void simpleButton12_Click(object sender, EventArgs e)
        {

        }

        private void DicDataForm_Load(object sender, EventArgs e)
        {
            gridView2.OptionsBehavior.Editable=false;
            gridControl2.DataSource = client.GetDicDataList();
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton8_Click(object sender, EventArgs e)
        {
            if (gridView2.FocusedRowHandle < 0) return;
            client.DeleteDicData((dicData)gridView2.GetRow(gridView2.FocusedRowHandle));
            gridControl2.DataSource = client.GetDicDataList();
        }

        private void simpleButton7_Click(object sender, EventArgs e)
        {
            if (gridView2.FocusedRowHandle < 0) return;
            var frm = new DicDataEditForm();
            frm.ShowDialog();
            gridControl2.DataSource = client.GetDicDataList();
        }
    }
}
