﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using JXC.Models;
using MD5Hash;

namespace JXC.UI.SystemSetup
{
    public partial class PWDEditForm : DevExpress.XtraEditors.XtraForm
    {
        private APIClient client=new APIClient();
        public PWDEditForm()
        {
            InitializeComponent();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            AppInfo.user.PWD = textEdit3.Text.GetMD5().ToUpper();
            client.UpdateCZY(AppInfo.user);
            this.Close();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void PWDEditForm_Load(object sender, EventArgs e)
        {
            
                textEdit1.Text = AppInfo.user.CZYCode;
                textEdit2.Text = AppInfo.user.CZYName;
                
        }
    }
}