﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using JXC.Models;
using JXC.WebApi.EntityExtend;

namespace JXC.UI.SystemSetup
{
    public partial class JSGLForm : DevExpress.XtraEditors.XtraForm
    {
        private APIClient _client = new APIClient();
        public JSGLForm()
        {
            InitializeComponent();
        }


        /// <summary>
        /// 增加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton6_Click(object sender, EventArgs e)
        {
            var frm= new JSGLEditForm();

            frm.ShowDialog();
            gridControl2.DataSource = _client.GetJSGLList();

        }

        private void simpleButton7_Click(object sender, EventArgs e)
        {
            if (gridView2.FocusedRowHandle < 0) return;
            var frm = new JSGLEditForm();
            frm.data = (jsgl)gridView2.GetRow(gridView2.FocusedRowHandle);
            frm.ShowDialog();
            gridControl2.DataSource = _client.GetJSGLList();
        }

        private void simpleButton8_Click(object sender, EventArgs e)
        {
            if (gridView2.FocusedRowHandle < 0) return;
            _client.DeleteJSGL(new JSGLM() { m = (jsgl)gridView2.GetRow(gridView2.FocusedRowHandle) });
            gridControl2.DataSource = _client.GetJSGLList();
        }

        private void JSGLForm_Load(object sender, EventArgs e)
        {
            gridControl2.DataSource = _client.GetJSGLList();
        }
    }
}