﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraTreeList.Nodes;
using JXC.Core;
using JXC.Models;
using JXC.WebApi.EntityExtend;
using DevExpress.Map.Kml.Model;

namespace JXC.UI.SystemSetup
{
    public partial class JSGLEditForm : DevExpress.XtraEditors.XtraForm
    {
        private APIClient client = new APIClient();
        public jsgl data;
        public JSGLEditForm()
        {
            InitializeComponent();
        }
        /// <summary>
        /// 全选
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkEdit1_CheckedChanged(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// queding 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (data == null)
            {
                if (string.IsNullOrEmpty(textEdit3.Text))
                {
                    "角色名称不能为空".ShowTips();
                    return;
                }
                JSGLM m = new JSGLM();
                m.m = new jsgl()
                {
                    JSName = textEdit3.Text
                };
                foreach (TreeListNode treeList1Node in treeList1.Nodes)
                {
                    if (treeList1Node.Checked)
                    {
                        jsgn g = new jsgn();
                        g.GNBM = treeList1Node.Tag.ToString();
                        m.d.Add(g);
                    }
                    GetGN(treeList1Node, m);
                }
                client.AddJSGL(m);
            }
            else
            {
                JSGLM m = new JSGLM();
                m.m = data;
                foreach (TreeListNode treeList1Node in treeList1.Nodes)
                {
                    if (treeList1Node.Checked)
                    {
                        jsgn g = new jsgn();
                        g.GNBM = treeList1Node.Tag.ToString();
                        g.JSId = data.Id;
                        m.d.Add(g);
                    }
                    GetGN(treeList1Node, m);
                }

                client.UpdateJSGL(m);
            }
            this.Close();

        }

        private void GetGN(TreeListNode root,JSGLM m)
        {
            foreach (TreeListNode treeListNode in root.Nodes)
            {
                if (treeListNode.Checked)
                {
                    jsgn g = new jsgn();
                    g.GNBM = treeListNode.Tag.ToString();
                    m.d.Add(g);
                }
                if (treeListNode.HasChildren)
                {
                    GetGN(treeListNode, m);
                }
            }
        }
        private void SetGN(TreeListNode root, List<jsgn> list)
        {
            foreach (TreeListNode treeListNode in root.Nodes)
            {
                if (list.Select(p=>p.GNBM).Contains(treeListNode.Tag.ToString()))
                {
                    treeListNode.Checked = true;
                }
                if (treeListNode.HasChildren)
                {
                    SetGN(treeListNode, list);
                }
            }
        }
        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //勾选|取消勾选 父节点，递归处理子节点
        private bool m_SetClick = true;
        private void SetSubNodeCheck(TreeListNode p_TreeNode, bool p_SelectCheck)
        {
            m_SetClick = false;
            foreach (TreeListNode _SubNode in p_TreeNode.Nodes)
            {
                _SubNode.Checked = p_SelectCheck;
                SetSubNodeCheck(_SubNode, p_SelectCheck);
            }
        }
        //勾选子节点，父节点同样勾上
        private void SetParentCheck(TreeListNode p_TreeNode)
        {
            if (p_TreeNode.Checked && p_TreeNode.ParentNode != null)
            {
                p_TreeNode.ParentNode.Checked = true;
                SetParentCheck(p_TreeNode.ParentNode);
            }
        }
        //如果当前分支下最后一个勾选的子节点取消勾选，父节点勾选状态取消
        private void SetParentNotCheck(TreeListNode p_TreeNode)
        {
            if (!p_TreeNode.Checked && p_TreeNode.ParentNode != null)
            {
                foreach (TreeListNode _Node in p_TreeNode.ParentNode.Nodes)
                {
                    if (_Node.Checked) return;
                }
                p_TreeNode.ParentNode.Checked = false;
                SetParentNotCheck(p_TreeNode.ParentNode);
            }
        }
        private void treeList1_AfterCheckNode(object sender, DevExpress.XtraTreeList.NodeEventArgs e)
        {
            // 禁用TreeView视图重绘的功能。
            treeList1.BeginUpdate();
            if (m_SetClick)
            {
                SetSubNodeCheck(e.Node, e.Node.Checked);
                SetParentCheck(e.Node);
                SetParentNotCheck(e.Node);
                m_SetClick = true;
            }
            // 启用TreeView视图重绘的功能。
            treeList1.EndUpdate();
        }

        private void JSGLEditForm_Load(object sender, EventArgs e)
        {
            if (data != null)
            {
                textEdit3.Text = data.JSName;
                textEdit3.ReadOnly = true;
                List<jsgn> list = client.GetJSGNList(data.Id);
                foreach (TreeListNode treeList1Node in treeList1.Nodes)
                {
                    if (list.Select(p=>p.GNBM).Contains(treeList1Node.Tag.ToString()))
                    {
                        treeList1Node.Checked=true;
                    }
                    SetGN(treeList1Node, list);
                }
            }
        }
    }
}