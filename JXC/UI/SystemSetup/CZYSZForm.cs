﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using JXC.Models;

namespace JXC.UI.SystemSetup
{
    public partial class CZYSZForm : DevExpress.XtraEditors.XtraForm
    {
        private APIClient client = new APIClient();
        public CZYSZForm()
        {
            InitializeComponent();
        }

        private void simpleButton6_Click(object sender, EventArgs e)
        {
            var frm = new CZYSZEditForm();
            frm.ShowDialog();
            gridControl2.DataSource = client.GetCZYList();
        }

        private void CZYSZForm_Load(object sender, EventArgs e)
        {
            gridControl2.DataSource = client.GetCZYList();
        }

        private void simpleButton8_Click(object sender, EventArgs e)
        {
            if (gridView2.FocusedRowHandle < 0) return;
            client.DeleteCZY((czysz)gridView2.GetRow(gridView2.FocusedRowHandle));
            gridControl2.DataSource = client.GetCZYList();

        }

        private void simpleButton7_Click(object sender, EventArgs e)
        {
            if (gridView2.FocusedRowHandle < 0) return;
            var frm= new CZYSZEditForm();
            frm.data = (czysz)gridView2.GetRow(gridView2.FocusedRowHandle);
            frm.ShowDialog();
            gridControl2.DataSource = client.GetCZYList();

        }

        private void simpleButton13_Click(object sender, EventArgs e)
        {
            var list= client.GetCZYList();
            if (!string.IsNullOrEmpty(textEdit1.Text))
            {
                list= list.Where(p=>p.CZYCode.Equals(textEdit1.Text)).ToList();
            }
            gridControl2.DataSource= list;
        }
    }
}