﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using JXC.WebApi.Entity;

namespace JXC.UI.SystemSetup
{
    public partial class DicTypeForm : DevExpress.XtraEditors.XtraForm
    {
        private readonly APIClient client=new();
        public DicTypeForm()
        {
            InitializeComponent();
        }
        /// <summary>
        /// 增加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton6_Click(object sender, EventArgs e)
        {
            new DicTypeEditForm().ShowDialog();
            gridControl2.DataSource = client.GetDicTypeList();
        }

        private void DicTypeForm_Load(object sender, EventArgs e)
        {
            gridView2.OptionsBehavior.Editable = false;
            gridControl2.DataSource = client.GetDicTypeList();
        }

        private void simpleButton8_Click(object sender, EventArgs e)
        {
            if (gridView2.FocusedRowHandle < 0) return;
            client.DeleteDicType((dicType)gridView2.GetRow(gridView2.FocusedRowHandle));
            gridControl2.DataSource = client.GetDicTypeList();

        }

        private void simpleButton7_Click(object sender, EventArgs e)
        {
            if (gridView2.FocusedRowHandle < 0) return;
            var frm = new DicTypeEditForm();
            frm.data = (dicType)gridView2.GetRow(gridView2.FocusedRowHandle);
            frm.ShowDialog();
            gridControl2.DataSource = client.GetDicTypeList();
        }
    }
}