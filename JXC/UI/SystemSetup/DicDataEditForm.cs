﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors.Controls;
using JXC.WebApi.Entity;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.TextBox;

namespace JXC.UI.SystemSetup
{
    public partial class DicDataEditForm : DevExpress.XtraEditors.XtraForm
    {
        public dicData data;
        private APIClient client=new APIClient();
        public DicDataEditForm()
        {
            InitializeComponent();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (data == null)
            {
                dicData m = new dicData();
                if (string.IsNullOrEmpty(textEdit3.Text))
                {
                    XtraMessageBox.Show("字典数据不能为空");
                    return;
                }
                m.DicTypeName = comboBoxEdit1.Text;
                m.BZ = textEdit1.Text;
                m.DicData = textEdit3.Text;
                client.AddDicData(m);
            }
            else
            {
                data.BZ=textEdit1.Text;
                client.UpdateDicData(data);
            }
            this.Close();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void DicDataEditForm_Load(object sender, EventArgs e)
        {
            comboBoxEdit1.Properties.TextEditStyle=TextEditStyles.DisableTextEditor;
            comboBoxEdit1.Properties.Items.AddRange(client.GetDicTypeList().Select(p=>p.DicTypeName).ToList());
            comboBoxEdit1.SelectedIndex = 0;
            if (data != null)
            {
                comboBoxEdit1.Text = data.DicTypeName;
                comboBoxEdit1.ReadOnly=true;
                textEdit3.ReadOnly = true;
                textEdit3.Text=data.DicData;
                textEdit1.Text = data.BZ;
            }
        }
    }
}