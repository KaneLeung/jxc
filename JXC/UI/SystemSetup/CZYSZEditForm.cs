﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using JXC.Commons;
using JXC.Core;
using JXC.Models;
using MD5Hash;

namespace JXC.UI.SystemSetup
{
    public partial class CZYSZEditForm : DevExpress.XtraEditors.XtraForm
    {
        private APIClient client = new();
        public czysz data;
        public CZYSZEditForm()
        {
            InitializeComponent();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (data == null)
            {
                if (string.IsNullOrEmpty(textEdit3.Text))
                {
                    "用户编码不能为空".ShowTips();
                    return;
                }
                if (string.IsNullOrEmpty(textEdit5.Text))
                {
                    "用户名称不能为空".ShowTips();
                    return;
                }

                if (string.IsNullOrEmpty(comboBoxEdit1.Text))
                {
                    "角色不能为空".ShowTips();
                    return;
                }
                if (string.IsNullOrEmpty(textEdit1.Text))
                {
                    "用户密码不能为空".ShowTips();
                    return;
                }
                czysz u = new czysz();
                u.CZYCode = textEdit3.Text;
                u.CZYName = textEdit5.Text;
                u.PWD = textEdit1.Text.GetMD5().ToUpper();
                u.IsTY = false;
                u.SSJSName = comboBoxEdit1.Text;
                client.AddCZY(u);
            }
            else
            {
                data.SSJSName = comboBoxEdit1.Text;
                client.UpdateCZY(data);
            }
            this.Close();

        }

        private void CZYSZEditForm_Load(object sender, EventArgs e)
        {
            comboBoxEdit1.Properties.Items.AddRange(client.GetJSGLList().Select(p=>p.JSName).ToList());
            comboBoxEdit1.SelectedIndex = 0;
            if (data != null)
            {
                textEdit3.Text = data.CZYCode;
                textEdit5.Text = data.CZYName;
                textEdit1.ReadOnly = true;
                textEdit3.ReadOnly = true;
                textEdit5.ReadOnly = true;
                //textEdit1.Text =AESEncryption.AESDecrypt(data.PWD) ;
                comboBoxEdit1.Text = data.SSJSName;
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}