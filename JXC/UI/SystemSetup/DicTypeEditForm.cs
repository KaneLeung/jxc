﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using JXC.WebApi.Entity;

namespace JXC.UI.SystemSetup
{
    public partial class DicTypeEditForm : DevExpress.XtraEditors.XtraForm
    {
        public dicType data;
        private APIClient client=new APIClient();
        public DicTypeEditForm()
        {
            InitializeComponent();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (data == null)
            {
                dicType m = new dicType();
                if (string.IsNullOrEmpty(textEdit3.Text))
                {
                    XtraMessageBox.Show("类型名称不能为空");
                    return;
                }
                m.BZ = textEdit1.Text;
                m.DicTypeName = textEdit3.Text;
                client.AddDicType(m);
            }
            else
            {
                data.BZ= textEdit1.Text;
                client.UpdateDicType(data);
            }
            this.Close();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void DicTypeEditForm_Load(object sender, EventArgs e)
        {
            if (data != null)
            {
                textEdit1.Text= data.BZ;
                textEdit3.Text=data.DicTypeName;
                textEdit3.ReadOnly=true;
            }
        }
    }
}