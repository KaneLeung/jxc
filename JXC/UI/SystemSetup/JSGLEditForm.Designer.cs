﻿namespace JXC.UI.SystemSetup
{
    partial class JSGLEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.textEdit3 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.treeList1 = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumn1 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeList1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.textEdit3);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.GroupStyle = DevExpress.Utils.GroupStyle.Light;
            this.groupControl1.Location = new System.Drawing.Point(12, 12);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(311, 60);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "角色信息";
            // 
            // textEdit3
            // 
            this.textEdit3.Location = new System.Drawing.Point(75, 29);
            this.textEdit3.Name = "textEdit3";
            this.textEdit3.Size = new System.Drawing.Size(143, 20);
            this.textEdit3.TabIndex = 13;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(17, 32);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(52, 14);
            this.labelControl3.TabIndex = 12;
            this.labelControl3.Text = "角色名称:";
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.treeList1);
            this.groupControl2.GroupStyle = DevExpress.Utils.GroupStyle.Light;
            this.groupControl2.Location = new System.Drawing.Point(12, 78);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(311, 371);
            this.groupControl2.TabIndex = 1;
            this.groupControl2.Text = "操作权限";
            // 
            // treeList1
            // 
            this.treeList1.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn1});
            this.treeList1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeList1.Location = new System.Drawing.Point(2, 23);
            this.treeList1.Name = "treeList1";
            this.treeList1.BeginUnboundLoad();
            this.treeList1.AppendNode(new object[] {
            "基础数据"}, -1, "JCSJ");
            this.treeList1.AppendNode(new object[] {
            "商品管理"}, 0, "btn1");
            this.treeList1.AppendNode(new object[] {
            "供货商管理"}, 0, "btn2");
            this.treeList1.AppendNode(new object[] {
            "客户管理"}, 0, "btn3");
            this.treeList1.AppendNode(new object[] {
            "入库管理"}, -1, "RKGL");
            this.treeList1.AppendNode(new object[] {
            "采购入库"}, 4, "btn14");
            this.treeList1.AppendNode(new object[] {
            "采购退货"}, 4, "btn15");
            this.treeList1.AppendNode(new object[] {
            "入库统计"}, 4, "btn18");
            this.treeList1.AppendNode(new object[] {
            "退货统计"}, 4, "btn19");
            this.treeList1.AppendNode(new object[] {
            "出库管理"}, -1, "CKGL");
            this.treeList1.AppendNode(new object[] {
            "商品销售"}, 9, "btn20");
            this.treeList1.AppendNode(new object[] {
            "顾客退货"}, 9, "btn21");
            this.treeList1.AppendNode(new object[] {
            "出库统计"}, 9, "btn24");
            this.treeList1.AppendNode(new object[] {
            "退库统计"}, 9, "btn25");
            this.treeList1.AppendNode(new object[] {
            "库存管理"}, -1, "KCGL");
            this.treeList1.AppendNode(new object[] {
            "库存盘点"}, 14, "btn38");
            this.treeList1.AppendNode(new object[] {
            "商品库存"}, 14, "btn39");
            this.treeList1.AppendNode(new object[] {
            "库存报警"}, 14, "btn40");
            this.treeList1.AppendNode(new object[] {
            "统计报表"}, -1, "TJBB");
            this.treeList1.AppendNode(new object[] {
            "进销存明细"}, 18, "btn26");
            this.treeList1.AppendNode(new object[] {
            "往来帐款"}, -1, "WLZK");
            this.treeList1.AppendNode(new object[] {
            "应收登记"}, 20, "btn32");
            this.treeList1.AppendNode(new object[] {
            "应付登记"}, 20, "btn33");
            this.treeList1.AppendNode(new object[] {
            "收款登记"}, 20, "btn34");
            this.treeList1.AppendNode(new object[] {
            "付款登记"}, 20, "btn35");
            this.treeList1.AppendNode(new object[] {
            "系统设置"}, -1, "XTSZ");
            this.treeList1.AppendNode(new object[] {
            "参数设置"}, 25, "btn5");
            this.treeList1.AppendNode(new object[] {
            "操作员设置"}, 25, "btn6");
            this.treeList1.AppendNode(new object[] {
            "角色设置"}, 25, "btn7");
            this.treeList1.AppendNode(new object[] {
            "字典类型"}, 25, "btn41");
            this.treeList1.AppendNode(new object[] {
            "字典数据"}, 25, "btn42");
            this.treeList1.EndUnboundLoad();
            this.treeList1.OptionsBehavior.Editable = false;
            this.treeList1.OptionsView.CheckBoxStyle = DevExpress.XtraTreeList.DefaultNodeCheckBoxStyle.Check;
            this.treeList1.Size = new System.Drawing.Size(307, 346);
            this.treeList1.TabIndex = 0;
            this.treeList1.AfterCheckNode += new DevExpress.XtraTreeList.NodeEventHandler(this.treeList1_AfterCheckNode);
            // 
            // treeListColumn1
            // 
            this.treeListColumn1.Caption = "功能名称";
            this.treeListColumn1.FieldName = "功能名称";
            this.treeListColumn1.Name = "treeListColumn1";
            this.treeListColumn1.Visible = true;
            this.treeListColumn1.VisibleIndex = 0;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(110, 461);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 3;
            this.simpleButton1.Text = "确认";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(213, 463);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 4;
            this.simpleButton2.Text = "取消";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // JSGLEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(335, 525);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "JSGLEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "增加角色";
            this.Load += new System.EventHandler(this.JSGLEditForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treeList1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.TextEdit textEdit3;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraTreeList.TreeList treeList1;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn1;
    }
}