﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace JXC.Models
{
    ///<summary>
    ///
    ///</summary>
    [SugarTable("cgthd")]
    public partial class cgthd
    {
           public cgthd(){


        }  /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey = true, IsIdentity = true)]
           public int Id { get; set; }
        /// <summary>
        /// Desc:主表编号
        /// Default:
        /// Nullable:True
        /// </summary>           
        public string? MCode {get;set;}

           /// <summary>
           /// Desc:从表编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? DCode {get;set;}

           /// <summary>
           /// Desc:商品编码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? SPCode {get;set;}

           /// <summary>
           /// Desc:商品名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? SPName {get;set;}

           /// <summary>
           /// Desc:产品规格
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? CPGG {get;set;}

           /// <summary>
           /// Desc:单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? DW {get;set;}

           /// <summary>
           /// Desc:单价
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DJ {get;set;}

           /// <summary>
           /// Desc:数量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? SL {get;set;}

           /// <summary>
           /// Desc:总金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? ZJE {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? BZ {get;set;}

           /// <summary>
           /// Desc:操作员编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? CZYCode {get;set;}

           /// <summary>
           /// Desc:操作员名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? CZYName {get;set;}

    }
}
