﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace JXC.Models
{
    ///<summary>
    ///
    ///</summary>
    [SugarTable("ghsxx")]
    public partial class ghsxx
    {
           public ghsxx(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int Id {get;set;}

           /// <summary>
           /// Desc:供货商编码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? GHSCode {get;set;}

           /// <summary>
           /// Desc:供货商名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? GHSName {get;set;}

           /// <summary>
           /// Desc:联系人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? LXR {get;set;}

           /// <summary>
           /// Desc:联系电话
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? LXDH {get;set;}

           /// <summary>
           /// Desc:供货商类型编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? GHSLXCode {get;set;}

           /// <summary>
           /// Desc:供货商类型名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? GHSLXName {get;set;}

           /// <summary>
           /// Desc:所属地区
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? SSDQ {get;set;}

           /// <summary>
           /// Desc:我方应付金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? WFYFJE {get;set;}

           /// <summary>
           /// Desc:联系地址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? LXDZ {get;set;}

           /// <summary>
           /// Desc:默认供应商
           /// Default:
           /// Nullable:True
           /// </summary>           
           public bool? IsMR {get;set;}

           /// <summary>
           /// Desc:禁用
           /// Default:
           /// Nullable:True
           /// </summary>           
           public bool? IsJY {get;set;}

           /// <summary>
           /// Desc:简码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? JM {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? BZ {get;set;}

    }
}
