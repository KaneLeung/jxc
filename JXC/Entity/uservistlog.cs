﻿using System;
using SqlSugar;

namespace JXC.Models;
[SugarTable("uservistlog")]
public class uservistlog
{
    public uservistlog(){


    }
    
    public int Id {get;set;}
    public string? IP {get;set;} 
    public string? Address {get;set;}
    public string? Method {get;set;}
    public DateTime RequestTime {get;set;}
}