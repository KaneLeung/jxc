﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace JXC.Models
{
    ///<summary>
    ///
    ///</summary>
    [SugarTable("czysz")]
    public partial class czysz
    {
           public czysz(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int Id {get;set;}

           /// <summary>
           /// Desc:编码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? CZYCode {get;set;}

           /// <summary>
           /// Desc:操作员名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? CZYName {get;set;}

           /// <summary>
           /// Desc:角色编码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? SSJSCode {get;set;}

           /// <summary>
           /// Desc:角色名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? SSJSName {get;set;}

           /// <summary>
           /// Desc:停用
           /// Default:
           /// Nullable:True
           /// </summary>           
           public bool? IsTY {get;set;}
           /// <summary>
           /// Desc:密码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? PWD { get; set; }

    }
}
