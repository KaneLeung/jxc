﻿using System.Collections.Generic;
using JXC.Models;

namespace JXC.WebApi.EntityExtend
{
    public class KCPD
    {
        /// <summary>
        /// 主表
        /// </summary>
        public kcpdm m { get; set; } = new();
        /// <summary>
        /// 明细表
        /// </summary>
        public List<kcpdd> d { get; set; } = new();
    }
}
