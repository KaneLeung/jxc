﻿using System.Collections.Generic;
using JXC.Models;

namespace JXC.WebApi.EntityExtend
{
    public class XSTH
    {
        public xsthm m { get; set; }
        public List<xsthd> d { get; set; } = new();
    }
}
