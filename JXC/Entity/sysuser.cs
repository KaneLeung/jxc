﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace JXC.Models
{
    ///<summary>
    ///
    ///</summary>
    [SugarTable("sysuser")]
    public partial class sysuser
    {
           public sysuser(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int Id {get;set;}

           /// <summary>
           /// Desc:登录账号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? UserCode {get;set;}

           /// <summary>
           /// Desc:名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? UserName {get;set;}

           /// <summary>
           /// Desc:密码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? Password {get;set;}

    }
}
