﻿using System.Collections.Generic;
using JXC.Models;

namespace JXC.WebApi.EntityExtend
{
    public class CGTH
    {
        /// <summary>
        /// 主表
        /// </summary>
        public cgthm m { get; set; } = new();
        /// <summary>
        /// 明细表
        /// </summary>
        public List<cgthd> d { get; set; } = new();
    }
}
