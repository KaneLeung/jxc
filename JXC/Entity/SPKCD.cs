﻿using System;

namespace JXC.WebApi.EntityExtend
{
    /// <summary>
    /// MINGXI
    /// </summary>
    public class SPKCD
    {
        public string code { get; set; }
        public DateTime CodeTime { get; set; }
        public string XGDW { get; set; }
        public string SPCode { get; set;}
        public string SPName { get; set; }
        public string SPLB { get; set; }

        public string DW { get; set; }

        public string GGXH { get; set; }
        public string DJLX { get; set; }

        public string IN { get; set; }
        public string OUT { get; set; }
    }
}
