﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace JXC.Models
{
    ///<summary>
    ///
    ///</summary>
    [SugarTable("spxx")]
    public partial class spxx
    {
           public spxx(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int Id {get;set;}

           /// <summary>
           /// Desc:商品编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? SPCode {get;set;}

           /// <summary>
           /// Desc:商品名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? SPName {get;set;}

           /// <summary>
           /// Desc:规格型号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? GGXH {get;set;}

           /// <summary>
           /// Desc:预设售价
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? YSSJ {get;set;}

           /// <summary>
           /// Desc:是否可作为赠品
           /// Default:
           /// Nullable:True
           /// </summary>           
           public bool? IsZP {get;set;}

           /// <summary>
           /// Desc:单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? DW {get;set;}

           /// <summary>
           /// Desc:批发价
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? PFJ {get;set;}

           /// <summary>
           /// Desc:预设进价
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? YSJJ {get;set;}

           /// <summary>
           /// Desc:商品条码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? SPTM {get;set;}

           /// <summary>
           /// Desc:库存下限
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? KCXX {get;set;}

           /// <summary>
           /// Desc:期初库存
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? QCKC {get;set;}

           /// <summary>
           /// Desc:商品均价
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? SPJJ {get;set;}

           /// <summary>
           /// Desc:成本总额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? CBZE {get;set;}

           /// <summary>
           /// Desc:预设进价作为销售成本价
           /// Default:
           /// Nullable:True
           /// </summary>           
           public bool? IsYSXS {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? BZ {get;set;}

           /// <summary>
           /// Desc:商品图片
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? SPTP {get;set;}

           /// <summary>
           /// Desc:商品类别编码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? SPLBCode {get;set;}

           /// <summary>
           /// Desc:商品类别名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? SPLBName {get;set;}

           /// <summary>
           /// Desc当前库存
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DQKC { get; set; }

    }
}
