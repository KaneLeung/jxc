﻿namespace JXC.WebApi.EntityExtend
{
    public class SPKCCX
    {
        public string SPCode { get; set; }
        public string SPName { get; set; }
        public string SPLB { get; set; }
        public string DW { get; set; }

        public string CPGH { get; set; }
        public decimal DQKC { get; set; }

        public decimal CBJ { get; set; }
        public decimal KCJE { get; set; }
        public decimal HSJ { get; set; }
        public decimal HSJE { get; set; }

        public decimal ZDKC { get; set; }
        public string BZ { get; set; }
    }
}
