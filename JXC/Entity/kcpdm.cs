﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace JXC.Models
{
    ///<summary>
    ///
    ///</summary>
    [SugarTable("kcpdm")]
    public partial class kcpdm
    {
           public kcpdm(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int Id {get;set;}

           /// <summary>
           /// Desc:单号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? Code {get;set;}

           /// <summary>
           /// Desc:盘点日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? PDRQ {get;set;}

           /// <summary>
           /// Desc:盘点数量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? PDSL {get;set;}

           /// <summary>
           /// Desc:库存数量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? KCSL {get;set;}

           /// <summary>
           /// Desc:差异数量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? CYSL {get;set;}

           /// <summary>
           /// Desc:操作员编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? CZYCode {get;set;}

           /// <summary>
           /// Desc:操作员姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? CZYName {get;set;}

           /// <summary>
           /// Desc:审核人编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? SHRCode {get;set;}

           /// <summary>
           /// Desc:审核人名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? SHRName {get;set;}

           /// <summary>
           /// Desc:审核日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? SHRQ {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? BZ {get;set;}

           /// <summary>
           /// Desc:是否审核
           /// Default:
           /// Nullable:True
           /// </summary>           
           public bool? IsSH {get;set;}

    }
}
