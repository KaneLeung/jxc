﻿using System.Collections.Generic;
using JXC.Models;

namespace JXC.WebApi.EntityExtend
{
    public class CGRK
    {
        /// <summary>
        /// 主表
        /// </summary>
        public cgrkm m { get; set; }=new();
        /// <summary>
        /// 明细表
        /// </summary>
        public List<cgrkd> d { get; set; }=new();
    }
}
