﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace JXC.Models
{
    ///<summary>
    ///
    ///</summary>
    [SugarTable("xsthm")]
    public partial class xsthm
    {
           public xsthm(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int Id {get;set;}

           /// <summary>
           /// Desc:单据号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? Code {get;set;}

           /// <summary>
           /// Desc:退货日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? CHRQ {get;set;}

           /// <summary>
           /// Desc:客户编码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? KHCode {get;set;}

           /// <summary>
           /// Desc:客户名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? KHName {get;set;}

           /// <summary>
           /// Desc:应付金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? YSJE {get;set;}

           /// <summary>
           /// Desc:实付金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? SSJE {get;set;}

           /// <summary>
           /// Desc:优惠金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? YHJE {get;set;}

           /// <summary>
           /// Desc:欠款金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? QKJE {get;set;}

           /// <summary>
           /// Desc:操作员编码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? CZYCode {get;set;}

           /// <summary>
           /// Desc:操作员名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? CZYName {get;set;}

           /// <summary>
           /// Desc:审核人编码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? SHRCode {get;set;}

           /// <summary>
           /// Desc:审核人名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? SHRName {get;set;}

           /// <summary>
           /// Desc:审核日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? SHRQ {get;set;}

           /// <summary>
           /// Desc:是否审核
           /// Default:
           /// Nullable:True
           /// </summary>           
           public bool? IsSH {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? BZ {get;set;}

           /// <summary>
           /// Desc:1 余款计入应付帐 2 按是付款结算
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? JSFS {get;set;}

    }
}
