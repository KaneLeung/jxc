﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace JXC.Models
{
    ///<summary>
    ///
    ///</summary>
    [SugarTable("cgrkm")]
    public partial class cgrkm
    {
           public cgrkm(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int Id {get;set;}

           /// <summary>
           /// Desc:单号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? Code {get;set;}

           /// <summary>
           /// Desc:供货商编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? GHSCode {get;set;}

           /// <summary>
           /// Desc:供货商名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? GHSName {get;set;}

           /// <summary>
           /// Desc:开单日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? CodeTime {get;set;}

           /// <summary>
           /// Desc:应付金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? YFJE {get;set;}

           /// <summary>
           /// Desc:实付金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? SFJE {get;set;}

           /// <summary>
           /// Desc:欠款金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? QKJE {get;set;}

           /// <summary>
           /// Desc:优惠金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? YHJE {get;set;}

           /// <summary>
           /// Desc:操作员编码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? CZYCode {get;set;}

           /// <summary>
           /// Desc:操作员名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? CZYName {get;set;}

           /// <summary>
           /// Desc:审核人编码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? SHRCode {get;set;}

           /// <summary>
           /// Desc:审核人名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? SHRName {get;set;}

           /// <summary>
           /// Desc:审核时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? SHTime {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? Remark {get;set;}

           /// <summary>
           /// Desc:是否审核
           /// Default:
           /// Nullable:True
           /// </summary>           
           public bool? IsSH {get;set;}

           /// <summary>
           /// Desc:1:余款计入应付帐 2:按实付款结算
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? JSFS {get;set;}

    }
}
