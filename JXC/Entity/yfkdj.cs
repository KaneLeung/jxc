﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace JXC.Models
{
    ///<summary>
    ///
    ///</summary>
    [SugarTable("yfkdj")]
    public partial class yfkdj
    {
           public yfkdj(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true, IsIdentity = true)]
           public int Id {get;set;}

           /// <summary>
           /// Desc:单据号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? Code {get;set;}

           /// <summary>
           /// Desc:开单日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? KDRQ {get;set;}

           /// <summary>
           /// Desc:供货商编码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? GHSCode {get;set;}

           /// <summary>
           /// Desc:供货商名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? GHSName {get;set;}

           /// <summary>
           /// Desc:应付金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? YFJE {get;set;}

           /// <summary>
           /// Desc:是否审核
           /// Default:
           /// Nullable:True
           /// </summary>           
           public bool? IsSH {get;set;}

           /// <summary>
           /// Desc:操作员编码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? CZYCode {get;set;}

           /// <summary>
           /// Desc:操作员名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? CZYName {get;set;}

           /// <summary>
           /// Desc:审核人编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? SHRCode {get;set;}

           /// <summary>
           /// Desc:审核人名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? SHRName {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? BZ {get;set;}
           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? DJLX { get; set; }

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? SCode { get; set; }
    }
}
