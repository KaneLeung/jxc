﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace JXC.Models
{
    ///<summary>
    ///
    ///</summary>
    [SugarTable("xscktj")]
    public partial class xscktj
    {
           public xscktj(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int Id {get;set;}

           /// <summary>
           /// Desc:单据号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? Code {get;set;}

           /// <summary>
           /// Desc:出库日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? CKRQ {get;set;}

           /// <summary>
           /// Desc:客户编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? KHCode {get;set;}

           /// <summary>
           /// Desc:客户名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? KHName {get;set;}

           /// <summary>
           /// Desc:商品编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? SPCode {get;set;}

           /// <summary>
           /// Desc:商品名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? SPName {get;set;}

           /// <summary>
           /// Desc:单价
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DJ {get;set;}

           /// <summary>
           /// Desc:数量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? SL {get;set;}

           /// <summary>
           /// Desc:总金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? ZJE {get;set;}

           /// <summary>
           /// Desc:单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? DW {get;set;}

           /// <summary>
           /// Desc:规格型号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? GGXH {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? BZ {get;set;}

    }
}
