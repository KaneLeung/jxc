﻿using SqlSugar;

namespace JXC.WebApi.Entity
{
    [SugarTable("dicData")]
    public class dicData
    {
        public dicData
            ()
        {


        }

        public string DicTypeName { get; set; }
        /// <summary>
        /// Desc:
        /// Default:
        /// Nullable:False
        /// </summary>           
        [SugarColumn(IsPrimaryKey = true, IsIdentity = true)]
        public int ID { get; set; }
        /// <summary>
        /// Desc:编码
        /// Default:
        /// Nullable:True
        /// </summary>           
        public string? DicData { get; set; }
        public string? BZ { get; set; }
    }
}
