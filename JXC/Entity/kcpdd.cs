﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace JXC.Models
{
    ///<summary>
    ///
    ///</summary>
    [SugarTable("kcpdd")]
    public partial class kcpdd
    {
           public kcpdd(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey = true, IsIdentity = true)]
           public int Id { get; set; }
        /// <summary>
        /// Desc:主表单号
        /// Default:
        /// Nullable:True
        /// </summary>           
        public string? MCode {get;set;}

           /// <summary>
           /// Desc:商品编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? SPCode {get;set;}

           /// <summary>
           /// Desc:商品名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? SPName {get;set;}

           /// <summary>
           /// Desc:规格型号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? GGXH {get;set;}

           /// <summary>
           /// Desc:单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? DW {get;set;}

           /// <summary>
           /// Desc:盘点数量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? PDSL {get;set;}

           /// <summary>
           /// Desc:库存数量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? KCSL {get;set;}

           /// <summary>
           /// Desc:差异数量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? CYSL {get;set;}

           /// <summary>
           /// Desc:核算单价
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? HSDJ {get;set;}

           /// <summary>
           /// Desc:盈亏金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? YKJE {get;set;}
           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? BZ { get; set; }
    }
}
