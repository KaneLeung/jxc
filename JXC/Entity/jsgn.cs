﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace JXC.Models
{
    ///<summary>
    ///
    ///</summary>
    [SugarTable("jsgn")]
    public partial class jsgn
    {
           public jsgn(){


           }
           /// <summary>
           /// Desc:角色ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int JSId {get;set;}

           /// <summary>
           /// Desc:功能编码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string? GNBM {get;set;}

    }
}
