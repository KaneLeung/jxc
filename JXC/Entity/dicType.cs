﻿using SqlSugar;

namespace JXC.WebApi.Entity
{
    [SugarTable("dicType")]
    public class dicType
    {
        public dicType
            ()
        {


        }
        /// <summary>
        /// Desc:
        /// Default:
        /// Nullable:False
        /// </summary>           
        [SugarColumn(IsPrimaryKey = true, IsIdentity = true)]
        public int Id { get; set; }
        /// <summary>
        /// Desc:编码
        /// Default:
        /// Nullable:True
        /// </summary>           
        public string? DicTypeName { get; set; }
        public string? BZ { get; set; }
    }
}
