﻿using System;

namespace JXC.WebApi.EntityExtend
{
    public class XSTJB
    {
        public string code { get; set; }
        public string KHName { get; set; }
        public DateTime CodeTime { get; set; }
        public string SPCode { get; set; }
        public string SPName { get; set; }
        public decimal DJ { get; set; }
        public decimal SL { get; set; }
        public decimal ZJE { get; set; }
        public string DW { get; set; }
        public string CPGG { get; set; }
        public string BZ { get; set; }

    }
}
