﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JXC.Update
{
    public partial class FrmUpdate : DevExpress.XtraEditors.XtraForm
    {
        public string url = "";
        public string oldVersion = "";
        public string newVsersion = "";
        public FrmUpdate()
        {
            InitializeComponent();
        }

        private void FrmUpdate_Load(object sender, EventArgs e)
        {
            labUpdateVersionTitle.Text = $"{newVsersion} 现在可用。您安装的版本是 {oldVersion}。是否更新？";
            this.webView21.ScrollBarsEnabled = false;
            webView21.Url = new Uri(url);
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Yes;
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;

        }
    }
}