﻿using DevExpress.XtraEditors;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JXC.Core
{
    public static class ExtensionMethod
    {
        /// <summary>
        /// 显示一般的提示信息
        /// </summary>
        /// <param name="message">提示信息</param>
        public static DialogResult ShowTips(this string message)
        {
            return DevExpress.XtraEditors.XtraMessageBox.Show((message), ("提示信息"), MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /// <summary>
        /// 显示警告信息
        /// </summary>
        /// <param name="message">警告信息</param>
        public static DialogResult ShowWarning(this string message)
        {
            return DevExpress.XtraEditors.XtraMessageBox.Show((message), ("警告信息"), MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        /// <summary>
        /// 显示错误信息
        /// </summary>
        /// <param name="message">错误信息</param>
        public static DialogResult ShowError(this string message)
        {
            return DevExpress.XtraEditors.XtraMessageBox.Show((message), ("错误信息"), MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        /// <summary>
        /// 显示询问用户信息，并显示错误标志
        /// </summary>
        /// <param name="message">错误信息</param>
        public static DialogResult ShowYesNoAndError(this string message)
        {
            return DevExpress.XtraEditors.XtraMessageBox.Show((message), ("错误信息"), MessageBoxButtons.YesNo, MessageBoxIcon.Error);
        }

        /// <summary>
        /// 显示询问用户信息，并显示提示标志
        /// </summary>
        /// <param name="message">错误信息</param>
        public static DialogResult ShowYesNoAndTips(this string message)
        {
            return DevExpress.XtraEditors.XtraMessageBox.Show((message), ("提示信息"), MessageBoxButtons.YesNo, MessageBoxIcon.Information);
        }

        /// <summary>
        /// 显示询问用户信息，并显示警告标志
        /// </summary>
        /// <param name="message">警告信息</param>
        public static DialogResult ShowYesNoAndWarning(this string message)
        {
            return DevExpress.XtraEditors.XtraMessageBox.Show((message), ("警告信息"), MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
        }

        /// <summary>
        /// 显示询问用户信息，并显示提示标志
        /// </summary>
        /// <param name="message">错误信息</param>
        public static DialogResult ShowYesNoCancelAndTips(this string message)
        {
            return DevExpress.XtraEditors.XtraMessageBox.Show((message), ("提示信息"), MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information);
        }

    }
}
